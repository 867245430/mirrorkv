#pragma once

#ifndef ROCKSDB_LITE
#include <stdint.h>
#include <string>
#include "options/cf_options.h"
#include "rocksdb/options.h"
#include "rocksdb/status.h"
#include "rocksdb/table.h"
#include "rocksdb/table_properties.h"
#include "table/format.h"

namespace rocksdb {

class ValueLogBuilder {
public:
  ValueLogBuilder(
      const ImmutableCFOptions& ioptions, const MutableCFOptions& moptions,
      uint32_t column_family_id, const std::string& column_family_name, WritableFileWriter* file);

  void Add(const Slice& value);
  Status status() const;
  Status Finish();
  uint64_t NumEntries();
  uint64_t FileSize() const;
  uint64_t UncompressedFileSize() const;
  Status WriteBlock(const Slice& block_contents, BlockHandle* block_handle);
  Status WriteDataBlock(const Slice& block_contents, CompressionType type, BlockHandle* block_handle);

  void Reset();
  Slice Buffer() { return buffer_; }
  size_t BufferSize() const { return buffer_.size(); }

private:
  const ImmutableCFOptions& ioptions_;
  const MutableCFOptions& moptions_;

  WritableFileWriter* file_;
  uint64_t offset_ = 0;
  uint64_t actual_offset_ = 0;
  TableProperties properties_;
  Status status_;

  // Each block starts with an actual offset.
  std::string buffer_;
};

}

#endif