//  Copyright (c) 2011-present, Facebook, Inc.  All rights reserved.
//  This source code is licensed under both the GPLv2 (found in the
//  COPYING file in the root directory) and Apache 2.0 License
//  (found in the LICENSE.Apache file in the root directory).

#include "rocksdb/flush_block_policy.h"
#include "rocksdb/options.h"
#include "rocksdb/slice.h"
#include "table/block_based/block_builder.h"
#include "table/block_based/value_log_builder.h"
#include "table/format.h"

#include <cassert>

namespace rocksdb {

// Flush block by size
class FlushBlockBySizePolicy : public FlushBlockPolicy {
 public:
  // @params block_size:           Approximate size of user data packed per
  //                               block.
  // @params block_size_deviation: This is used to close a block before it
  //                               reaches the configured
  FlushBlockBySizePolicy(const uint64_t block_size,
                         const uint64_t block_size_deviation,
                         const bool align,
                         const BlockBuilder& data_block_builder,
                         const ValueLogBuilder* vlog = nullptr)
      : block_size_(block_size),
        block_size_deviation_limit_(
            ((block_size * (100 - block_size_deviation)) + 99) / 100),
        align_(align),
        data_block_builder_(data_block_builder),
        vlog_(vlog) {}

  bool Update(const Slice& key, const Slice& value) override {
    // it makes no sense to flush when the data block is empty
    if (data_block_builder_.empty()) {
      return false;
    }

    size_t curr_size = 0;
    if (vlog_)
      curr_size = vlog_->BufferSize();
    curr_size += data_block_builder_.CurrentSizeEstimate();

    // Do flush if one of the below two conditions is true:
    // 1) if the current estimated size already exceeds the block size,
    // 2) block_size_deviation is set and the estimated size after appending
    // the kv will exceed the block size and the current size is under the
    // the deviation.
    return curr_size >= block_size_ || BlockAlmostFull(key, value);
  }

 private:
  bool BlockAlmostFull(const Slice& key, const Slice& value) const {
    if (block_size_deviation_limit_ == 0) {
      return false;
    }

    size_t curr_size = 0, estimated_size_after = 0;
    if (vlog_) {
      curr_size = vlog_->BufferSize();
      estimated_size_after = curr_size;
    }
    curr_size += data_block_builder_.CurrentSizeEstimate();
    estimated_size_after +=
        data_block_builder_.EstimateSizeAfterKV(key, value);

    if (align_) {
      estimated_size_after += kBlockTrailerSize;
      return estimated_size_after > block_size_;
    }

    return estimated_size_after > block_size_ &&
           curr_size > block_size_deviation_limit_;
  }

  const uint64_t block_size_;
  const uint64_t block_size_deviation_limit_;
  const bool align_;
  const BlockBuilder& data_block_builder_;
  const ValueLogBuilder* vlog_;
};

class FlushBlockByCompressedSizePolicy : public FlushBlockPolicy {
 public:
  // @params block_size:           Approximate size of user data packed per
  //                               block.
  // @params block_size_deviation: This is used to close a block before it
  //                               reaches the configured
  FlushBlockByCompressedSizePolicy(const uint64_t block_size,
                         const uint64_t block_size_deviation,
                         const bool align,
                         const BlockBuilder& data_block_builder,
                         const ValueLogBuilder* vlog = nullptr)
      : block_size_(block_size),
        block_size_deviation_limit_(
            ((block_size * (100 - block_size_deviation)) + 99) / 100),
        block_size_deviation_(block_size_deviation),
        align_(align),
        data_block_builder_(data_block_builder),
        vlog_(vlog),
        previous_compressed_size_(0), previous_raw_size_(0),
        threshold1_(block_size_), threshold2_(block_size_deviation_limit_) {}

  bool Update(const Slice& key, const Slice& value) override {
    // it makes no sense to flush when the data block is empty
    if (data_block_builder_.empty()) {
      return false;
    }

    size_t curr_size = 0;
    if (vlog_)
      curr_size = vlog_->BufferSize();
    curr_size += data_block_builder_.CurrentSizeEstimate();

    // Do flush if one of the below two conditions is true:
    // 1) if the current estimated size already exceeds the block size,
    // 2) block_size_deviation is set and the estimated size after appending
    // the kv will exceed the block size and the current size is under the
    // the deviation.
    bool should_flush = curr_size >= threshold1_ || BlockAlmostFull(key, value);
    if (should_flush) {
      previous_raw_size_ = curr_size;
    }
    return should_flush;
  }

  void UpdateCompressedSize(uint64_t size) override {
    previous_compressed_size_ = size;
    threshold1_ = (uint64_t)((double)(previous_raw_size_) / (double)(previous_compressed_size_) * (double)(block_size_));
    threshold2_ = ((threshold1_ * (100 - block_size_deviation_)) + 99) / 100;
  }

 private:
  bool BlockAlmostFull(const Slice& key, const Slice& value) const {
    if (block_size_deviation_limit_ == 0) {
      return false;
    }

    size_t curr_size = 0, estimated_size_after = 0;
    if (vlog_) {
      curr_size = vlog_->BufferSize();
      estimated_size_after = curr_size;
    }
    curr_size += data_block_builder_.CurrentSizeEstimate();
    estimated_size_after +=
        data_block_builder_.EstimateSizeAfterKV(key, value);

    if (align_) {
      estimated_size_after += kBlockTrailerSize;
      return estimated_size_after > threshold1_;
    }

    return estimated_size_after > threshold1_ &&
           curr_size > threshold2_;
  }

  const uint64_t block_size_;
  const uint64_t block_size_deviation_limit_;
  const uint64_t block_size_deviation_;
  const bool align_;
  const BlockBuilder& data_block_builder_;
  const ValueLogBuilder* vlog_;
  uint64_t previous_compressed_size_;
  uint64_t previous_raw_size_;
  uint64_t threshold1_;
  uint64_t threshold2_;
};

FlushBlockPolicy* FlushBlockBySizePolicyFactory::NewFlushBlockPolicy(
    const BlockBasedTableOptions& table_options,
    const BlockBuilder& data_block_builder, const ValueLogBuilder* vlog) const {
  if (table_options.use_compressed_block_size)
    return new FlushBlockByCompressedSizePolicy(
        table_options.compressed_block_size, table_options.block_size_deviation,
        table_options.block_align, data_block_builder, vlog);
  return new FlushBlockBySizePolicy(
      table_options.block_size, table_options.block_size_deviation,
      table_options.block_align, data_block_builder, vlog);
}

FlushBlockPolicy* FlushBlockBySizePolicyFactory::NewFlushBlockPolicy(
    const uint64_t size, const int deviation,
    const BlockBuilder& data_block_builder, const ValueLogBuilder* vlog) {
  return new FlushBlockBySizePolicy(size, deviation, false, data_block_builder, vlog);
}

}  // namespace rocksdb
