#ifndef ROCKSDB_LITE

#include "table/block_based/value_log_builder.h"
#include "table/format.h"
#include "table/meta_blocks.h"
#include "util/coding.h"

namespace rocksdb {

extern const uint64_t kBlockBasedTableValueLogMagicNumber = 0x85251705259ull;

ValueLogBuilder::ValueLogBuilder(
    const ImmutableCFOptions& ioptions, const MutableCFOptions& moptions,
    uint32_t column_family_id, const std::string& column_family_name,
    WritableFileWriter* file)
    : ioptions_(ioptions),
      moptions_(moptions),
      file_(file) {
  properties_.column_family_id = column_family_id;
  properties_.column_family_name = column_family_name;
  Reset();
}

Status ValueLogBuilder::WriteDataBlock(const Slice& block_contents, CompressionType type, BlockHandle* block_handle) {
  block_handle->set_vl_offset(actual_offset_);
  block_handle->set_vl_size(block_contents.size());
  actual_offset_ += block_contents.size();
  Status s = file_->Append(block_contents);
  if (!s.ok())
    return s;
  char t[1];
  t[0] = type;
  s = file_->Append(Slice(t, 1));
  actual_offset_++;
  return s;
}

Status ValueLogBuilder::WriteBlock(const Slice& block_contents, BlockHandle* block_handle) {
  block_handle->set_offset(actual_offset_);
  block_handle->set_size(block_contents.size());
  Status s = file_->Append(block_contents);

  if (s.ok()) {
    // offset_ += block_contents.size();
    actual_offset_ += block_contents.size();
  }
  return s;
}

void __test_print_buffer_(const std::string & s) {
  for (size_t i = 0; i < s.size(); i++)
    printf("%02x ", s[i]);
  printf("\n");
}

void ValueLogBuilder::Reset() {
  // printf("ValueLogBuilder::Reset offset_:%lu actual_offset_:%lu\n", offset_, actual_offset_);
  // printf("ValueLogBuilder::Reset buffer_:");
  // __test_print_buffer_(buffer_);
  buffer_.clear();
  PutFixed32(&buffer_, (uint32_t)(offset_));
  // offset_ += 4;
}

void ValueLogBuilder::Add(const Slice& value) {
  // uint32_t value_size = static_cast<uint32_t>(value.size());
  // char tmp_buf[6];
  // char* end_ptr = EncodeVarint32(tmp_buf, value_size);

  // // Write value size.
  // file_->Append(Slice(tmp_buf, end_ptr - tmp_buf));
  
  // Write value.
  // file_->Append(value);
  buffer_.append(value.data(), value.size());
  offset_ += value.size();
  properties_.num_entries++;
  properties_.raw_value_size += value.size();
}

Status ValueLogBuilder::status() const {
  return status_;
}

Status ValueLogBuilder::Finish() {
  properties_.data_size = actual_offset_;

  MetaIndexBuilder meta_index_builer;

  // -- Write property block
  PropertyBlockBuilder property_block_builder;
  // -- Add basic properties
  property_block_builder.AddTableProperty(properties_);
  BlockHandle property_block_handle;
  auto s = WriteBlock(
      property_block_builder.Finish(),
      &property_block_handle
  );
  if (!s.ok()) {
    return s;
  }
  meta_index_builer.Add(kPropertiesBlock, property_block_handle);

  // -- write metaindex block
  BlockHandle metaindex_block_handle;
  s = WriteBlock(
      meta_index_builer.Finish(),
      &metaindex_block_handle
  );
  if (!s.ok()) {
    return s;
  }

  // Write Footer
  // no need to write out new footer if we're using default checksum
  Footer footer(kBlockBasedTableValueLogMagicNumber, 0);
  footer.set_metaindex_handle(metaindex_block_handle);
  footer.set_index_handle(BlockHandle::NullBlockHandle());
  std::string footer_encoding;
  footer.EncodeTo(&footer_encoding);
  s = file_->Append(footer_encoding);
  if (s.ok()) {
    // offset_ += footer_encoding.size();
    actual_offset_ += footer_encoding.size();
  }

  return s;
}

uint64_t ValueLogBuilder::NumEntries() {
  return properties_.num_entries;
}

uint64_t ValueLogBuilder::FileSize() const {
  return actual_offset_;
}

uint64_t ValueLogBuilder::UncompressedFileSize() const {
  return offset_;
}

}

#endif