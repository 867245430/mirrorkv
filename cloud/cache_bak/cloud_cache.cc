#include <functional>
#include <iostream>
#include <limits>
#include <sstream>
#include <thread>

#include "cloud/aws/aws_env.h"
#include "cloud/cloud_cache.h"
#include "util/mutexlock.h"

namespace rocksdb {

uint32_t PAGE_SIZE = 2048;
uint32_t FIRST_LEVEL_GRANULARITY = 8 * 1024 * 1024;
uint32_t SECOND_LEVEL_GRANULARITY = 1 * 1024 * 1024;
uint32_t THIRD_LEVEL_GRANULARITY = 128 * 1024;
uint8_t MAX_SAMPLING_COUNT = 3;
uint32_t FIXED_GRANULARITY = 1 * 1024 * 1024;
int SHARDED_FRAGMENT_MAP_NUM = 64;
int SHARDED_PERSISTENT_MAP_NUM = 256;
int MUTABLE_PERSISTENT_BLOCK_NUM = 4;

FragmentMap::FragmentMap(uint64_t file_size):
	file_size_(file_size),
	cached_size_(0),
	pages_per_seg1_(FIRST_LEVEL_GRANULARITY / PAGE_SIZE),
	pages_per_seg2_(SECOND_LEVEL_GRANULARITY / PAGE_SIZE),
	pages_per_seg3_(THIRD_LEVEL_GRANULARITY / PAGE_SIZE),
	id_(0),
	p_id_(-2)
{
	last_seg1_ = (file_size_ - 1) / FIRST_LEVEL_GRANULARITY;
	int tmp1 = (file_size_ - 1) % SECOND_LEVEL_GRANULARITY;
	last_seg2_ = tmp1 / SECOND_LEVEL_GRANULARITY;
	int tmp2 = tmp1 % THIRD_LEVEL_GRANULARITY;
	last_seg3_ = tmp2 / THIRD_LEVEL_GRANULARITY;
	last_page_ = (tmp2 % THIRD_LEVEL_GRANULARITY) / PAGE_SIZE;
	global_last_page_ = (file_size_ - 1) / PAGE_SIZE;
	// Init root nodes.
	int s = (file_size + FIRST_LEVEL_GRANULARITY - 1) / FIRST_LEVEL_GRANULARITY;
	roots_ = std::vector<std::unique_ptr<FragmentNode>>(s);
}

PageLookUp FragmentMap::lookup_helper(int page_id) {
	int seg1 = page_id / pages_per_seg1_;
	int page_in_seg1 = page_id % pages_per_seg1_;
	int seg2 = page_in_seg1 / pages_per_seg2_;
	int page_in_seg2 = page_in_seg1 % pages_per_seg2_;
	int seg3 = page_in_seg2 / pages_per_seg3_;
	int idx = page_in_seg2 % pages_per_seg3_;
	return PageLookUp(seg1, seg2, seg3, idx);
}

void FragmentMap::lookup_helper(int page_id, PageLookUp* p) {
	p->seg1_ = page_id / pages_per_seg1_;
	int page_in_seg1 = page_id % pages_per_seg1_;
	p->seg2_ = page_in_seg1 / pages_per_seg2_;
	int page_in_seg2 = page_in_seg1 % pages_per_seg2_;
	p->seg3_ = page_in_seg2 / pages_per_seg3_;
	p->idx_ = page_in_seg2 % pages_per_seg3_;
}

void FragmentMap::next_page(PageLookUp* p) {
	p->idx_++;
	if (p->idx_ == pages_per_seg3_) {
		p->idx_ = 0;
		p->seg3_++;
		if (p->seg3_ == (int)(SECOND_LEVEL_GRANULARITY / THIRD_LEVEL_GRANULARITY)) {
			p->seg3_ = 0;
			p->seg2_++;
			if (p->seg2_ == (int)(FIRST_LEVEL_GRANULARITY / SECOND_LEVEL_GRANULARITY)) {
				p->seg2_ = 0;
				p->seg1_++;
			}
		}
	}
}

void FragmentMap::prepare(const PageLookUp& p) {
	bool is_last_seg1 = (p.seg1_ == last_seg1_);
	if (roots_[p.seg1_] == nullptr) {
		// Init first level segment.
		if (is_last_seg1 && file_size_ % FIRST_LEVEL_GRANULARITY != 0)
			roots_[p.seg1_] = std::unique_ptr<FragmentNode>(new FragmentInternalNode(SECOND_LEVEL_GRANULARITY, file_size_ % FIRST_LEVEL_GRANULARITY));
		else
			roots_[p.seg1_] = std::unique_ptr<FragmentNode>(new FragmentInternalNode(SECOND_LEVEL_GRANULARITY, FIRST_LEVEL_GRANULARITY));
	}
	bool is_last_seg2 = is_last_seg1 && (p.seg2_ == last_seg2_);
	if (roots_[p.seg1_]->get_child(p.seg2_) == nullptr) {
		// Init second level segment.
		if (is_last_seg2 && file_size_ % SECOND_LEVEL_GRANULARITY != 0)
			roots_[p.seg1_]->set_child(
				p.seg2_,
				std::unique_ptr<FragmentNode>(new FragmentInternalNode(THIRD_LEVEL_GRANULARITY, file_size_ % SECOND_LEVEL_GRANULARITY)));
		else
			roots_[p.seg1_]->set_child(
				p.seg2_,
				std::unique_ptr<FragmentNode>(new FragmentInternalNode(THIRD_LEVEL_GRANULARITY, SECOND_LEVEL_GRANULARITY)));
	}
	bool is_last_seg3 = is_last_seg2 && (p.seg3_ == last_seg3_);
	if (roots_[p.seg1_]->get_child(p.seg2_)->get_child(p.seg3_) == nullptr) {
		// Init leaf node.
		if (is_last_seg3 && file_size_ % THIRD_LEVEL_GRANULARITY != 0)
			roots_[p.seg1_]->get_child(p.seg2_)->set_child(
				p.seg3_,
				std::unique_ptr<FragmentNode>(new FragmentLeafNode((file_size_ % THIRD_LEVEL_GRANULARITY + PAGE_SIZE - 1) / PAGE_SIZE, file_size_ % THIRD_LEVEL_GRANULARITY)));
		else
			roots_[p.seg1_]->get_child(p.seg2_)->set_child(
				p.seg3_,
				std::unique_ptr<FragmentNode>(new FragmentLeafNode(pages_per_seg3_, THIRD_LEVEL_GRANULARITY)));
	}
}

void FragmentMap::lookup(uint64_t off, uint64_t length,
												int* start_page,
												std::vector<int>* results) {
	*start_page = off / PAGE_SIZE;
	int end_page = (off + length - 1) / PAGE_SIZE;
	PageLookUp p;
	lookup_helper(*start_page, &p);
	for (int i = *start_page; i <= end_page; i++) {
		prepare(p);
		results->push_back(roots_[p.seg1_]->get_child(p.seg2_)->get_child(p.seg3_)->get_ids()->at(p.idx_));
		next_page(&p);
	}
}

int FragmentMap::lookup(int page_id) {
	PageLookUp p;
	lookup_helper(page_id, &p);
	prepare(p);
	return roots_[p.seg1_]->get_child(p.seg2_)->get_child(p.seg3_)->get_ids()->at(p.idx_);
}

void FragmentMap::insert(const std::vector<std::pair<int, int>>& ids) {
	PageLookUp p;
	cached_size_ += PAGE_SIZE * ids.size();
	for (int i = 0; i < (int)(ids.size()); i++) {
		lookup_helper(ids[i].first, &p);
		if (ids[i].first == global_last_page_ && file_size_ % PAGE_SIZE != 0)
			cached_size_ -= PAGE_SIZE - (file_size_ % PAGE_SIZE);
		roots_[p.seg1_]->get_child(p.seg2_)->get_child(p.seg3_)->get_ids()->at(p.idx_) = ids[i].second;
	}
}

void FragmentMap::insert(int page_id, int cache_id) {
	PageLookUp p;
	lookup_helper(page_id, &p);
	if (page_id == global_last_page_ && file_size_ % PAGE_SIZE != 0)
		cached_size_ += file_size_ % PAGE_SIZE;
	else
		cached_size_ += PAGE_SIZE;
	prepare(p);
	roots_[p.seg1_]->get_child(p.seg2_)->get_child(p.seg3_)->get_ids()->at(p.idx_) = cache_id;
}

void FragmentMap::evict(const std::vector<int>& ids) {
	PageLookUp p;
	cached_size_ -= PAGE_SIZE * ids.size();
	for (int i = 0; i < (int)(ids.size()); i++) {
		lookup_helper(ids[i], &p);
		if (ids[i] == global_last_page_ && file_size_ % PAGE_SIZE != 0)
			cached_size_ += PAGE_SIZE - (file_size_ % PAGE_SIZE);
		prepare(p);
		roots_[p.seg1_]->get_child(p.seg2_)->get_child(p.seg3_)->get_ids()->at(p.idx_) = -1;
	}
}

void FragmentMap::evict(int page_id) {
	PageLookUp p;
	lookup_helper(page_id, &p);
	if (page_id == global_last_page_ && file_size_ % PAGE_SIZE != 0)
		cached_size_ -= file_size_ % PAGE_SIZE;
	else
		cached_size_ -= PAGE_SIZE;
	prepare(p);
	roots_[p.seg1_]->get_child(p.seg2_)->get_child(p.seg3_)->get_ids()->at(p.idx_) = -1;
}

void FragmentMap::print() {
	for (int i = 0; i < (int)(roots_.size()); i++) {
		std::cout << "[" << std::to_string(FIRST_LEVEL_GRANULARITY / 1024 / 1024) << "MB, "
				 << std::to_string(SECOND_LEVEL_GRANULARITY / 1024 / 1024) << "MB, "
				 << std::to_string(THIRD_LEVEL_GRANULARITY / 1024) << "KB]" << std::endl;
		if (roots_[i] != nullptr) {
			// First level.
			std::cout << "- " << i << std::endl;
			for (int j = 0; j < roots_[i]->get_child_num(); j++) {
				if (roots_[i]->get_child(j) != nullptr) {
					// Second level.
					std::cout << "\t\t- " << j << std::endl;
					for (int k = 0; k < roots_[i]->get_child(j)->get_child_num(); k++) {
						if (roots_[i]->get_child(j)->get_child(k) != nullptr) {
							// Third level.
							bool empty = true;
							std::cout << "\t\t\t\t- " << k << std::endl;
							std::cout << "\t\t\t\t\t\t- ";
							for (int l = 0; l < (int)(roots_[i]->get_child(j)->get_child(k)->get_ids()->size()); l++) {
								int id = roots_[i]->get_child(j)->get_child(k)->get_ids()->at(l);
								if (id != -1) {
									std::cout << l << "->" << id << " ";
									empty = false;
								}
							}
							if (empty)
								std::cout << "Empty leaf";
							std::cout << std::endl;
						}
					}
				}
			} 
		}
	}
}

PersistentCacheManager::PersistentCacheManager(Env* env, uint64_t cache_size, uint64_t block_size):
																							 env_(env), cache_size_(cache_size),
																							 block_size_(block_size), num_files_(cache_size / block_size),
																							 max_block_page_num_(block_size / PAGE_SIZE),
																							 block_id_(0), first_block_(0), counters_(MUTABLE_PERSISTENT_BLOCK_NUM),
																							 pm_locks_(MUTABLE_PERSISTENT_BLOCK_NUM), sharded_pm_(MUTABLE_PERSISTENT_BLOCK_NUM) {
	opts_.use_direct_reads = true;
	opts_.use_direct_writes = true;
	// Init the first block.
	std::vector<std::string> files;
	if (env_->GetChildren("cloud_pcache", &files).ok()) {
		for (const std::string& file: files)
			env_->DeleteFile("cloud_pcache/" + file);
	}
	env_->DeleteDir("cloud_pcache");
	env_->CreateDir("cloud_pcache");
	std::unique_ptr<WritableFile> w;

	immutable_blocks_.reserve(MUTABLE_PERSISTENT_BLOCK_NUM);
	for (int i = 0; i < MUTABLE_PERSISTENT_BLOCK_NUM; i++) {
		immutable_blocks_.emplace_back();
		immutable_blocks_.back().buf_ = new char[block_size_];
	}
}

inline int PersistentCacheManager::hash(uint64_t key) {
	return key % MUTABLE_PERSISTENT_BLOCK_NUM;
}

uint64_t PersistentCacheManager::convert(int block_id, int page_id) {
	return (((uint64_t)(block_id) << 32) | (uint64_t)(page_id));
}

uint64_t PersistentCacheManager::convert(int block_id, int page_id, bool on_disk) {
	return (((uint64_t)(block_id) << 32) | (uint64_t)(page_id)) | ((uint64_t)(!on_disk) << 63);
}

void PersistentCacheManager::convert(uint64_t key, int* block_id, int* page_id, bool* on_disk) {
	*block_id = key >> 32;
	*page_id = key & 0xffffffff;
	*on_disk = ((*block_id & 0x80000000) == 0);
	*block_id &= 0x7fffffff;
}

Status PersistentCacheManager::create_block(int block_id) {
	char name[20];
	sprintf(name, "cloud_pcache/%06d", block_id);
	std::unique_ptr<WritableFile> w;
	Status st = env_->NewWritableFile(name, &w, opts_);
	if (!st.ok()) {
		return st;
	}
	w.reset();
	blocks_.push_back(std::unique_ptr<RandomRWFile>());
	st = env_->NewRandomRWFile(name, &(blocks_.back()), opts_);

	return st;
}

// TODO(Alec), block selection.
Status PersistentCacheManager::evict_block() {
	int block_id_tmp = first_block_.fetch_add(1);
	char name[20];
	sprintf(name, "cloud_pcache/%06d", block_id_tmp);

	bool lock_shard = false;
	int shard_id = hash(page_map_[0][0]);
	// Clear all corresponding pages in persistent_map_.
	if (pm_locks_[shard_id].HeldPid() != port::CurrentThread::pid()) {
		pm_locks_[shard_id].WriteLock();
		lock_shard = true;
	}
	for (uint64_t key: page_map_[0])
		sharded_pm_[shard_id].erase(key);

	page_map_.pop_front();
	blocks_.pop_front();

	if (lock_shard)
		pm_locks_[shard_id].WriteUnlock();

	return env_->DeleteFile(name);
}

Status PersistentCacheManager::insert_page(uint64_t key, char* data) {
	Status st;
	int page_id;
	int block_id;
	int relative_block_id;

	int shard_id = hash(key);

	WriteLock lock1(&pm_locks_[shard_id]);
	page_id = counters_[shard_id]++;
	sharded_pm_[shard_id][key] = convert(shard_id, page_id, false);
	immutable_blocks_[shard_id].pm_.push_back(key);
	memcpy(immutable_blocks_[shard_id].buf_ + page_id * PAGE_SIZE, data, PAGE_SIZE);
	if (page_id == max_block_page_num_ - 1) {
		// Need to flush the block.
		WriteLock lock2(&block_lock_);
		if ((int)(blocks_.size()) >= num_files_) {
			// Need to evict a block.
			MutexLock mlock(block_locks_[0].get());
			st = evict_block();
			if (!st.ok())
					return st;
		} else {
			block_locks_.push_back(std::unique_ptr<port::Mutex>(new port::Mutex()));
		}
		// Create a new block.
		block_id = block_id_.fetch_add(1);
		st = create_block(block_id);
		if (!st.ok())
			return st;

		// Write block.
		relative_block_id = block_id - first_block_.load();
		st = blocks_[relative_block_id]->Write(0, Slice(immutable_blocks_[shard_id].buf_, block_size_));

		for (int i = 0; i < (int)(immutable_blocks_[shard_id].pm_.size()); i++)
			sharded_pm_[shard_id][immutable_blocks_[shard_id].pm_[i]] = convert(block_id, i, true);
		page_map_.push_back(std::move(immutable_blocks_[shard_id].pm_));
		immutable_blocks_[shard_id].pm_ = std::vector<uint64_t>();
		counters_[shard_id] = 0;
	}
	
	return st;
}

Status PersistentCacheManager::get_page(uint64_t key, char* data) {
	int block_id, page_id;
	bool on_disk;
	int shard_id = hash(key);
	{
		ReadLock lock(&pm_locks_[shard_id]);
		auto it = sharded_pm_[shard_id].find(key);
		if (it == sharded_pm_[shard_id].end())
			return Status::NotFound();
		convert(it->second, &block_id, &page_id, &on_disk);
		if (!on_disk) {
			memcpy(data, immutable_blocks_[shard_id].buf_ + page_id * PAGE_SIZE, PAGE_SIZE);
			return Status::OK();
		}
	}

	Slice s;
	ReadLock lock(&block_lock_);
	int relative_block_id = block_id - first_block_.load();
	MutexLock mlock(block_locks_[relative_block_id].get());
	Status st = blocks_[relative_block_id]->Read(page_id * PAGE_SIZE, PAGE_SIZE, &s, data);

	return st;
}

void PersistentCacheManager::get_block_range(int* start, int* end) {
	*start = first_block_.load();
	*end = block_id_.load();
}

void PersistentCacheManager::TEST_get_info(uint64_t key, int* block_id, int* page_id, bool* on_disk) {
	int shard_id = hash(key);
	{
		ReadLock lock(&pm_locks_[shard_id]);
		auto it = sharded_pm_[shard_id].find(key);
		if (it == sharded_pm_[shard_id].end())
			return;
		convert(it->second, block_id, page_id, on_disk);
	}
}

CloudCache::CloudCache(const std::shared_ptr<Cache> & cache,
											 uint64_t cache_size,
											 uint64_t block_size,
											 Env* env):
											 page_cache_(cache),
											 sharded_ids_(SHARDED_FRAGMENT_MAP_NUM),
											 sharded_fm_(SHARDED_FRAGMENT_MAP_NUM),
											 fm_locks_(SHARDED_FRAGMENT_MAP_NUM),
											 // lock_held_(SHARDED_FRAGMENT_MAP_NUM, false),
											 file_id_(0) {
	if (env) {
		pc_ = std::unique_ptr<PersistentCacheManager>(
			new PersistentCacheManager(env, cache_size, block_size));
	}
}

std::string CloudCache::convert(int file_id, int cache_id) {
	std::string r(8, '\0');
	r[0] = (file_id >> 24) & 0xff;
	r[1] = (file_id >> 16) & 0xff;
	r[2] = (file_id >> 8) & 0xff;
	r[3] = file_id & 0xff;
	r[4] = (cache_id >> 24) & 0xff;
	r[5] = (cache_id >> 16) & 0xff;
	r[6] = (cache_id >> 8) & 0xff;
	r[7] = cache_id & 0xff;
	return r;
}

void CloudCache::convert(const std::string& key, int* file_id, int* cache_id) {
	*file_id = 0;
	file_id += ((int)(key[0]) << 24);
	file_id += ((int)(key[1]) << 16);
	file_id += ((int)(key[2]) << 8);
	file_id += (int)(key[3]);
	*cache_id = 0;
	cache_id += ((int)(key[4]) << 24);
	cache_id += ((int)(key[5]) << 16);
	cache_id += ((int)(key[6]) << 8);
	cache_id += (int)(key[7]);
}

void cloud_cache_callback(const Slice& key_, void* value_, CloudCache* c, int file_id_tmp, int page_id_tmp, void (*deleter_)(const Slice& key, void* value)) {
	int idx2 = file_id_tmp % SHARDED_FRAGMENT_MAP_NUM;
	bool need_release_lock = false;

	// NOTE(Alec), lock if current thread did not hold the lock.
	if (c->fm_locks_[idx2].HeldPid() != port::CurrentThread::pid()) {
		c->fm_locks_[idx2].WriteLock();
		need_release_lock = true;
		// printf("not locked:%d %d\n", idx2, page_id_tmp);
	}
	std::unordered_map<int, std::unique_ptr<FragmentMap>>::iterator it2 = c->sharded_fm_[idx2].find(file_id_tmp);
	if (it2 != c->sharded_fm_[idx2].end()) {
		if (c->pc_) {
			// Evict the page from FragmentMap.
			// TODO(Alec), sanity check.
			it2->second->evict(page_id_tmp);
			int p_cache_id = it2->second->fetch_sub(1);
			it2->second->insert(page_id_tmp, p_cache_id);
			c->pc_->insert_page(c->pc_->convert(file_id_tmp, p_cache_id), (char*)(value_));
		} else {
			// Evict the page from FragmentMap.
			// TODO(Alec), sanity check.
			it2->second->evict(page_id_tmp);
		}
	}
	if (need_release_lock)
		c->fm_locks_[idx2].WriteUnlock();

	if (deleter_)
		deleter_(key_, value_);
}

void CloudCache::insert(const std::string & file, int page_id, void * value, size_t charge, void (*deleter)(const Slice& key, void* value)) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	std::unordered_map<std::string, int>::iterator it1;
	int file_id;
	{
		ReadLock lock(&fm_locks_[idx1]);

		it1 = sharded_ids_[idx1].find(file);
		if (it1 == sharded_ids_[idx1].end())
			return;
		file_id = it1->second;
	}
	int idx2 = file_id % SHARDED_FRAGMENT_MAP_NUM;
	int cache_id;
	{
		// Insert page to fragment map.
		WriteLock lock(&fm_locks_[idx2]);
		// TODO(Alec), sanity check.
		std::unordered_map<int, std::unique_ptr<FragmentMap>>::iterator it2 = sharded_fm_[idx2].find(file_id);
		cache_id = it2->second->fetch_add(1);
		it2->second->insert(page_id, cache_id);

		page_cache_->InsertFunc(convert(file_id, cache_id), value, charge,
			std::bind(cloud_cache_callback, std::placeholders::_1, std::placeholders::_2, this, file_id, page_id, deleter), nullptr, Cache::Priority::HIGH);
	}
}

void CloudCache::insert(const std::string & file, int page_id, void * value, size_t charge) {
	insert(file, page_id, value, charge, nullptr);
}

void CloudCache::insert(int file_id, int page_id, void * value, size_t charge, void (*deleter)(const Slice& key, void* value)) {
	int idx2 = file_id % SHARDED_FRAGMENT_MAP_NUM;
	int cache_id;
	{
		// Insert page to fragment map.
		WriteLock l(&fm_locks_[idx2]);
		// TODO(Alec), sanity check.
		std::unordered_map<int, std::unique_ptr<FragmentMap>>::iterator it2 = sharded_fm_[idx2].find(file_id);
		cache_id = it2->second->fetch_add(1);
		it2->second->insert(page_id, cache_id);
		page_cache_->InsertFunc(convert(file_id, cache_id), value, charge,
			std::bind(cloud_cache_callback, std::placeholders::_1, std::placeholders::_2, this, file_id, page_id, deleter), nullptr, Cache::Priority::HIGH);
	}
}

void CloudCache::lookup(const std::string & file, uint64_t off, uint64_t length, int* start_page, std::vector<int>* results) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	std::unordered_map<std::string, int>::iterator it1;
	int fid;
	{
		ReadLock lock(&fm_locks_[idx1]);
		it1 = sharded_ids_[idx1].find(file);
		if (it1 == sharded_ids_[idx1].end())
			return;
		fid = it1->second;
	}
	int idx2 = fid % SHARDED_FRAGMENT_MAP_NUM;
	ReadLock lock(&fm_locks_[idx2]);
	sharded_fm_[idx2].find(fid)->second->lookup(off, length, start_page, results);
}

Cache::Handle* CloudCache::lookup(const std::string& file, int page_id) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	std::unordered_map<std::string, int>::iterator it1;
	int file_id;
	{
		ReadLock lock(&fm_locks_[idx1]);
		it1 = sharded_ids_[idx1].find(file);
		if (it1 == sharded_ids_[idx1].end())
			return nullptr;
		file_id = it1->second;
	}

	int idx2 = file_id % SHARDED_FRAGMENT_MAP_NUM;
	int cache_id;
	LRUHandle* lh = nullptr;
	{
		WriteLock lock(&fm_locks_[idx2]);
		// TODO(Alec), sanity check.
		std::unordered_map<int, std::unique_ptr<FragmentMap>>::iterator it2 = sharded_fm_[idx2].find(file_id);
		cache_id = it2->second->lookup(page_id);

		if (cache_id <= -2) {
			lh = new LRUHandle();
			lh->refs = 0;
			lh->flags = 0;
			lh->value = (void*)(new char[PAGE_SIZE]);
			Status s = pc_->get_page(pc_->convert(file_id, cache_id), (char*)(lh->value));
			if (s.code() == Status::Code::kNotFound) {
				delete (char*)(lh->value);
				delete lh;
				// TODO(Alec).
				return nullptr;
			}

			return reinterpret_cast<Cache::Handle*>(lh);
		}
	}

	return page_cache_->Lookup(convert(file_id, cache_id));
}

Cache::Handle* CloudCache::lookup(int file_id, int page_id) {
	int idx2 = file_id % SHARDED_FRAGMENT_MAP_NUM;
	int cache_id;
	LRUHandle* lh = nullptr;
	{
		WriteLock lock(&fm_locks_[idx2]);
		// TODO(Alec), sanity check.
		std::unordered_map<int, std::unique_ptr<FragmentMap>>::iterator it2 = sharded_fm_[idx2].find(file_id);
		cache_id = it2->second->lookup(page_id);
		if (cache_id <= -2) {
			lh = new LRUHandle();
			lh->refs = 0;
			lh->flags = 0;
			lh->value = (void*)(new char[PAGE_SIZE]);
			Status s = pc_->get_page(pc_->convert(file_id, cache_id), (char*)(lh->value));
			if (s.code() == Status::Code::kNotFound) {
				delete (char*)(lh->value);
				delete lh;
				return nullptr;
			}

			return reinterpret_cast<Cache::Handle*>(lh);
		}
	}
	return page_cache_->Lookup(convert(file_id, cache_id));
}

bool CloudCache::release(Cache::Handle* handle, bool force_erase) {
	LRUHandle* e = reinterpret_cast<LRUHandle*>(handle);
	if (e->refs == 0 && e->flags == 0) {
		// NOTE(Alec), do not delete e->value because it's still in cache.
		delete (char*)(e->value);
		delete e;
		return false;
	}
	return page_cache_->Release(handle, force_erase);
}

int CloudCache::file_id(const std::string& file) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	ReadLock lock(&fm_locks_[idx1]);
	return sharded_ids_[idx1].find(file)->second;
}

uint64_t CloudCache::file_size(const std::string& file) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	std::unordered_map<std::string, int>::iterator it1;
	int fid;
	{
		ReadLock lock(&fm_locks_[idx1]);
		it1 = sharded_ids_[idx1].find(file);
		if (it1 == sharded_ids_[idx1].end())
			return 0;
		fid = it1->second;
	}
	int idx2 = fid % SHARDED_FRAGMENT_MAP_NUM;
	ReadLock lock(&fm_locks_[idx2]);
	return sharded_fm_[idx2][fid]->file_size();
}

uint64_t CloudCache::cached_size(const std::string& file) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	std::unordered_map<std::string, int>::iterator it1;
	int fid;
	{
		ReadLock lock(&fm_locks_[idx1]);
		it1 = sharded_ids_[idx1].find(file);
		if (it1 == sharded_ids_[idx1].end())
			return 0;
		fid = it1->second;
	}
	int idx2 = fid % SHARDED_FRAGMENT_MAP_NUM;
	ReadLock lock(&fm_locks_[idx2]);
	return sharded_fm_[idx2][fid]->cached_size();
}

FragmentMap* CloudCache::get_map(const std::string& file) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	std::unordered_map<std::string, int>::iterator it1;
	int fid;
	{
		ReadLock lock(&fm_locks_[idx1]);
		it1 = sharded_ids_[idx1].find(file);
		if (it1 == sharded_ids_[idx1].end())
			return nullptr;
		fid = it1->second;
	}
	int idx2 = fid % SHARDED_FRAGMENT_MAP_NUM;
	ReadLock lock(&fm_locks_[idx2]);
	return sharded_fm_[idx2][fid].get();
}

int CloudCache::next_id(const std::string& file) {
	int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
	std::unordered_map<std::string, int>::iterator it1;
	int fid;
	{
		ReadLock lock(&fm_locks_[idx1]);
		it1 = sharded_ids_[idx1].find(file);
		if (it1 == sharded_ids_[idx1].end())
			return -1;
		fid = it1->second;
	}
	int idx2 = fid % SHARDED_FRAGMENT_MAP_NUM;
	ReadLock lock(&fm_locks_[idx2]);
	return sharded_fm_[idx2][fid]->fetch_add(1);
}

void CloudCache::add_file(const std::string & file_path, uint64_t file_size) {
	int idx1 = std::hash<std::string>{}(file_path) % SHARDED_FRAGMENT_MAP_NUM;
	int fid = -1;
	{
		WriteLock lock(&fm_locks_[idx1]);
		auto n = sharded_ids_[idx1].find(file_path);
		if (n == sharded_ids_[idx1].end()) {
			fid = file_id_.fetch_add(1);
			sharded_ids_[idx1][file_path] = fid;
		}
	}
	if (fid != -1) {
		int idx2 = fid % SHARDED_FRAGMENT_MAP_NUM;
		WriteLock lock(&fm_locks_[idx2]);
		sharded_fm_[idx2][fid] = std::unique_ptr<FragmentMap>(new FragmentMap(file_size));
	}
}

void CloudCache::remove_file(const std::string& file_path) {
	int idx1 = std::hash<std::string>{}(file_path) % SHARDED_FRAGMENT_MAP_NUM;
	int fid = -1;
	{
		WriteLock lock(&fm_locks_[idx1]);
		auto n = sharded_ids_[idx1].find(file_path);
		if (n == sharded_ids_[idx1].end()) {
			return;
		}
		fid = n->second;
		sharded_ids_[idx1].erase(file_path);
	}
	if (fid != -1) {
		int idx2 = fid % SHARDED_FRAGMENT_MAP_NUM;
		WriteLock lock(&fm_locks_[idx2]);
		sharded_fm_[idx2].erase(fid);
	}
}

void CloudCache::TEST_persist_info(const std::string& file, int cache_id, int* block_id, int* page_id, bool* on_disk) {
	if (pc_) {
		int idx1 = std::hash<std::string>{}(file) % SHARDED_FRAGMENT_MAP_NUM;
		std::unordered_map<std::string, int>::iterator it1;
		int file_id;
		{
			ReadLock lock(&fm_locks_[idx1]);
			it1 = sharded_ids_[idx1].find(file);
			if (it1 == sharded_ids_[idx1].end())
				return;
			file_id = it1->second;
		}
		pc_->TEST_get_info(pc_->convert(file_id, cache_id), block_id, page_id, on_disk);
	}
}

void CloudCache::print_summary(bool detail) {
	printf("CloudCache memory:%lu/%lu=%.2f%% disk:%lu/%lu=%.2f%%\n",
			page_cache_->GetUsage(), page_cache_->GetCapacity(),
			static_cast<double>(page_cache_->GetUsage()) / static_cast<double>(page_cache_->GetCapacity()) * 100.0,
			pc_ == nullptr ? 0lu : pc_->disk_size(),
			pc_ == nullptr ? 0lu : pc_->cache_size(),
			(pc_ == nullptr && pc_->cache_size() != 0) ? 0 : static_cast<double>(pc_->disk_size()) / static_cast<double>(pc_->cache_size()) * 100.0);
	if (pc_ && detail) {
		int start, end;
		pc_->get_block_range(&start, &end);
		printf("PersistentCacheManager start_block:%d end_block:%d\n", start, end);
	}
}

}