#pragma once
#include <atomic>
#include <deque>
#include <list>
#include <memory>
#include <set>
#include <stdint.h>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "cache/lru_cache.h"
#include "port/port.h"
#include "rocksdb/cache.h"
#include "rocksdb/env.h"

namespace rocksdb {

extern uint32_t PAGE_SIZE;
extern uint32_t FIRST_LEVEL_GRANULARITY;
extern uint32_t SECOND_LEVEL_GRANULARITY;
extern uint32_t THIRD_LEVEL_GRANULARITY;
extern uint8_t MAX_SAMPLING_COUNT;
extern uint32_t FIXED_GRANULARITY;
extern int SHARDED_FRAGMENT_MAP_NUM;
extern int SHARDED_PERSISTENT_MAP_NUM;
extern int MUTABLE_PERSISTENT_BLOCK_NUM;

// class AwsEnv;

class FragmentNode {
public:
	virtual ~FragmentNode()=default;
	// 0: internal, 1: leaf.
	virtual int type() = 0;
	// For leaf node, it is the size.
	virtual int granularity() = 0;
	virtual int size() = 0;
	virtual int id() { return -1; }
	virtual bool initialized() = 0;
	virtual void set_size(int size) { (void)(size); }
	virtual void set_id(int id) { (void)(id); }
	virtual FragmentNode* get_child(int i) {
		(void)(i);
		return nullptr;
	}
	virtual std::vector<std::unique_ptr<FragmentNode>>* get_childs() { return nullptr; }
	virtual void set_child(int i, std::unique_ptr<FragmentNode>&& n) {
		(void)(i);
		(void)(n);
	}
	virtual int get_child_num() { return 0; }
	virtual std::vector<int>* get_ids() { return nullptr; }
};

class FragmentInternalNode: public FragmentNode {
public:
	FragmentInternalNode(): init_(false) {}
	FragmentInternalNode(int granularity, int size):
		granularity_(granularity), size_(size), init_(false), childs_((size + granularity -1) / granularity) {}

	virtual int type() override { return 0; }
	virtual int granularity() override { return granularity_; }
	virtual int size() override { return size_; }
	virtual bool initialized() override { return init_; }
	virtual void set_size(int size) override { size_ = size; }
	
	// It is user's responsibility to pass in-bound i.
	virtual FragmentNode* get_child(int i) override {
		if (childs_[i] != nullptr)
			return childs_[i].get();
		return nullptr;
	}
	virtual std::vector<std::unique_ptr<FragmentNode>>* get_childs() override {
		return &childs_;
	}
	virtual void set_child(int i, std::unique_ptr<FragmentNode>&& n) override {
		childs_[i] = std::move(n);
	}
	virtual int get_child_num() override { return childs_.size(); }

private:
	int granularity_;
	int size_;
	bool init_;
	std::vector<std::unique_ptr<FragmentNode>> childs_;
};

class FragmentLeafNode: public FragmentNode {
public:
	FragmentLeafNode(int size, int num): size_(size), ids_(num, -1) {}
	virtual int type() override { return 1; }
	virtual int granularity() override { return size_; }
	virtual int size() override { return size_; }
	virtual bool initialized() override { return true; }
	virtual void set_size(int size) override { size_ = size; }
	virtual std::vector<int>* get_ids() override {
		return &ids_;
	}

// private:
	int size_;
	std::vector<int> ids_;
};

struct PageLookUp {
	int seg1_; // Physical.
	int seg2_; // Relative.
	int seg3_;
	int idx_;
	PageLookUp()=default;
	PageLookUp(int seg1, int seg2, int seg3, int idx):
		seg1_(seg1), seg2_(seg2), seg3_(seg3), idx_(idx) {}
};

// Map the page range to a unique id for each cloud file.
class FragmentMap {
public:
	FragmentMap(uint64_t file_size);
	~FragmentMap()=default;

	void lookup(uint64_t off, uint64_t length,
							int* start_page,
							std::vector<int>* results);
	int lookup(int page_id);

	// void get_valid_pages(std::vector<int>* results);

	void insert(const std::vector<std::pair<int, int>>& results);
	void insert(int page_id, int cache_id);

	void evict(const std::vector<int>& ids);
	void evict(int page_id);

	// void set(int page_id, int cache_id);

	uint64_t file_size() { return file_size_; }
	uint64_t cached_size() { return cached_size_; }

	int fetch_add(int i) { return id_.fetch_add(i); }
	int fetch_sub(int i) { return p_id_.fetch_sub(i); }
	void print();

private:
	PageLookUp lookup_helper(int page_id);
	void lookup_helper(int page_id, PageLookUp* p);
	void next_page(PageLookUp* p);
	void prepare(const PageLookUp& p);

	uint64_t file_size_;
	uint64_t cached_size_;
	int last_seg1_;
	int last_seg2_;
	int last_seg3_;
	int last_page_;
	int global_last_page_;
	int pages_per_seg1_;
	int pages_per_seg2_;
	int pages_per_seg3_;
	std::atomic<int> id_;
	std::atomic<int> p_id_; // Persistent id (<= -2).
	std::vector<std::unique_ptr<FragmentNode>> roots_;
};

class PersistentCacheManager {
public:
	struct ImmutableBlock {
		char* buf_;
		std::vector<uint64_t> pm_;
		ImmutableBlock(): buf_(nullptr) {}
		~ImmutableBlock() {
			if (buf_)
				delete buf_;
		}
	};
	PersistentCacheManager(Env* env, uint64_t cache_size, uint64_t block_size = 1 * 1024 * 1024);

	Status insert_page(uint64_t key, char* data);
	Status get_page(uint64_t key, char* data); // data should be at least PAGE_SIZE.

	std::string stats() {
		std::string stat;
		stat.append("num_files:");
		stat.append(std::to_string(num_files_));
		stat.append("\nblock_id:");
		stat.append(std::to_string(block_id_.load()));
		stat.append("\nfirst_block:");
		stat.append(std::to_string(first_block_.load()));
		stat.append("\n--------- <key:block_id,page_id,on_disk> -------");
		for (int i = 0; i < (int)(sharded_pm_.size()); i++) {
			stat.append("\nshard");
			stat.append(std::to_string(i));
			stat.append(":[");
			for (const auto& it: sharded_pm_[i]) {
				int block_id, page_id;
				bool on_disk;
				convert(it.second, &block_id, &page_id, &on_disk);

				size_t size = snprintf(nullptr, 0, "<%lu:%d,%d,%d>", it.first, block_id, page_id, on_disk) + 1;
				std::unique_ptr<char[]> buf(new char[size]); 
		    snprintf(buf.get(), size, "<%lu:%d,%d,%d>", it.first, block_id, page_id, on_disk);
				
				for (size_t j = 0; j < size; j++)
					stat.push_back((buf.get())[j]);
			}
			stat.append("]");
		}
		stat.append("\n------------------------------------------------\n");
		return stat;
	}
	uint64_t convert(int block_id, int page_id);
	uint64_t convert(int block_id, int page_id, bool on_disk);
	void convert(uint64_t key, int* block_id, int* page_id, bool* on_disk);

	uint64_t cache_size() { return cache_size_; }
	uint64_t disk_size() { return block_size_ * blocks_.size(); }
	void get_block_range(int* start, int* end);

	void TEST_get_info(uint64_t key, int* block_id, int* page_id, bool* on_disk);

private:
	Status create_block(int block_id);
	Status evict_block();
	int hash(uint64_t key);

	Env* env_;
	EnvOptions opts_;
	uint64_t cache_size_;
	uint64_t block_size_;
	int num_files_;
	int max_block_page_num_;
	std::atomic<int> block_id_;
	std::atomic<int> first_block_; // connect available_blocks_.

	std::vector<ImmutableBlock> immutable_blocks_;
	std::vector<int> counters_;
	std::deque<std::unique_ptr<RandomRWFile>> blocks_;
	std::vector<port::RWMutex> pm_locks_;
	std::vector<std::unordered_map<uint64_t, uint64_t>> sharded_pm_;
	std::deque<std::vector<uint64_t>> page_map_;

	port::RWMutex block_lock_;
	port::RWMutex available_block_lock_;
	std::vector<std::unique_ptr<port::Mutex>> block_locks_;
};

// Call functions when file exists.
class CloudCache {
public:
	CloudCache(const std::shared_ptr<Cache> & cache,
						 uint64_t cache_size=1*1024*1024*1024,
						 uint64_t block_size=1*1024*1024,
						 Env* env=nullptr);
	~CloudCache() {
		if(pc_)
			pc_.reset();
		page_cache_.reset();
	}

	void insert(const std::string & file, int page_id, void * value, size_t charge, void (*deleter)(const Slice& key, void* value));
	void insert(const std::string & file, int page_id, void * value, size_t charge);
	void insert(int file_id, int page_id, void * value, size_t charge, void (*deleter)(const Slice& key, void* value));

	void lookup(const std::string & file, uint64_t off, uint64_t length, int* start_page, std::vector<int>* results);

	Cache::Handle* lookup(const std::string& file, int page_id);
	Cache::Handle* lookup(int file_id, int page_id);

	int file_id(const std::string& file);
	uint64_t file_size(const std::string& file);
	uint64_t cached_size(const std::string& file);

	FragmentMap* get_map(const std::string& file);

	bool release(Cache::Handle* handle, bool force_erase = false);

	void add_file(const std::string& file_path, uint64_t file_size);
	void remove_file(const std::string& file_path);

	int next_id(const std::string& file);

	void* value(Cache::Handle* handle) { return page_cache_->Value(handle); }

	void print_summary(bool detail=false);

	void TEST_persist_info(const std::string& file, int cache_id, int* block_id, int* page_id, bool* on_disk);

// private:
	std::string convert(int file_id, int page_id);
	void convert(const std::string& key, int* file_id, int* page_id);

	std::shared_ptr<Cache> page_cache_;

	std::vector<std::unordered_map<std::string, int>> sharded_ids_;
	std::vector<std::unordered_map<int, std::unique_ptr<FragmentMap>>> sharded_fm_;
	std::vector<port::RWMutex> fm_locks_;
	// std::vector<std::atomic<bool>> lock_held_;

	std::atomic<int> file_id_;

	std::unique_ptr<PersistentCacheManager> pc_;

};

} // namespace rocksdb.