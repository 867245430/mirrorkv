#include <cmath>
#include <deque>
#include <iostream>
#include <limits>
#include <list>
#include <vector>

#define ASSERT(c, m) tb_assert(c, m, __FILE__, __LINE__)
#define EQUALS(exp, act) tb_equals(exp, act, __FILE__, __LINE__)

void tb_assert(bool condition, const std::string& message, const char* n = __FILE__, int l = __LINE__) {
	if (!condition) {
		std::cout << n << ":" << l << ": " << message << std::endl;
		exit(-1);
	}
}

template <typename T>
void tb_equals(T exp, T act, const char* n = __FILE__, int l = __LINE__) {
	if (exp != act) {
		std::cout << n << ":" << l << ":" << std::endl;
		std::cout << "\texp: " << exp << std::endl;
		std::cout << "\tgot: " << act << std::endl;
		exit(-1);
	}
}

template <typename T>
void tb_equals(const std::vector<T>& exp, const std::vector<T>& act, const char* n = __FILE__, int l = __LINE__) {
	if (exp != act) {
		std::cout << n << ":" << l << ":" << std::endl;
		std::cout << "\texp: [";
		for (int i = 0; i < exp.size() - 1; i++)
			std::cout << exp[i] << ",";
		if (!exp.empty())
			std::cout << exp.back();
		std::cout << "]" << std::endl;
		std::cout << "\tgot: [";
		for (int i = 0; i < act.size() - 1; i++)
			std::cout << act[i] << ",";
		if (!act.empty())
			std::cout << act.back();
		std::cout << "]" << std::endl;
		exit(-1);
	}
}

template <typename T>
void tb_equals(const std::deque<T>& exp, const std::deque<T>& act, const char* n = __FILE__, int l = __LINE__) {
	if (exp != act) {
		std::cout << n << ":" << l << ":" << std::endl;
		std::cout << "\texp: [";
		for (int i = 0; i < exp.size() - 1; i++)
			std::cout << exp[i] << ",";
		if (!exp.empty())
			std::cout << exp.back();
		std::cout << "]" << std::endl;
		std::cout << "\tgot: [";
		for (int i = 0; i < act.size() - 1; i++)
			std::cout << act[i] << ",";
		if (!act.empty())
			std::cout << act.back();
		std::cout << "]" << std::endl;
		exit(-1);
	}
}

template <typename T>
void tb_equals(const std::list<T>& exp, const std::list<T>& act, const char* n = __FILE__, int l = __LINE__) {
	if (exp != act) {
		std::cout << n << ":" << l << ":" << std::endl;
		std::cout << "\texp: [";
		int i = 0;
		auto it = exp.begin();
		while (i < exp.size() - 1) {
			std::cout << *it << ",";
			i++;
			it++;
		}
		if (!exp.empty())
			std::cout << *it;
		std::cout << "]" << std::endl;
		std::cout << "\tgot: [";
		i = 0;
		it = act.begin();
		while (i < act.size() - 1) {
			std::cout << *it << ",";
			i++;
			it++;
		}
		if (!act.empty())
			std::cout << *it;
		std::cout << "]" << std::endl;
		exit(-1);
	}
}