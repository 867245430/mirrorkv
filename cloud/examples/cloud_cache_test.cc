#include "cache/lru_cache.h"
#include "cloud/cloud_cache.h"
#include "cloud/testutil.h"
#include "rocksdb/env.h"
#include "third-party/thread_pool.h"

void test_fragment_map() {
	rocksdb::FragmentMap m(8 * 1024 * 1024 + 2 * 1024 + 99);
	std::vector<int> ids;
	int start_page;

	// Test multiple pages.
	m.lookup(12, 4 * 1024, &start_page, &ids);
	EQUALS(0, start_page);
	EQUALS(ids, std::vector<int>({-1, -1, -1}));
	m.insert(std::vector<std::pair<int, int>>({{0,1}, {1,3}, {2,4}}));
	ids.clear();
	m.lookup(12, 2 * 1024, &start_page, &ids);
	EQUALS(0, start_page);
	EQUALS(ids, std::vector<int>({1, 3}));

	// Test the boundary for the third level.
	ids.clear();
	m.lookup(128 * 1024 - 1, 4 * 1024, &start_page, &ids);
	EQUALS(63, start_page);
	EQUALS(ids, std::vector<int>({-1, -1, -1}));
	m.insert(std::vector<std::pair<int, int>>({{63,0}, {64,5}, {65,6}}));
	ids.clear();
	m.lookup(128 * 1024, 2 * 1024 + 1, &start_page, &ids);
	EQUALS(64, start_page);
	EQUALS(ids, std::vector<int>({5, 6}));

	// Test the boundary for the second level.
	ids.clear();
	m.lookup(1 * 1024 * 1024 - 1, 4 * 1024, &start_page, &ids);
	EQUALS(511, start_page);
	EQUALS(ids, std::vector<int>({-1, -1, -1}));
	m.insert(std::vector<std::pair<int, int>>({{511,7}, {512,8}, {513,9}}));
	ids.clear();
	m.lookup(1 * 1024 * 1024, 2 * 1024 + 1, &start_page, &ids);
	EQUALS(512, start_page);
	EQUALS(ids, std::vector<int>({8, 9}));

	// Test the boundary for the first level.
	ids.clear();
	m.lookup(8 * 1024 * 1024 - 1, 2 * 1024, &start_page, &ids);
	EQUALS(4095, start_page);
	EQUALS(ids, std::vector<int>({-1, -1}));
	m.insert(std::vector<std::pair<int, int>>({{4095,10}, {4096,11}}));
	ids.clear();
	m.lookup(8 * 1024 * 1024, 2 * 1024 + 1, &start_page, &ids);
	EQUALS(4096, start_page);
	EQUALS(ids, std::vector<int>({11, -1}));

	// Test last page.
	ids.clear();
	m.lookup(8 * 1024 * 1024 + 2 * 1024, 9, &start_page, &ids);
	EQUALS(4097, start_page);
	EQUALS(ids, std::vector<int>({-1}));
	m.insert(std::vector<std::pair<int, int>>({{4097,12}}));
	ids.clear();
	m.lookup(8 * 1024 * 1024 + 2 * 1024, 99, &start_page, &ids);
	EQUALS(4097, start_page);
	EQUALS(ids, std::vector<int>({12}));
}

void test_persistent_cache() {
	rocksdb::PAGE_SIZE = 4;
	rocksdb::MUTABLE_PERSISTENT_BLOCK_NUM = 2;
	rocksdb::Env* env = rocksdb::Env::Default();
	rocksdb::PersistentCacheManager pc(env, 32, 16);
	// std::cout << pc.stats() << std::endl;
	for (int i = 0; i < 8; i++) {
		char buf[16];
		sprintf(buf, "%04d", i);
		EQUALS(true, pc.insert_page(i, buf).ok());		
	}
	// std::cout << pc.stats() << std::endl;
	for (int i = 0; i < 8; i++) {
		char buf[16];
		sprintf(buf, "%04d", i);
		char read_buf[16];
		EQUALS(true, pc.get_page(i, read_buf).ok());
		EQUALS(std::string(buf, 4), std::string(read_buf, 4));
	}
	
	EQUALS(rocksdb::Status::NotFound().code(), pc.get_page(9, nullptr).code());

	for (int i = 8; i < 14; i++) {
		char buf[16];
		sprintf(buf, "%04d", i);
		EQUALS(true, pc.insert_page(i, buf).ok());		
	}
	// std::cout << pc.stats() << std::endl;
	for (int i = 0; i < 14; i++) {
		char buf[16];
		sprintf(buf, "%04d", i);
		char read_buf[16];
		EQUALS(true, pc.get_page(i, read_buf).ok());
		EQUALS(std::string(buf, 4), std::string(read_buf, 4));
	}
	for (int i = 14; i < 16; i++) {
		char buf[16];
		sprintf(buf, "%04d", i);
		EQUALS(true, pc.insert_page(i, buf).ok());		
	}
	for (int i = 0; i < 8; i++) {
		EQUALS(rocksdb::Status::NotFound().code(), pc.get_page(i, nullptr).code());
	}
	for (int i = 8; i < 16; i++) {
		char buf[16];
		sprintf(buf, "%04d", i);
		char read_buf[16];
		EQUALS(true, pc.get_page(i, read_buf).ok());
		EQUALS(std::string(buf, 4), std::string(read_buf, 4));
	}
	// std::cout << pc.stats() << std::endl;
}

void multi_thread_persistence_cache(int thread_num) {
	rocksdb::PAGE_SIZE = 4;
	rocksdb::MUTABLE_PERSISTENT_BLOCK_NUM = 4;
	rocksdb::Env* env = rocksdb::Env::Default();
	rocksdb::PersistentCacheManager pc(env, 16 * 1024, 512);

	ThreadPool pool(thread_num);
	std::atomic<int> p(0);

	for (int i = 0; i < thread_num; i++)
		pool.enqueue(std::bind([](rocksdb::PersistentCacheManager* pc_, std::atomic<int>* p_){
			int pid = p_->fetch_add(1);
			while (pid < 4096) {
				char buf[16];
				sprintf(buf, "%04d", pid);
				EQUALS(true, pc_->insert_page(pid, buf).ok());
				pid = p_->fetch_add(1);
			}
		}, &pc, &p));
	pool.wait_barrier();

	p.store(0);
	for (int i = 0; i < thread_num; i++)
		pool.enqueue(std::bind([](rocksdb::PersistentCacheManager* pc_, std::atomic<int>* p_){
			int pid = p_->fetch_add(1);
			while (pid < 4096) {
				char buf[16];
				sprintf(buf, "%04d", pid);
				char read_buf[16];
				EQUALS(true, pc_->get_page(pid, read_buf).ok());
				EQUALS(std::string(buf, 4), std::string(read_buf, 4));
				pid = p_->fetch_add(1);
			}
		}, &pc, &p));
	pool.wait_barrier();
}

void test_cloud_cache(int test) {
	if (test == 1 || test == -1) {
		rocksdb::PAGE_SIZE = 2 * 1024;
		rocksdb::CloudCache* c = new rocksdb::CloudCache(std::shared_ptr<rocksdb::Cache>(rocksdb::NewLRUCache(3)));

		c->add_file("f1", 10 * 1024);
		c->add_file("f2", 10 * 1024);

		// Insert 3 pages to LRU.
		std::vector<int> ids;
		int start_page;
		c->lookup("f1", 2 * 1024 + 9, 6 * 1024, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({-1, -1, -1, -1}), ids);
		c->insert("f1", 1, nullptr, 1, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(2 * 1024), c->cached_size("f1"));
		c->insert("f1", 2, nullptr, 1, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(4 * 1024), c->cached_size("f1"));
		c->insert("f1", 3, nullptr, 1, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(6 * 1024), c->cached_size("f1"));

		// Varify the first three pages.
		ids.clear();
		c->lookup("f1", 2 * 1024 + 9, 6 * 1024, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({0, 1, 2, -1}), ids);

		// Insert the fourth page.
		c->insert("f1", 4, nullptr, 1, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(6 * 1024), c->cached_size("f1"));

		// The first page is evicted.
		ids.clear();
		c->lookup("f1", 2 * 1024 + 9, 6 * 1024, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({-1, 1, 2, 3}), ids);

		rocksdb::Cache::Handle* h = c->lookup("f1", 2);
		EQUALS((void*)(nullptr), c->value(h));

		// Insert the first page.
		char* buf = new char[2048];
		sprintf(buf, "helloworld");
		c->insert("f1", 1, buf, 1, [](const rocksdb::Slice& key, void* value) {delete (char*)(value);});
		EQUALS((uint64_t)(6 * 1024), c->cached_size("f1"));

		// The third page is evicted.
		ids.clear();
		c->lookup("f1", 2 * 1024 + 9, 6 * 1024, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({4, 1, -1, 3}), ids);

		// Read back first page.
		h = c->lookup("f1", 1);
		EQUALS(std::string("helloworld", 10), std::string((char*)(c->value(h)), 10));
		c->release(h);
		delete c;
	}
	if (test == 2 || test == -1) {
		rocksdb::PAGE_SIZE = 4;
		rocksdb::CloudCache c(
			std::shared_ptr<rocksdb::Cache>(rocksdb::NewLRUCache(3*rocksdb::PAGE_SIZE)),
			16, 8, rocksdb::Env::Default());

		char p1[] = "0001", p2[] = "0002", p3[] = "0003", p4[] = "0004";
		c.add_file("f1", 20);

		// Insert 3 pages to LRU.
		std::vector<int> ids;
		int start_page;
		c.lookup("f1", 6, 12, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({-1, -1, -1, -1}), ids);
		c.insert("f1", 1, p1, rocksdb::PAGE_SIZE);
		EQUALS((uint64_t)(4), c.cached_size("f1"));
		c.insert("f1", 2, p2, rocksdb::PAGE_SIZE);
		EQUALS((uint64_t)(8), c.cached_size("f1"));
		c.insert("f1", 3, p3, rocksdb::PAGE_SIZE);
		EQUALS((uint64_t)(12), c.cached_size("f1"));

		// Varify the first three pages.
		ids.clear();
		c.lookup("f1", 6, 12, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({0, 1, 2, -1}), ids);

		// Insert the fourth page.
		c.insert("f1", 4, p4, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(16), c.cached_size("f1"));

		// The first page is evicted to the persistent cache.
		ids.clear();
		c.lookup("f1", 6, 12, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({-2, 1, 2, 3}), ids);

		// Read back page 1.
		rocksdb::Cache::Handle* h = c.lookup("f1", 1);
		EQUALS(std::string("0001", 4), std::string((char*)(c.value(h)), 4));

		// The first page is still in the persistent cache.
		ids.clear();
		c.lookup("f1", 6, 12, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({-2, 1, 2, 3}), ids);

		c.add_file("f2", 33);
		char p5[] = "0005", p6[] = "0006", p7[] = "7";

		c.insert("f2", 8, p7, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(1), c.cached_size("f2"));

		ids.clear();
		c.lookup("f2", 24, 9, &start_page, &ids);
		EQUALS(6, start_page);
		EQUALS(std::vector<int>({-1, -1, 0}), ids);

		c.insert("f2", 7, p6, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(5), c.cached_size("f2"));

		ids.clear();
		c.lookup("f2", 24, 9, &start_page, &ids);
		EQUALS(6, start_page);
		EQUALS(std::vector<int>({-1, 1, 0}), ids);

		c.insert("f2", 6, p5, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(9), c.cached_size("f2"));

		ids.clear();
		c.lookup("f2", 24, 9, &start_page, &ids);
		EQUALS(6, start_page);
		EQUALS(std::vector<int>({2, 1, 0}), ids);

		c.insert("f2", 5, p5, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) {});
		EQUALS((uint64_t)(13), c.cached_size("f2"));

		// page 8 of f2 is evicted to persistent cache. 
		ids.clear();
		c.lookup("f2", 24, 9, &start_page, &ids);
		EQUALS(6, start_page);
		EQUALS(std::vector<int>({2, 1, -2}), ids);

		// Read back page 8 of f2.
		h = c.lookup("f2", 8);
		EQUALS(std::string("7", 1), std::string((char*)(c.value(h)), 1));

		// page 8 of f2 is still in persistent cache. 
		ids.clear();
		c.lookup("f2", 24, 9, &start_page, &ids);
		EQUALS(6, start_page);
		EQUALS(std::vector<int>({2, 1, -2}), ids);

		// Verify pages of f1.
		ids.clear();
		c.lookup("f1", 6, 12, &start_page, &ids);
		EQUALS(1, start_page);
		EQUALS(std::vector<int>({-2, -3, -4, -5}), ids);
	}
}

void multi_thread_test(int thread_num) {
	rocksdb::FIRST_LEVEL_GRANULARITY = 2048 * 8;
	rocksdb::SECOND_LEVEL_GRANULARITY = 2048;
	rocksdb::THIRD_LEVEL_GRANULARITY = 256;
	rocksdb::MUTABLE_PERSISTENT_BLOCK_NUM = 4;
	rocksdb::PAGE_SIZE = 4;
	{
		// Without persistent cache.
		rocksdb::CloudCache c(
			std::shared_ptr<rocksdb::Cache>(rocksdb::NewLRUCache(2048*rocksdb::PAGE_SIZE)));
		c.add_file("file", 2048 * 8);
		ThreadPool pool(thread_num);

		std::atomic<int> page_id(0);
		for (int i = 0; i < thread_num; i++) {
			pool.enqueue(std::bind([](rocksdb::CloudCache* cache, std::atomic<int>* pid) {
				int page = pid->fetch_add(1);
				while (page < 2048) {
					char* buf = new char[16];
					sprintf(buf, "%04d", page);
					cache->insert("file", page, buf, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) { delete (char*)(value); });
					page = pid->fetch_add(1);
				}
			}, &c, &page_id));
		}
		pool.wait_barrier();

		for (int i = 0; i < thread_num; i++) {
			pool.enqueue(std::bind([](rocksdb::CloudCache* cache) {
				for (int j = 0; j < 2048; j++) {
					char buf[16];
					sprintf(buf, "%04d", j);
					rocksdb::Cache::Handle* h = cache->lookup("file", j);
					EQUALS(std::string(buf, 4), std::string((char*)(cache->value(h)), 4));
					cache->release(h);
				}
			}, &c));
		}
		pool.wait_barrier();
	}
	{
		// Without persistent cache, read/write mixed.
		rocksdb::CloudCache c(
			std::shared_ptr<rocksdb::Cache>(rocksdb::NewLRUCache(2048*rocksdb::PAGE_SIZE)));
		c.add_file("file", 2048 * 8);
		ThreadPool pool(thread_num);

		std::atomic<int> page_id(0);
		for (int i = 0; i < thread_num; i++) {
			pool.enqueue(std::bind([](rocksdb::CloudCache* cache, std::atomic<int>* pid) {
				int page = pid->fetch_add(1);
				std::vector<int> inserted_pages;
				while (page < 2048) {
					inserted_pages.push_back(page);
					char* buf = new char[16];
					sprintf(buf, "%04d", page);
					cache->insert("file", page, buf, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) { delete (char*)(value); });
					page = pid->fetch_add(1);

					if ((int)(inserted_pages.size()) % 16 == 0) {
						for (int j: inserted_pages) {
							char buf[16];
							sprintf(buf, "%04d", j);
							rocksdb::Cache::Handle* h = cache->lookup("file", j);
							if (h == nullptr) {
								printf("nullptr page:%d\n", j);
							}
							EQUALS(std::string(buf, 4), std::string((char*)(cache->value(h)), 4));
							cache->release(h);
						}
						inserted_pages.clear();
					}
				}
			}, &c, &page_id));
		}
		pool.wait_barrier();
	}
	{
		// With persistent cache.
		rocksdb::CloudCache c(
			std::shared_ptr<rocksdb::Cache>(rocksdb::NewLRUCache(2048*rocksdb::PAGE_SIZE)),
			16 * 1024, 512, rocksdb::Env::Default());
		c.add_file("file", 2048 * 8);
		ThreadPool pool(thread_num);

		std::atomic<int> page_id(0);
		for (int i = 0; i < thread_num; i++) {
			pool.enqueue(std::bind([](rocksdb::CloudCache* cache, std::atomic<int>* pid) {
				int page = pid->fetch_add(1);
				while (page < 2048 * 2) {
					char* buf = new char[16];
					sprintf(buf, "%04d", page);
					cache->insert("file", page, buf, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) { delete (char*)(value); });
					page = pid->fetch_add(1);
				}
			}, &c, &page_id));
		}
		pool.wait_barrier();
		std::cout << "Finish insert" << std::endl;

		for (int i = 0; i < thread_num; i++) {
			pool.enqueue(std::bind([](rocksdb::CloudCache* cache) {
				for (int j = 0; j < 2048 * 2; j++) {
					char buf[16];
					sprintf(buf, "%04d", j);
					rocksdb::Cache::Handle* h = cache->lookup("file", j);
					EQUALS(std::string(buf, 4), std::string((char*)(cache->value(h)), 4));
					cache->release(h);
				}
			}, &c));
		}
		pool.wait_barrier();
		std::cout << "Finish lookup" << std::endl;
	}
	{
		// With persistent cache, read/write mixed.
		rocksdb::CloudCache c(
			std::shared_ptr<rocksdb::Cache>(rocksdb::NewLRUCache(2048*rocksdb::PAGE_SIZE)),
				16 * 1024, 512, rocksdb::Env::Default());
		c.add_file("file", 2048 * 8);
		ThreadPool pool(thread_num);

		std::atomic<int> page_id(0);
		for (int i = 0; i < thread_num; i++) {
			pool.enqueue(std::bind([](rocksdb::CloudCache* cache, std::atomic<int>* pid) {
				int page = pid->fetch_add(1);
				std::vector<int> inserted_pages;
				while (page < 2048 * 2) {
					inserted_pages.push_back(page);
					char* buf = new char[16];
					sprintf(buf, "%04d", page);
					cache->insert("file", page, buf, rocksdb::PAGE_SIZE, [](const rocksdb::Slice& key, void* value) { delete (char*)(value); });
					page = pid->fetch_add(1);

					if ((int)(inserted_pages.size()) % 16 == 0) {
						for (int j: inserted_pages) {
							char buf[16];
							sprintf(buf, "%04d", j);
							rocksdb::Cache::Handle* h = cache->lookup("file", j);
							if (h == nullptr) {
								printf("nullptr page:%d\n", j);
							}
							EQUALS(std::string(buf, 4), std::string((char*)(cache->value(h)), 4));
							cache->release(h);
						}
						inserted_pages.clear();
					}
				}
			}, &c, &page_id));
		}
		pool.wait_barrier();
	}
}

int main() {
	test_persistent_cache();
	test_cloud_cache(-1);
	multi_thread_persistence_cache(4);
	multi_thread_test(4);
}