#include <algorithm>
#include <inttypes.h>
#include <iostream>
#include <sstream>
#include <vector>

#include "port/port.h"
#include "rocksdb/cloud/db_cloud.h"
#include "rocksdb/db.h"
#include "rocksdb/iterator.h"
#include "rocksdb/slice.h"
#include "rocksdb/options.h"
#include "util/stderr_logger.h"

using namespace std;
using namespace rocksdb;

std::string dbpath = "/tmp/cloud_write_test";
std::string bucketSuffix = "cloud-db-examples.alec";
std::string bucketPrefix = "rockset.";
std::string region = "ap-northeast-1";

int main() {
	CloudEnvOptions cloud_env_options;
	std::unique_ptr<CloudEnv> cloud_env;
	cloud_env_options.src_bucket.SetBucketName(bucketSuffix,bucketPrefix);
	cloud_env_options.dest_bucket.SetBucketName(bucketSuffix,bucketPrefix);
	cloud_env_options.keep_local_sst_files = true;
	CloudEnv* cenv;
	const std::string bucketName = bucketPrefix + bucketSuffix;
	Status s = CloudEnv::NewAwsEnv(Env::Default(),
				bucketSuffix, dbpath, region,
				bucketSuffix, dbpath, region,
				cloud_env_options, nullptr, &cenv);
	if (!s.ok()) {
		fprintf(stderr, "Unable to create cloud env in bucket %s. %s\n",
						bucketName.c_str(), s.ToString().c_str());
		return -1;
	}
	cloud_env.reset(cenv);
	Options options;
	options.env = cloud_env.get();
	options.create_if_missing = true;
	options.kv_separation = true;
	options.info_log.reset(new StderrLogger(InfoLogLevel::DEBUG_LEVEL));
	std::string persistent_cache = "";
	DBCloud* db;
	s = DBCloud::Open(options, dbpath, persistent_cache, 0, &db);
	if (!s.ok()) {
		fprintf(stderr, "Unable to open db at path %s with bucket %s. %s\n",
						dbpath.c_str(), bucketName.c_str(), s.ToString().c_str());
		return -1;
	}

	std::string value;
	db->Get(ReadOptions(), "key1", &value);
	assert(value == "value");
	delete db;
}