#include "rocksdb/db.h"
#include "rocksdb/options.h"
using namespace rocksdb;
int main() {
	DB* db;
	Options options;
	options.create_if_missing = true;
	options.kv_separation = true;
	// open DB
	Status s = DB::Open(options, "/tmp/write_test", &db);
	assert(s.ok());
	std::string value;
	db->Get(ReadOptions(), "key1", &value);
	assert(value == "value");
	delete db;
}