#include <iostream>

#include "cloud/testutil.h"
#include "rocksdb/env.h"
using namespace rocksdb;

void test_randomrw() {
	Env* env = Env::Default();
	env->DeleteFile("env_test.txt");

	std::unique_ptr<RandomRWFile> rw;
	EnvOptions opts;
	opts.use_direct_reads = true;
	opts.use_direct_writes = true;

	std::unique_ptr<WritableFile> w;
	Status st = env->NewWritableFile("env_test.txt", &w, opts);
	w.reset();

	st = env->NewRandomRWFile("env_test.txt", &rw, opts);
	EQUALS(true, st.ok());

	char buf[2048];
	sprintf(buf, "helloworld");
	st = rw->Write(0, Slice(buf, strlen(buf)));
	EQUALS(true, st.ok());
	st = rw->Write(0, Slice(buf, strlen(buf)));
	EQUALS(true, st.ok());
	sprintf(buf, "test");
	st = rw->Write(3, Slice(buf, strlen(buf)));
	EQUALS(true, st.ok());
	
	char result[2048];
	Slice rs;
	st = rw->Read(0, 10, &rs, result);
	EQUALS(true, st.ok());
	EQUALS(std::string("heltestrld"), std::string(result, 10));
}

int main() {
	test_randomrw();
}