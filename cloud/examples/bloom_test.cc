#include <string>

// #include "key_generator.h"
#include "table/block_based/full_filter_block.h"
#include "rocksdb/db.h"
#include "rocksdb/filter_policy.h"
#include "rocksdb/options.h"
#include "rocksdb/table.h"

using namespace rocksdb;

int main() {
  // {
  //   const FilterPolicy* bfp = NewBloomFilterPolicy(10, false);
  //   FullFilterBlockBuilder ffbb(nullptr, true, bfp->GetFilterBitsBuilder());

  //   SequentialKeyGenerator skg1(10);
  //   while (skg1.Next()) {
  //     ffbb.Add(skg1.Get());
  //   }
  //   Slice content = ffbb.Finish();
  //   printf("filter content length:%ld\n", content.size());

  //   FilterBitsReader* fbr = bfp->GetFilterBitsReader(content);
  //   SequentialKeyGenerator skg2(10);
  //   int i = 0;
  //   while (skg2.Next())
  //     printf("key%d:%d\n", i++, fbr->MayMatch(skg2.Get()));
  //   SequentialKeyGenerator skg3(10, 20);
  //   i = 10;
  //   while (skg3.Next())
  //     printf("key%d:%d\n", i++, fbr->MayMatch(skg3.Get()));
  // }
  {
    DB* db;
    Options options;
    options.create_if_missing = true;
    options.kv_separation = true;
    options.write_buffer_size = 110 << 10;
    rocksdb::BlockBasedTableOptions table_options;
    table_options.filter_policy.reset(rocksdb::NewBloomFilterPolicy(10, false));
    options.table_factory.reset(
        rocksdb::NewBlockBasedTableFactory(table_options));
    // open DB
    Status s = DB::Open(options, "/tmp/bloom_test", &db);
    assert(s.ok());
    for (int i = 0; i < 1000; i++) {
      s = db->Put(WriteOptions(), "key" + std::to_string(i), std::string("v", 100));
      assert(s.ok());
    }
    db->Flush(FlushOptions());

    std::string value;
    for (int i = 0; i < 1000; i++) {
      db->Get(ReadOptions(), "key" + std::to_string(i), &value);
      assert(value == std::string("v", 100));
    }
    delete db;
  }
}