#include <algorithm>
#include <inttypes.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <unistd.h>

#include "cloud/cloud_cache.h"
#include "port/port.h"
#include "rocksdb/cloud/db_cloud.h"
#include "rocksdb/db.h"
#include "rocksdb/iterator.h"
#include "rocksdb/slice.h"
#include "rocksdb/options.h"
#include "util/gflags_compat.h"
#include "util/random.h"

using namespace std;
using namespace rocksdb;
using GFLAGS_NAMESPACE::ParseCommandLineFlags;

DEFINE_string(
	op,
	"fillseq,"
	"fillrandom,"
	"readseq,"
	"readrandom,"
	"delete",
	"");

DEFINE_string(dbpath, "/tmp/bench_cloud", "DB path");
DEFINE_int32(key_size, 16, "size of each key");
DEFINE_int32(value_size, 100, "Size of each value");
DEFINE_int64(num, 1000000, "Number of key/values to place in database");
DEFINE_int64(batch_size, 1, "Batch size");
DEFINE_double(compression_ratio, 0.5, "Arrange to generate values that shrink"
			  " to this fraction of their original size after compression");
DEFINE_int64(seed, 0, "Seed base for random number generators. "
			 "When 0 it is deterministic.");
DEFINE_bool(verify_checksum, true,
            "Verify checksum for every block read"
            " from storage");
DEFINE_int64(checkpoint_itvl, 63, "Record duration for batch of operations");

DEFINE_string(bucket_prefix, "rockset.", "bucket prefix");
DEFINE_string(bucket_suffix, "cloud-db-examples.alec", "bucket suffix");
DEFINE_string(region, "ap-northeast-1", "AWS region");
DEFINE_bool(keep_sst, true, "");

class Timer {
public:
	typedef std::chrono::high_resolution_clock Time;
	typedef std::chrono::milliseconds ms;
	typedef std::chrono::nanoseconds ns;
	typedef std::chrono::duration<float> fsec;

	Timer()=default;
	int stop() { return std::chrono::duration_cast<ms>(Time::now() - last_stop_).count(); }
	uint64_t stop_nano() { return std::chrono::duration_cast<ns>(Time::now() - last_stop_).count(); }
	void start() {
		start_time_ = Time::now();
		last_stop_ = Time::now();
	}
	void resume() { last_stop_ = Time::now(); }
	int since_start() { return std::chrono::duration_cast<ms>(Time::now() - start_time_).count(); }
	uint64_t since_start_nano() { return std::chrono::duration_cast<ns>(Time::now() - start_time_).count(); }

private:
	chrono::time_point<chrono::high_resolution_clock> start_time_;
	chrono::time_point<chrono::high_resolution_clock> last_stop_;
};

class RandomGenerator {
private:
	std::string data_;
	unsigned int pos_;

	Slice RandomString(Random* rnd, int len, std::string* dst) {
		dst->resize(len);
		for (int i = 0; i < len; i++) {
			(*dst)[i] = static_cast<char>(' ' + rnd->Uniform(95));  // ' ' .. '~'
		}
		return Slice(*dst);
	}

	Slice CompressibleString(Random* rnd, double compressed_fraction,
							int len, std::string* dst) {
		int raw = static_cast<int>(len * compressed_fraction);
		if (raw < 1) raw = 1;
		std::string raw_data;
		RandomString(rnd, raw, &raw_data);

		// Duplicate the random data until we have filled "len" bytes
		dst->clear();
		while (dst->size() < (unsigned int)len) {
			dst->append(raw_data);
		}
		dst->resize(len);
		return Slice(*dst);
	}

public:
	RandomGenerator() {
		// We use a limited amount of data over and over again and ensure
		// that it is larger than the compression window (32KB), and also
		// large enough to serve all typical value sizes we want to write.
		Random rnd(301);
		std::string piece;
		while (data_.size() < (unsigned)std::max(1048576, FLAGS_value_size)) {
			CompressibleString(&rnd, FLAGS_compression_ratio, 100, &piece);
			data_.append(piece);
		}
		pos_ = 0;
	}

	Slice Generate(unsigned int len) {
		assert(len <= data_.size());
		if (pos_ + len > data_.size()) {
			pos_ = 0;
		}
		pos_ += len;
		return Slice(data_.data() + pos_ - len, len);
	}

	Slice GenerateWithTTL(unsigned int len) {
		assert(len <= data_.size());
		if (pos_ + len > data_.size()) {
			pos_ = 0;
		}
		pos_ += len;
		return Slice(data_.data() + pos_ - len, len);
	}
};

enum WriteMode {
	RANDOM, SEQUENTIAL, UNIQUE_RANDOM
};

class KeyGenerator {
public:
	KeyGenerator(Random64* rand, WriteMode mode, uint64_t num,
		uint64_t /*num_per_set*/ = 64 * 1024)
			: rand_(rand), mode_(mode), num_(num), next_(0) {
		if (mode_ == UNIQUE_RANDOM) {
		// NOTE: if memory consumption of this approach becomes a concern,
		// we can either break it into pieces and only random shuffle a section
		// each time. Alternatively, use a bit map implementation
		// (https://reviews.facebook.net/differential/diff/54627/)
		values_.resize(num_);
		for (uint64_t i = 0; i < num_; ++i) {
		  values_[i] = i;
		}
		std::shuffle(
			values_.begin(), values_.end(),
			std::default_random_engine(static_cast<unsigned int>(FLAGS_seed)));
		}
	}

	uint64_t Next() {
		switch (mode_) {
		case SEQUENTIAL:
			return next_++;
		case RANDOM:
			return rand_->Next() % num_;
		case UNIQUE_RANDOM:
			assert(next_ < num_);
			return values_[next_++];
		}
		assert(false);
		return std::numeric_limits<uint64_t>::max();
	}

private:
	Random64* rand_;
	WriteMode mode_;
	const uint64_t num_;
	uint64_t next_;
	std::vector<uint64_t> values_;
};

// Generate key according to the given specification and random number.
// The resulting key will have the following format (if keys_per_prefix_
// is positive), extra trailing bytes are either cut off or padded with '0'.
// The prefix value is derived from key value.
//   ----------------------------
//   | prefix 00000 | key 00000 |
//   ----------------------------
// If keys_per_prefix_ is 0, the key is simply a binary representation of
// random number followed by trailing '0's
//   ----------------------------
//   |        key 00000         |
//   ----------------------------
void GenerateKeyFromInt(uint64_t v, int64_t num_keys, Slice* key) {
	char* start = const_cast<char*>(key->data());
	char* pos = start;

	int bytes_to_fill = std::min(FLAGS_key_size - static_cast<int>(pos - start), 8);
	if (port::kLittleEndian) {
		for (int i = 0; i < bytes_to_fill; ++i) {
			pos[i] = (v >> ((bytes_to_fill - i - 1) << 3)) & 0xFF;
		}
	} else {
		memcpy(pos, static_cast<void*>(&v), bytes_to_fill);
	}
	pos += bytes_to_fill;
	if (FLAGS_key_size > pos - start) {
		memset(pos, '0', FLAGS_key_size - (pos - start));
	}
}

Slice AllocateKey(std::unique_ptr<const char[]>* key_guard) {
	char* data = new char[FLAGS_key_size];
	const char* const_data = data;
	key_guard->reset(const_data);
	return Slice(key_guard->get(), FLAGS_key_size);
}

int64_t GetRandomKey(Random64* rand) {
	return rand->Next() % FLAGS_num;
}

void test_read_sequential(DB* db) {
	ReadOptions roptions(FLAGS_verify_checksum, true);
	vector<uint64_t> durations(FLAGS_num / FLAGS_checkpoint_itvl);
	int counter = 0;
	Timer timer;
	timer.start();
	Iterator* iter = db->NewIterator(roptions);
	int64_t i = 0;
	int64_t bytes = 0;
	for (iter->SeekToFirst(); i < FLAGS_num && iter->Valid(); iter->Next()) {
		bytes += iter->key().size() + iter->value().size();
		++i;

		if ((i & FLAGS_checkpoint_itvl) == 0) {
			durations[counter++] = timer.stop_nano();
			timer.resume();
		}
	}
	cout << "Read sequential:" << FLAGS_num << " Total:" << timer.since_start_nano() << "ns" << endl;
	sort(durations.begin(), durations.end());
	cout << "Batch:" << FLAGS_checkpoint_itvl + 1
		 << " min:" << durations.front()
		 << "ns max:" << durations.back()
		 << "ns med:" << durations[FLAGS_num / FLAGS_checkpoint_itvl / 2]
		 << "ns avg:" << accumulate(durations.begin(), durations.end(), 0.0) / (double)(durations.size()) << "ns" << endl;
}

void test_read_random(DB* db) {
	Random64 r(0);
	int64_t read = 0;
	int64_t found = 0;
	int64_t bytes = 0;
	ReadOptions roptions(FLAGS_verify_checksum, true);
	std::unique_ptr<const char[]> key_guard;
	Slice key = AllocateKey(&key_guard);
	PinnableSlice pinnable_val;

	vector<uint64_t> durations(FLAGS_num / FLAGS_checkpoint_itvl);
	int counter = 0;
	Timer timer;
	timer.start();
	while (read < FLAGS_num) {
		// DBWithColumnFamilies* db_with_cfh = SelectDBWithCfh(thread);
		// We use same key_rand as seed for key and column family so that we can
		// deterministically find the cfh corresponding to a particular key, as it
		// is done in DoWrite method.
		int64_t key_rand = GetRandomKey(&r);
		GenerateKeyFromInt(key_rand, FLAGS_num, &key);
		read++;
		Status s;
		pinnable_val.Reset();
		s = db->Get(roptions,
				db->DefaultColumnFamily(), key,
				&pinnable_val);
		if (s.ok()) {
			found++;
			bytes += key.size() + pinnable_val.size();
		} else if (!s.IsNotFound()) {
			fprintf(stderr, "Get returned an error: %s\n", s.ToString().c_str());
			abort();
		}

		if ((read & FLAGS_checkpoint_itvl) == 0) {
			durations[counter++] = timer.stop_nano();
			timer.resume();
		}
	}
	cout << "Read random:" << FLAGS_num << " Total:" << timer.since_start_nano() << "ns" << endl;
	sort(durations.begin(), durations.end());
	cout << "Batch:" << FLAGS_checkpoint_itvl + 1
		 << " min:" << durations.front()
		 << "ns max:" << durations.back()
		 << "ns med:" << durations[FLAGS_num / FLAGS_checkpoint_itvl / 2]
		 << "ns avg:" << accumulate(durations.begin(), durations.end(), 0.0) / (double)(durations.size()) << "ns" << endl;
}

void test_write(DB* db, WriteMode mode) {
	Random64 r(0);
	std::unique_ptr<const char[]> key_guard;
	Slice key = AllocateKey(&key_guard);
	RandomGenerator gen;
	KeyGenerator kg(&r, mode, FLAGS_num);

	WriteBatch batch;
	vector<uint64_t> durations(FLAGS_num / FLAGS_checkpoint_itvl);
	int counter = 0;
	Timer timer;
	timer.start();
	for (int i = 1; i <= FLAGS_num; i++) {
		int64_t rand_num = kg.Next();
		GenerateKeyFromInt(rand_num, FLAGS_num, &key);
		batch.Clear();
		batch.Put(key, gen.Generate(FLAGS_value_size));
		db->Write(WriteOptions(), &batch);
		if ((i & FLAGS_checkpoint_itvl) == 0) {
			durations[counter++] = timer.stop_nano();
			timer.resume();
		}
	}
	cout << "Finish: " << timer.since_start_nano() << "ns" << endl;
	db->Flush(FlushOptions());
	cout << "Write:" << FLAGS_num << " Total:" << timer.since_start_nano() << "ns" << endl;
	sort(durations.begin(), durations.end());
	cout << "Batch:" << FLAGS_checkpoint_itvl + 1
		 << " min:" << durations.front()
		 << "ns max:" << durations.back()
		 << "ns med:" << durations[FLAGS_num / FLAGS_checkpoint_itvl / 2]
		 << "ns avg:" << accumulate(durations.begin(), durations.end(), 0.0) / (double)(durations.size()) << "ns" << endl;
}

void print_key(const Slice& s) {
	printf("key: ");
	for (int i = 0; i < s.size(); i++)
		printf("%d ", s.data()[i]);
	printf("\n");
}

void test_random_generator() {
	Random64 r(0);
	std::unique_ptr<const char[]> key_guard;
	Slice key = AllocateKey(&key_guard);
	RandomGenerator gen;

	int num = 10;
	cout << "port::kLittleEndian? " << port::kLittleEndian << endl;
	{
		KeyGenerator kg(&r, RANDOM, 10);
		for (int i = 0; i < num; i++) {
			int64_t rand_num = kg.Next();
			cout << "rand_num: " << rand_num << endl;
			// printf("rand num: %d\n", rand_num);
			GenerateKeyFromInt(rand_num, FLAGS_num, &key);
			print_key(key);
			cout << "value: " << gen.Generate(FLAGS_value_size).size() << endl;
		}
	}
	cout << endl;
	{
		KeyGenerator kg(&r, SEQUENTIAL, 10);
		for (int i = 0; i < num; i++) {
			int64_t rand_num = kg.Next();
			cout << "rand_num: " << rand_num << endl;
			// printf("rand num: %d\n", rand_num);
			GenerateKeyFromInt(rand_num, FLAGS_num, &key);
			print_key(key);
			cout << "value: " << gen.Generate(FLAGS_value_size).size() << endl;
		}
	}
}

int main(int argc, char** argv) {
	ParseCommandLineFlags(&argc, &argv, true);
	CloudEnvOptions cloud_env_options;
	std::unique_ptr<CloudEnv> cloud_env;
	cloud_env_options.src_bucket.SetBucketName(FLAGS_bucket_suffix,FLAGS_bucket_prefix);
	cloud_env_options.dest_bucket.SetBucketName(FLAGS_bucket_suffix,FLAGS_bucket_prefix);
	cloud_env_options.keep_local_sst_files = FLAGS_keep_sst;
	cloud_env_options.keep_sst_levels = 2;
	CloudEnv* cenv;
	const std::string bucketName = FLAGS_bucket_prefix + FLAGS_bucket_suffix;
	Status s = CloudEnv::NewAwsEnv(Env::Default(),
				FLAGS_bucket_suffix, FLAGS_dbpath, FLAGS_region,
				FLAGS_bucket_suffix, FLAGS_dbpath, FLAGS_region,
				cloud_env_options, nullptr, &cenv);
				// NewLRUCache(256*1024*1024));
	if (!s.ok()) {
		fprintf(stderr, "Unable to create cloud env in bucket %s. %s\n",
						bucketName.c_str(), s.ToString().c_str());
		return -1;
	}
	cloud_env.reset(cenv);
	Options options;
	options.write_buffer_size = 8 << 20; // 8 MB.
  options.max_bytes_for_level_base = 32 * 1048576; // 32 MB
	options.env = cloud_env.get();
	options.create_if_missing = true;
	std::string persistent_cache = "";
	options.aws_use_cloud_cache = true;
	options.keep_levels = 2;
  options.ccache = std::shared_ptr<CloudCache>(new CloudCache(
      std::shared_ptr<Cache>(NewLRUCache(512 * 1024 * 1024)),
      512 * 1024 * 1024/*pcache size*/, 1024 * 1024/*block size*/,
      cloud_env->GetBaseEnv()));
	DBCloud* db;
	s = DBCloud::Open(options, FLAGS_dbpath, persistent_cache, 0, &db);
	if (!s.ok()) {
		fprintf(stderr, "Unable to open db at path %s with bucket %s. %s\n",
						FLAGS_dbpath.c_str(), bucketName.c_str(), s.ToString().c_str());
		return -1;
	}

	std::stringstream benchmark_stream(FLAGS_op);
	std::string name;
	while(std::getline(benchmark_stream, name, ',')) {
		if (name == "fillseq")
			test_write(db, SEQUENTIAL);
		else if (name == "fillrandom")
			test_write(db, RANDOM);
		else if (name == "readseq")
			test_read_sequential(db);
		else if (name == "readrandom")
			test_read_random(db);
	}

	sleep(10);
	db->GetBaseDB()->PrintDefaultLevel(true, true);
	options.ccache->print_summary(true);

	delete db;
}