#pragma once
#include <vector>

namespace rocksdb {

class Bitmap {
public:
	Bitmap(int size): size_(size), btimap_((size + 7) / 8, 0) {}
	bool get(int idx) {
		int i1 = idx >> 3;
		int i2 = idx % 8;
		return (btimap_[i1] >> i2);
	}
	void set(int idx) {
		int i1 = idx >> 3;
		int i2 = idx % 8;
		btimap_[i1] |= (1 << i2);
	}
	void unset(int idx) {
		int i1 = idx >> 3;
		int i2 = idx % 8;
		btimap_[i1] &= ~(1 << i2);
	}

private:
	int size_;
	std::vector<char> btimap_;
};

}