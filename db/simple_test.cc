#include "rocksdb/db.h"
#include "rocksdb/options.h"
#include "rocksdb/status.h"
using namespace rocksdb;

int main() {
  DB* db;
  Options options;
  options.create_if_missing = true;
  options.kv_separation = true;
  // open DB
  Status s = DB::Open(options, "/tmp/simple_test", &db);
  assert(s.ok());
  s = db->Put(WriteOptions(), "key1", "value");
  assert(s.ok());
  db->Flush(FlushOptions());
  delete db;

  s = DB::Open(options, "/tmp/simple_test", &db);
  assert(s.ok());
  std::string value;
  db->Get(ReadOptions(), "key1", &value);
  assert(value == "value");

  printf("before iter\n");
  auto it = db->NewIterator(ReadOptions());
  it->SeekToFirst();
  // it->Next();
  while (it->Valid()) {
    printf("key:%s value:%s\n", it->key().ToString().c_str(), it->value().ToString().c_str());
    it->Next();
  }
  delete it;
  delete db;
}