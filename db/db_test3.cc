#include <fcntl.h>
#include <algorithm>
#include <set>
#include <thread>
#include <unordered_set>
#include <utility>
#ifndef OS_WIN
#include <unistd.h>
#endif
#ifdef OS_SOLARIS
#include <alloca.h>
#endif

#include "cache/lru_cache.h"
#include "cloud/cloud_cache.h"
#include "db/db_impl/db_impl.h"
#include "db/db_test_util.h"
#include "db/dbformat.h"
#include "db/job_context.h"
#include "db/version_set.h"
#include "db/write_batch_internal.h"
#include "env/mock_env.h"
#include "file/filename.h"
#include "memtable/hash_linklist_rep.h"
#include "monitoring/thread_status_util.h"
#include "port/port.h"
#include "port/stack_trace.h"
#include "rocksdb/cache.h"
#include "rocksdb/cloud/db_cloud.h"
#include "rocksdb/compaction_filter.h"
#include "rocksdb/convenience.h"
#include "rocksdb/db.h"
#include "rocksdb/env.h"
#include "rocksdb/experimental.h"
#include "rocksdb/filter_policy.h"
#include "rocksdb/options.h"
#include "rocksdb/perf_context.h"
#include "rocksdb/slice.h"
#include "rocksdb/slice_transform.h"
#include "rocksdb/snapshot.h"
#include "rocksdb/table.h"
#include "rocksdb/table_properties.h"
#include "rocksdb/thread_status.h"
#include "rocksdb/utilities/checkpoint.h"
#include "rocksdb/utilities/optimistic_transaction_db.h"
#include "rocksdb/utilities/write_batch_with_index.h"
#include "table/block_based/block_based_table_factory.h"
#include "table/mock_table.h"
#include "table/plain/plain_table_factory.h"
#include "table/scoped_arena_iterator.h"
#include "test_util/sync_point.h"
#include "test_util/testharness.h"
#include "test_util/testutil.h"
#include "util/compression.h"
#include "util/file_reader_writer.h"
#include "util/mutexlock.h"
#include "util/rate_limiter.h"
#include "util/string_util.h"
#include "util/thread_pool.h"
#include "utilities/merge_operators.h"

namespace rocksdb {

class DBTest : public testing::Test {
 public:
  static std::string Key(int i) {
    char buf[100];
    snprintf(buf, sizeof(buf), "key%06d", i);
    return std::string(buf);
  }
  static std::string RandomString(Random* rnd, int len) {
    std::string r;
    test::RandomString(rnd, len, &r);
    return r;
  }
};

bool __db_test3_kv_separation = true;

// Original max_bytes_for_level_base.
TEST_F(DBTest, PartitionTest1) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTest1", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  int counter = 0;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      keys.push_back(Key(counter++));
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
    db->PrintDefaultLevel();
  }

  // Overwrite.
  counter = 0;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      values[counter] = RandomString(&rnd, (i == 99) ? 1 : 990);
      ASSERT_OK(db->Put(WriteOptions(), keys[counter], values[counter]));
      counter++;
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
    db->PrintDefaultLevel();
  }

  counter = 0;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string val;
      ASSERT_OK(db->Get(ReadOptions(), keys[counter], &val));
      ASSERT_EQ(values[counter], val);
      counter++;
    }
  }

  delete db;
}

TEST_F(DBTest, PartitionTestToy) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 1024 << 10; // 1MB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTestToy", &db));

  ASSERT_OK(db->Put(WriteOptions(), "key1", "value1"));
  ASSERT_OK(db->Put(WriteOptions(), "key2", "value2"));
  ASSERT_OK(db->Put(WriteOptions(), "key3", "value3"));
  ASSERT_OK(db->Put(WriteOptions(), "key4", "value4"));
  ASSERT_OK(db->Flush(FlushOptions()));

  std::string val;
  ASSERT_OK(db->Get(ReadOptions(), "key1", &val));
  ASSERT_EQ("value1", val);
  ASSERT_OK(db->Get(ReadOptions(), "key2", &val));
  ASSERT_EQ("value2", val);
  ASSERT_OK(db->Get(ReadOptions(), "key3", &val));
  ASSERT_EQ("value3", val);
  ASSERT_OK(db->Get(ReadOptions(), "key4", &val));
  ASSERT_EQ("value4", val);

  // Close and re-open.
  delete db;

  ASSERT_OK(DB::Open(options, "/tmp/PartitiohnTestToy", &db));
  ASSERT_OK(db->Get(ReadOptions(), "key1", &val));
  ASSERT_EQ("value1", val);
  ASSERT_OK(db->Get(ReadOptions(), "key2", &val));
  ASSERT_EQ("value2", val);
  ASSERT_OK(db->Get(ReadOptions(), "key3", &val));
  ASSERT_EQ("value3", val);
  ASSERT_OK(db->Get(ReadOptions(), "key4", &val));
  ASSERT_EQ("value4", val);
}

// max_bytes_for_level_base=1MB.
TEST_F(DBTest, PartitionTest2) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 1024 << 10; // 1MB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTest2", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  int counter = 0;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      keys.push_back(Key(counter++));
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel();
  printf("counter:%d %lu %lu\n", counter, keys.size(), values.size());

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      keys.push_back(Key(counter++));
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel();
  printf("counter:%d %lu %lu\n", counter, keys.size(), values.size());

  for (int i = 0; i < counter; i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }

  delete db;
}

// max_bytes_for_level_base=1MB.
TEST_F(DBTest, PartitionTestHotL1) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 1024 << 10; // 1MB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTestHotL1", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  std::set<std::string> existing_keys;
  int counter = 0;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      counter++;
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel();
  printf("counter:%d %lu %lu\n", counter, keys.size(), values.size());

  for (int i = 0; i < counter; i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      counter++;
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel();
  printf("counter:%d %lu %lu\n", counter, keys.size(), values.size());

  for (int i = 0; i < counter; i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      counter++;
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel();
  printf("counter:%d %lu %lu\n", counter, keys.size(), values.size());

  for (int i = 0; i < counter; i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }

  delete db;
}

// max_bytes_for_level_base=100KB.
TEST_F(DBTest, PartitionTest3_1) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 100 << 10; // 100KB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTest3_1", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  std::set<std::string> existing_keys;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("%lu %lu\n", keys.size(), values.size());

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("%lu %lu\n", keys.size(), values.size());

  for (size_t i = 0; i < keys.size(); i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }

  // std::string val;
  // ASSERT_OK(db->Get(ReadOptions(), keys[1157], &val));
  // ASSERT_EQ(values[1157], val);

  delete db;
}

// max_bytes_for_level_base=10KB.
TEST_F(DBTest, PartitionTest3_2) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 10 << 10; // 10KB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTest3_2", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  std::set<std::string> existing_keys;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("%lu %lu\n", keys.size(), values.size());

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      Status s = db->Put(WriteOptions(), keys.back(), values.back());
      if (!s.ok())
        db->PrintDefaultLevel(true);
      ASSERT_OK(s);
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("%lu %lu\n", keys.size(), values.size());
  sleep(10);
  db->PrintDefaultLevel(true);

  for (size_t i = 0; i < keys.size(); i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }

  printf("Finish reading\n");
  db->PrintDefaultLevel(true);
  sleep(10);
  db->PrintDefaultLevel(true);
  delete db;
}

TEST_F(DBTest, PartitionTest3_2_concurrent) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 10 << 10; // 10KB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTest3_2_concurrent", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  std::set<std::string> existing_keys;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("%lu %lu\n", keys.size(), values.size());

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      Status s = db->Put(WriteOptions(), keys.back(), values.back());
      if (!s.ok())
        db->PrintDefaultLevel(true);
      ASSERT_OK(s);
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("%lu %lu\n", keys.size(), values.size());
  sleep(10);
  db->PrintDefaultLevel(true);

  TestThreadPool pool(8);
  auto ro = ReadOptions();
  ro.use_concurrent_read = true;
  ro.pool = &pool;
  for (size_t i = 0; i < keys.size(); i++) {
    std::string val;
    ASSERT_OK(db->Get(ro, keys[i], &val));
    ASSERT_EQ(values[i], val);
  }

  printf("Finish reading\n");
  pool.wait_until_empty();
  db->PrintDefaultLevel(true);
  sleep(10);
  db->PrintDefaultLevel(true);
  delete db;
}

// max_bytes_for_level_base=100KB.
// Test scan.
TEST_F(DBTest, PartitionTest3_3) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 100 << 10; // 100KB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTest3_3", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::pair<std::string, std::string>> pairs;
  std::set<std::string> existing_keys;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      pairs.emplace_back(tmp_key, RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), pairs.back().first, pairs.back().second));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("num of pairs: %lu\n", pairs.size());

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      pairs.emplace_back(tmp_key, RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), pairs.back().first, pairs.back().second));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel(true);
  printf("num of pairs: %lu\n", pairs.size());

  sleep(5);
  const Comparator* cmp = db->DefaultColumnFamily()->GetComparator();
  std::sort(pairs.begin(), pairs.end(), [&](const std::pair<std::string, std::string>& lhs, const std::pair<std::string, std::string>& rhs) -> bool {
    return cmp->Compare(Slice(lhs.first), Slice(rhs.first)) < 0;
  });
  Iterator* it = db->NewIterator(ReadOptions(), db->DefaultColumnFamily());
  it->Seek(pairs.front().first);
  int i = 0;
  while (it->Valid()) {
    // if (pairs[i].first != std::string(it->key().data(), it->key().size()))
    //   printf("i:%d exp:[%s] got:[%s]\n", i, Slice(pairs[i].first).ToString(true).c_str(), Slice(it->key().data(), it->key().size()).ToString(true).c_str());
    // printf("%s\n", Slice(pairs[i].first).ToString(true).c_str());
    ASSERT_EQ(pairs[i].first, std::string(it->key().data(), it->key().size()));
    ASSERT_EQ(pairs[i].second, std::string(it->value().data(), it->value().size()));
    it->Next();
    i++;
  }
  delete it;

  delete db;
}

// Test with posix cache.
TEST_F(DBTest, PartitionTest4) {
  Options options;
  options.keep_levels = 2;
  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 100 << 10; // 100KB
  options.env = Env::Default();
  options.kv_separation = __db_test3_kv_separation;
  options.posix_use_cloud_cache = true;
  options.ccache = std::shared_ptr<CloudCache>(new CloudCache(
      std::shared_ptr<Cache>(NewLRUCache(1 * 1024 * 1024)),
      1 * 1024 * 1024/*pcache size*/, 128 * 1024/*block size*/,
      Env::Default()));
  DB* db;
  ASSERT_OK(DB::Open(options, "/tmp/PartitionTest4", &db));

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  std::set<std::string> existing_keys;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel();
  printf("keys.size()=%lu values.size()=%lu\n", keys.size(), values.size());
  options.ccache->print_summary();

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }
  db->PrintDefaultLevel();
  printf("keys.size()=%lu values.size()=%lu\n", keys.size(), values.size());
  options.ccache->print_summary();

  for (size_t i = 0; i < keys.size(); i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }
  db->PrintDefaultLevel();
  options.ccache->print_summary();

  delete db;
}

// Test with cloud.
TEST_F(DBTest, PartitionTest5) {
  Options options;
  options.keep_levels = 2;
  std::string dbpath = "/tmp/PartitionTest5";
  std::string region = "ap-northeast-1";
  std::string bucket_prefix = "rockset.";
  std::string bucket_suffix = "cloud-db-examples.alec";

  CloudEnvOptions cloud_env_options;
  std::unique_ptr<CloudEnv> cloud_env;
  cloud_env_options.src_bucket.SetBucketName(bucket_suffix,bucket_prefix);
  cloud_env_options.dest_bucket.SetBucketName(bucket_suffix,bucket_prefix);
  cloud_env_options.keep_local_sst_files = true;
  cloud_env_options.keep_sst_levels = 2;
  CloudEnv* cenv;
  const std::string bucketName = bucket_suffix + bucket_prefix;
  Status s = CloudEnv::NewAwsEnv(Env::Default(),
        bucket_suffix, dbpath, region,
        bucket_suffix, dbpath, region,
        cloud_env_options, nullptr, &cenv);
        // NewLRUCache(256*1024*1024));
  ASSERT_OK(s);
  cloud_env.reset(cenv);

  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 100 << 10; // 100KB
  options.env = cloud_env.get();
  options.kv_separation = __db_test3_kv_separation;
  options.aws_use_cloud_cache = true;
  options.ccache = std::shared_ptr<CloudCache>(new CloudCache(
      std::shared_ptr<Cache>(NewLRUCache(
        1 * 1024 * 1024,
        -1/*num_shard_bits*/,
        false/*strict_capacity_limit*/,
        0.3/*high_pri_pool_ratio*/
      )),
      1 * 1024 * 1024/*pcache size*/, 128 * 1024/*block size*/,
      cloud_env->GetBaseEnv()));
  std::string persistent_cache = "";
  DBCloud* db;
  s = DBCloud::Open(options, dbpath, persistent_cache, 0, &db);

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  std::set<std::string> existing_keys;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
    options.ccache->print_summary(true);
  }
  db->PrintDefaultLevel();
  printf("keys.size()=%lu values.size()=%lu\n", keys.size(), values.size());
  options.ccache->print_summary(true);

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }

  db->Flush(FlushOptions());

  db->PrintDefaultLevel();
  printf("keys.size()=%lu values.size()=%lu\n", keys.size(), values.size());
  options.ccache->print_summary(true);

  for (size_t i = 0; i < keys.size(); i++) {
    std::string val;
    ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
    ASSERT_EQ(values[i], val);
  }
  db->PrintDefaultLevel();
  options.ccache->print_summary(true);
  printf("--------- PartitionTest5 Finished ---------\n");

  delete db;
}

TEST_F(DBTest, PartitionTest5Concurrent) {
  Options options;
  options.keep_levels = 2;
  std::string dbpath = "/tmp/PartitionTest5Concurrent";
  std::string region = "ap-northeast-1";
  std::string bucket_prefix = "rockset.";
  std::string bucket_suffix = "cloud-db-examples.alec";

  CloudEnvOptions cloud_env_options;
  std::unique_ptr<CloudEnv> cloud_env;
  cloud_env_options.src_bucket.SetBucketName(bucket_suffix,bucket_prefix);
  cloud_env_options.dest_bucket.SetBucketName(bucket_suffix,bucket_prefix);
  cloud_env_options.keep_local_sst_files = true;
  cloud_env_options.keep_sst_levels = 2;
  CloudEnv* cenv;
  const std::string bucketName = bucket_suffix + bucket_prefix;
  Status s = CloudEnv::NewAwsEnv(Env::Default(),
        bucket_suffix, dbpath, region,
        bucket_suffix, dbpath, region,
        cloud_env_options, nullptr, &cenv);
        // NewLRUCache(256*1024*1024));
  ASSERT_OK(s);
  cloud_env.reset(cenv);

  options.create_if_missing = true;
  options.compaction_style = kCompactionStyleLevel;
  options.write_buffer_size = 110 << 10;  // 110KB
  options.arena_block_size = 4 << 10;
  options.level0_file_num_compaction_trigger = 2;
  options.max_bytes_for_level_base = 100 << 10; // 100KB
  options.env = cloud_env.get();
  options.kv_separation = __db_test3_kv_separation;
  options.aws_use_cloud_cache = true;
  options.ccache = std::shared_ptr<CloudCache>(new CloudCache(
      std::shared_ptr<Cache>(NewLRUCache(
        1 * 1024 * 1024,
        -1/*num_shard_bits*/,
        false/*strict_capacity_limit*/,
        0.3/*high_pri_pool_ratio*/
      )),
      1 * 1024 * 1024/*pcache size*/, 128 * 1024/*block size*/,
      cloud_env->GetBaseEnv()));
  std::string persistent_cache = "";
  DBCloud* db;
  s = DBCloud::Open(options, dbpath, persistent_cache, 0, &db);

  int num_files = 10;
  int num_keys = 100;
  Random rnd(301);
  std::vector<std::string> keys;
  std::vector<std::string> values;
  std::set<std::string> existing_keys;
  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
    options.ccache->print_summary(true);
  }
  db->PrintDefaultLevel();
  printf("keys.size()=%lu values.size()=%lu\n", keys.size(), values.size());
  options.ccache->print_summary(true);

  for (int j = 0; j < num_files; j++) {
    for (int i = 0; i < num_keys; i++) {
      std::string tmp_key;
      while (true) {
        tmp_key = RandomString(&rnd, 9);
        if (existing_keys.find(tmp_key) == existing_keys.end()) {
          existing_keys.insert(tmp_key);
          break;
        }
      }
      keys.push_back(tmp_key);
      values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
      ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
    }
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForFlushMemTable();
    // reinterpret_cast<DBImpl*>(db)->TEST_WaitForCompact();
    printf("************ Write File %d *************\n", j);
  }

  db->Flush(FlushOptions());

  db->PrintDefaultLevel();
  printf("keys.size()=%lu values.size()=%lu\n", keys.size(), values.size());
  options.ccache->print_summary(true);

  sleep(10);
  ReadOptions ro;
  TestThreadPool pool(8);
  ro.pool = &pool;
  ro.use_concurrent_read = true;
  for (size_t i = 0; i < keys.size(); i++) {
    std::string val;
    ASSERT_OK(db->Get(ro, keys[i], &val));
    // printf("%lu\n", i);
    ASSERT_EQ(values[i], val);
  }
  pool.wait_until_empty();
  db->PrintDefaultLevel();
  options.ccache->print_summary(true);
  printf("--------- PartitionTest5Concurrent Finished ---------\n");

  delete db;
}

} // namespacce rocksdb

int main(int argc, char** argv) {
  rocksdb::port::InstallStackTraceHandler();
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
