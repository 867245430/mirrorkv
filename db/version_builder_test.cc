//  Copyright (c) 2011-present, Facebook, Inc.  All rights reserved.
//  This source code is licensed under both the GPLv2 (found in the
//  COPYING file in the root directory) and Apache 2.0 License
//  (found in the LICENSE.Apache file in the root directory).

#include <string>
#include "db/version_edit.h"
#include "db/version_set.h"
#include "logging/logging.h"
#include "test_util/testharness.h"
#include "test_util/testutil.h"
#include "util/string_util.h"

namespace rocksdb {

class VersionBuilderTest : public testing::Test {
 public:
  const Comparator* ucmp_;
  InternalKeyComparator icmp_;
  Options options_;
  ImmutableCFOptions ioptions_;
  MutableCFOptions mutable_cf_options_;
  VersionStorageInfo vstorage_;
  uint32_t file_num_;
  CompactionOptionsFIFO fifo_options_;
  std::vector<uint64_t> size_being_compacted_;

  VersionBuilderTest()
      : ucmp_(BytewiseComparator()),
        icmp_(ucmp_),
        ioptions_(options_),
        mutable_cf_options_(options_),
        vstorage_(&icmp_, ucmp_, options_.num_levels, kCompactionStyleLevel,
                  nullptr, false, options_.num_levels),
        file_num_(1) {
    mutable_cf_options_.RefreshDerivedOptions(ioptions_);
    size_being_compacted_.resize(options_.num_levels);
  }

  ~VersionBuilderTest() override {
    for (int i = 0; i < vstorage_.num_levels(); i++) {
      for (auto* f : vstorage_.LevelFiles(i)) {
        if (--f->refs == 0) {
          delete f;
        }
      }
    }
  }

  InternalKey GetInternalKey(const char* ukey,
                             SequenceNumber smallest_seq = 100) {
    return InternalKey(ukey, smallest_seq, kTypeValue);
  }

  void Add(int level, uint32_t file_number, const char* smallest,
           const char* largest, uint64_t file_size = 0, uint32_t path_id = 0,
           SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100,
           uint64_t num_entries = 0, uint64_t num_deletions = 0,
           bool sampled = false, SequenceNumber smallest_seqno = 0,
           SequenceNumber largest_seqno = 0) {
    assert(level < vstorage_.num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, path_id, file_size);
    f->smallest = GetInternalKey(smallest, smallest_seq);
    f->largest = GetInternalKey(largest, largest_seq);
    f->fd.smallest_seqno = smallest_seqno;
    f->fd.largest_seqno = largest_seqno;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = num_entries;
    f->num_deletions = num_deletions;
    vstorage_.AddFile(level, f);
    if (sampled) {
      f->init_stats_from_file = true;
      vstorage_.UpdateAccumulatedStats(f);
    }
  }

  void UpdateVersionStorageInfo() {
    vstorage_.UpdateFilesByCompactionPri(ioptions_.compaction_pri);
    vstorage_.UpdateNumNonEmptyLevels();
    vstorage_.GenerateFileIndexer();
    vstorage_.GenerateLevelFilesBrief();
    vstorage_.CalculateBaseBytes(ioptions_, mutable_cf_options_);
    vstorage_.GenerateLevel0NonOverlapping();
    vstorage_.SetFinalized();
  }
};

void UnrefFilesInVersion(VersionStorageInfo* new_vstorage) {
  for (int i = 0; i < new_vstorage->keep_levels(); i++) {
    for (auto* f : new_vstorage->LevelFiles(i)) {
      if (--f->refs == 0) {
        delete f;
      }
    }
  }
  for (int i = new_vstorage->keep_levels(); i < new_vstorage->num_levels(); i++) {
    for (auto& p : new_vstorage->LevelPartitions(i)) {
      for (auto& f : p.files_) {
        if (--f.file_metadata->refs == 0) {
          delete f.file_metadata;
        }
      }
    }
  }
}

TEST_F(VersionBuilderTest, ApplyAndSaveTo) {
  Add(0, 1U, "150", "200", 100U);

  Add(1, 66U, "150", "200", 100U);
  Add(1, 88U, "201", "300", 100U);

  Add(2, 6U, "150", "179", 100U);
  Add(2, 7U, "180", "220", 100U);
  Add(2, 8U, "221", "300", 100U);

  Add(3, 26U, "150", "170", 100U);
  Add(3, 27U, "171", "179", 100U);
  Add(3, 28U, "191", "220", 100U);
  Add(3, 29U, "221", "300", 100U);
  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddFile(2, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.DeleteFile(3, 27U);

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, options_.num_levels);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_EQ(400U, new_vstorage.NumLevelBytes(2));
  ASSERT_EQ(300U, new_vstorage.NumLevelBytes(3));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderTest, ApplyAndSaveToDynamic) {
  ioptions_.level_compaction_dynamic_level_bytes = true;

  Add(0, 1U, "150", "200", 100U, 0, 200U, 200U, 0, 0, false, 200U, 200U);
  Add(0, 88U, "201", "300", 100U, 0, 100U, 100U, 0, 0, false, 100U, 100U);

  Add(4, 6U, "150", "179", 100U);
  Add(4, 7U, "180", "220", 100U);
  Add(4, 8U, "221", "300", 100U);

  Add(5, 26U, "150", "170", 100U);
  Add(5, 27U, "171", "179", 100U);
  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddFile(3, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.DeleteFile(0, 1U);
  version_edit.DeleteFile(0, 88U);

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, options_.num_levels);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_EQ(0U, new_vstorage.NumLevelBytes(0));
  ASSERT_EQ(100U, new_vstorage.NumLevelBytes(3));
  ASSERT_EQ(300U, new_vstorage.NumLevelBytes(4));
  ASSERT_EQ(200U, new_vstorage.NumLevelBytes(5));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderTest, ApplyAndSaveToDynamic2) {
  ioptions_.level_compaction_dynamic_level_bytes = true;

  Add(0, 1U, "150", "200", 100U, 0, 200U, 200U, 0, 0, false, 200U, 200U);
  Add(0, 88U, "201", "300", 100U, 0, 100U, 100U, 0, 0, false, 100U, 100U);

  Add(4, 6U, "150", "179", 100U);
  Add(4, 7U, "180", "220", 100U);
  Add(4, 8U, "221", "300", 100U);

  Add(5, 26U, "150", "170", 100U);
  Add(5, 27U, "171", "179", 100U);
  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddFile(4, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.DeleteFile(0, 1U);
  version_edit.DeleteFile(0, 88U);
  version_edit.DeleteFile(4, 6U);
  version_edit.DeleteFile(4, 7U);
  version_edit.DeleteFile(4, 8U);

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, options_.num_levels);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_EQ(0U, new_vstorage.NumLevelBytes(0));
  ASSERT_EQ(100U, new_vstorage.NumLevelBytes(4));
  ASSERT_EQ(200U, new_vstorage.NumLevelBytes(5));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderTest, ApplyMultipleAndSaveTo) {
  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddFile(2, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.AddFile(2, 676, 0, 100U, GetInternalKey("401"),
                       GetInternalKey("450"), 200, 200, false);
  version_edit.AddFile(2, 636, 0, 100U, GetInternalKey("601"),
                       GetInternalKey("650"), 200, 200, false);
  version_edit.AddFile(2, 616, 0, 100U, GetInternalKey("501"),
                       GetInternalKey("550"), 200, 200, false);
  version_edit.AddFile(2, 606, 0, 100U, GetInternalKey("701"),
                       GetInternalKey("750"), 200, 200, false);

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, options_.num_levels);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_EQ(500U, new_vstorage.NumLevelBytes(2));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderTest, ApplyDeleteAndSaveTo) {
  UpdateVersionStorageInfo();

  EnvOptions env_options;
  VersionBuilder version_builder(env_options, nullptr, &vstorage_);
  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, options_.num_levels);

  VersionEdit version_edit;
  version_edit.AddFile(2, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.AddFile(2, 676, 0, 100U, GetInternalKey("401"),
                       GetInternalKey("450"), 200, 200, false);
  version_edit.AddFile(2, 636, 0, 100U, GetInternalKey("601"),
                       GetInternalKey("650"), 200, 200, false);
  version_edit.AddFile(2, 616, 0, 100U, GetInternalKey("501"),
                       GetInternalKey("550"), 200, 200, false);
  version_edit.AddFile(2, 606, 0, 100U, GetInternalKey("701"),
                       GetInternalKey("750"), 200, 200, false);
  version_builder.Apply(&version_edit);

  VersionEdit version_edit2;
  version_edit.AddFile(2, 808, 0, 100U, GetInternalKey("901"),
                       GetInternalKey("950"), 200, 200, false);
  version_edit2.DeleteFile(2, 616);
  version_edit2.DeleteFile(2, 636);
  version_edit.AddFile(2, 806, 0, 100U, GetInternalKey("801"),
                       GetInternalKey("850"), 200, 200, false);
  version_builder.Apply(&version_edit2);

  version_builder.SaveTo(&new_vstorage);

  ASSERT_EQ(300U, new_vstorage.NumLevelBytes(2));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderTest, EstimatedActiveKeys) {
  const uint32_t kTotalSamples = 20;
  const uint32_t kNumLevels = 5;
  const uint32_t kFilesPerLevel = 8;
  const uint32_t kNumFiles = kNumLevels * kFilesPerLevel;
  const uint32_t kEntriesPerFile = 1000;
  const uint32_t kDeletionsPerFile = 100;
  for (uint32_t i = 0; i < kNumFiles; ++i) {
    Add(static_cast<int>(i / kFilesPerLevel), i + 1,
        ToString((i + 100) * 1000).c_str(),
        ToString((i + 100) * 1000 + 999).c_str(),
        100U,  0, 100, 100,
        kEntriesPerFile, kDeletionsPerFile,
        (i < kTotalSamples));
  }
  // minus 2X for the number of deletion entries because:
  // 1x for deletion entry does not count as a data entry.
  // 1x for each deletion entry will actually remove one data entry.
  ASSERT_EQ(vstorage_.GetEstimatedActiveKeys(),
            (kEntriesPerFile - 2 * kDeletionsPerFile) * kNumFiles);
}

class VersionBuilderPartitionTest : public testing::Test {
 public:
  const Comparator* ucmp_;
  InternalKeyComparator icmp_;
  Options options_;
  ImmutableCFOptions ioptions_;
  MutableCFOptions mutable_cf_options_;
  VersionStorageInfo vstorage_;
  uint32_t file_num_;
  CompactionOptionsFIFO fifo_options_;
  std::vector<uint64_t> size_being_compacted_;

  VersionBuilderPartitionTest()
      : ucmp_(BytewiseComparator()),
        icmp_(ucmp_),
        ioptions_(options_),
        mutable_cf_options_(options_),
        vstorage_(&icmp_, ucmp_, options_.num_levels, kCompactionStyleLevel,
                  nullptr, false, 2),
        file_num_(1) {
    mutable_cf_options_.RefreshDerivedOptions(ioptions_);
    size_being_compacted_.resize(options_.num_levels);
  }

  ~VersionBuilderPartitionTest() override {
    for (int i = 0; i < vstorage_.keep_levels(); i++) {
      for (auto* f : vstorage_.LevelFiles(i)) {
        if (--f->refs == 0) {
          delete f;
        }
      }
    }
    for (int i = vstorage_.keep_levels(); i < vstorage_.num_levels(); i++) {
      for (auto& p : vstorage_.LevelPartitions(i)) {
        for (auto& f : p.files_) {
          if (--f.file_metadata->refs == 0) {
            delete f.file_metadata;
          }
        }
      }
    }
  }

  InternalKey GetInternalKey(const char* ukey,
                             SequenceNumber smallest_seq = 100) {
    return InternalKey(ukey, smallest_seq, kTypeValue);
  }

  void Add(int level, uint32_t file_number, const char* smallest,
           const char* largest, uint64_t file_size = 0, uint32_t path_id = 0,
           SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100,
           uint64_t num_entries = 0, uint64_t num_deletions = 0,
           bool sampled = false, SequenceNumber smallest_seqno = 0,
           SequenceNumber largest_seqno = 0) {
    assert(level < vstorage_.keep_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, path_id, file_size);
    f->smallest = GetInternalKey(smallest, smallest_seq);
    f->largest = GetInternalKey(largest, largest_seq);
    f->fd.smallest_seqno = smallest_seqno;
    f->fd.largest_seqno = largest_seqno;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = num_entries;
    f->num_deletions = num_deletions;
    vstorage_.AddFile(level, f);
    if (sampled) {
      f->init_stats_from_file = true;
      vstorage_.UpdateAccumulatedStats(f);
    }
  }

  FileMetaData* AddNew(int level, uint32_t file_number, const char* smallest,
           const char* largest, const char* seg_smallest, const char*seg_largest,
           uint64_t file_size = 0, uint32_t path_id = 0,
           SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100,
           uint64_t num_entries = 0, uint64_t num_deletions = 0,
           bool sampled = false, SequenceNumber smallest_seqno = 0,
           SequenceNumber largest_seqno = 0) {
    assert(level >= vstorage_.keep_levels() &&
           level < vstorage_.num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, path_id, file_size);
    f->smallest = GetInternalKey(smallest, smallest_seq);
    f->largest = GetInternalKey(largest, largest_seq);
    f->fd.smallest_seqno = smallest_seqno;
    f->fd.largest_seqno = largest_seqno;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = num_entries;
    f->num_deletions = num_deletions;
    vstorage_.AddFileNewPartition(level, f, GetInternalKey(seg_smallest, smallest_seq), GetInternalKey(seg_largest, largest_seq));
    if (sampled) {
      f->init_stats_from_file = true;
      vstorage_.UpdateAccumulatedStats(f);
    }
    return f;
  }

  void AddNew(int level, FileMetaData* f, const char* seg_smallest, const char*seg_largest,
              SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100) {
    vstorage_.AddFileNewPartition(level, f, GetInternalKey(seg_smallest, smallest_seq), GetInternalKey(seg_largest, largest_seq));
  }

  FileMetaData* AddOld(int level, uint32_t file_number, const char* smallest,
           const char* largest, const char* seg_smallest, const char*seg_largest,
           uint64_t file_size = 0, uint32_t path_id = 0,
           SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100,
           uint64_t num_entries = 0, uint64_t num_deletions = 0,
           bool sampled = false, SequenceNumber smallest_seqno = 0,
           SequenceNumber largest_seqno = 0) {
    assert(level >= vstorage_.keep_levels() &&
           level < vstorage_.num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, path_id, file_size);
    f->smallest = GetInternalKey(smallest, smallest_seq);
    f->largest = GetInternalKey(largest, largest_seq);
    f->fd.smallest_seqno = smallest_seqno;
    f->fd.largest_seqno = largest_seqno;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = num_entries;
    f->num_deletions = num_deletions;
    vstorage_.AddFileOldPartition(level, f, GetInternalKey(seg_smallest, smallest_seq), GetInternalKey(seg_largest, largest_seq));
    if (sampled) {
      f->init_stats_from_file = true;
      vstorage_.UpdateAccumulatedStats(f);
    }
    return f;
  }

  void AddOld(int level, FileMetaData* f, const char* seg_smallest, const char*seg_largest,
              SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100) {
    vstorage_.AddFileOldPartition(level, f, GetInternalKey(seg_smallest, smallest_seq), GetInternalKey(seg_largest, largest_seq));
  }

  void AddDynamic(int level, FileMetaData* f, const char* seg_smallest, const char*seg_largest,
                  SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100) {
    vstorage_.AddFileDynamicPartition(level, f, GetInternalKey(seg_smallest, smallest_seq), GetInternalKey(seg_largest, largest_seq));
  }

  FileMetaData* CreateFMD(uint32_t file_number, const char* smallest,
                          const char* largest, uint64_t file_size = 0, uint32_t path_id = 0,
                          SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100,
                          uint64_t num_entries = 0, uint64_t num_deletions = 0,
                          SequenceNumber smallest_seqno = 0, SequenceNumber largest_seqno = 0) {
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, path_id, file_size);
    f->smallest = GetInternalKey(smallest, smallest_seq);
    f->largest = GetInternalKey(largest, largest_seq);
    f->fd.smallest_seqno = smallest_seqno;
    f->fd.largest_seqno = largest_seqno;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = num_entries;
    f->num_deletions = num_deletions;
    return f;
  }

  void AddFileSegmentMeta(uint64_t fd, const char* seg_smallest, const char*seg_largest,
                          bool valid, SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100) {
    vstorage_.AddFileSegmentMeta(fd, GetInternalKey(seg_smallest, smallest_seq), GetInternalKey(seg_largest, largest_seq), valid);
  }

  void DeleteFileSegmentMeta(uint64_t fd, const char* seg_smallest, const char*seg_largest,
                             SequenceNumber smallest_seq = 100, SequenceNumber largest_seq = 100) {
    vstorage_.DeleteFileSegmentMeta(fd, GetInternalKey(seg_smallest, smallest_seq), GetInternalKey(seg_largest, largest_seq));
  }

  void UpdateVersionStorageInfo() {
    vstorage_.UpdateFilesByCompactionPri(ioptions_.compaction_pri);
    vstorage_.UpdateNumNonEmptyLevels();
    vstorage_.GenerateFileIndexer();
    vstorage_.GenerateLevelFilesBrief();
    vstorage_.CalculateBaseBytes(ioptions_, mutable_cf_options_);
    vstorage_.GenerateLevel0NonOverlapping();
    vstorage_.SetFinalized();
  }

  bool ValidateSegmentMetas(VersionStorageInfo* vstorage, uint64_t fd,
                            const std::vector<std::pair<InternalKey, InternalKey>>& metas) {
    const auto& m = vstorage->GetFileSegmentMetas();
    auto it = m.find(fd);
    if (it == m.end())
      return false;
    else {
      int count = 0;
      if (it->second.size() != metas.size()) {
        // printf("%lu %lu\n", it->second.size(), metas.size());
        return false;
      }
      for (const auto& p : it->second) {
        printf("%lu %s %s %s %s\n", fd, p.smallest.user_key().ToString().c_str(), p.largest.user_key().ToString().c_str(),
               metas[count].first.user_key().ToString().c_str(), metas[count].second.user_key().ToString().c_str());
        if (icmp_.Compare(p.smallest, metas[count].first) != 0 || icmp_.Compare(p.largest, metas[count].second) != 0)
          return false;
        count++;
      }
    }
    return true;
  }
};

TEST_F(VersionBuilderPartitionTest, ApplyAndSaveTo) {
  Add(0, 1U, "150", "200", 100U);

  Add(1, 66U, "150", "200", 100U);
  Add(1, 88U, "201", "300", 100U);

  AddNew(2, 6U, "150", "179", "150", "179", 100U);
  AddOld(2, 7U, "180", "220", "180", "220", 100U);
  auto f1 = AddOld(2, 8U, "221", "300", "221", "300", 100U);

  AddFileSegmentMeta(6, "150", "179", true);
  AddFileSegmentMeta(7, "180", "220", true);
  AddFileSegmentMeta(8, "221", "300", true);

  auto f2 = CreateFMD(26, "150", "300");

  AddNew(3, f2, "150", "170");
  AddNew(3, f2, "171", "179");
  AddNew(3, f2, "191", "220");
  AddNew(3, f2, "221", "300");

  AddFileSegmentMeta(26, "150", "170", true);
  AddFileSegmentMeta(26, "171", "179", true);
  AddFileSegmentMeta(26, "191", "220", true);
  AddFileSegmentMeta(26, "221", "300", true);

  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddPartitionFile(2, 0, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.DeletePartitionFile(3, 0, 26U, GetInternalKey("221"), GetInternalKey("300"));
  version_edit.AddPartitionMeta(666, GetInternalKey("301"), GetInternalKey("350"), true);
  version_edit.DeletePartitionMeta(26, GetInternalKey("221"), GetInternalKey("300"));

  version_edit.DeletePartitionFile(2, 0, 8, GetInternalKey("221"), GetInternalKey("300"));
  version_edit.DeletePartitionMeta(8, GetInternalKey("221"), GetInternalKey("300"));
  version_edit.AddPartitionFile(3, 0, *f1, GetInternalKey("221"), GetInternalKey("250"));
  version_edit.AddPartitionFile(3, 0, *f1, GetInternalKey("251"), GetInternalKey("300"));
  version_edit.AddPartitionMeta(8, GetInternalKey("221"), GetInternalKey("250"), true);
  version_edit.AddPartitionMeta(8, GetInternalKey("251"), GetInternalKey("300"), true);

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, 2);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 6, {{GetInternalKey("150"), GetInternalKey("179")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 7, {{GetInternalKey("180"), GetInternalKey("220")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 8, {
    {GetInternalKey("221"), GetInternalKey("250")},
    {GetInternalKey("251"), GetInternalKey("300")},
  }));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 666, {{GetInternalKey("301"), GetInternalKey("350")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 26, {
    {GetInternalKey("150"), GetInternalKey("170")},
    {GetInternalKey("171"), GetInternalKey("179")},
    {GetInternalKey("191"), GetInternalKey("220")},
  }));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderPartitionTest, ApplyAndSaveToDynamic) {
  ioptions_.level_compaction_dynamic_level_bytes = true;

  Add(0, 1U, "150", "200", 100U, 0, 200U, 200U, 0, 0, false, 200U, 200U);
  Add(0, 88U, "201", "300", 100U, 0, 100U, 100U, 0, 0, false, 100U, 100U);

  AddNew(4, 6U, "150", "179", "150", "179", 100U);
  AddOld(4, 7U, "180", "220", "180", "220", 100U);
  AddOld(4, 8U, "221", "300", "221", "300", 100U);

  AddFileSegmentMeta(6, "150", "179", true);
  AddFileSegmentMeta(7, "180", "220", true);
  AddFileSegmentMeta(8, "221", "300", true);

  AddNew(5, 26U, "150", "170", "150", "170", 100U);
  AddOld(5, 27U, "171", "179", "171", "179", 100U);

  AddFileSegmentMeta(26, "150", "170", true);
  AddFileSegmentMeta(27, "171", "179", true);

  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddPartitionFile(3, 0, 666, 0, 100U, GetInternalKey("301"),
                                GetInternalKey("350"), 200, 200, false);
  version_edit.AddPartitionMeta(666, GetInternalKey("301"), GetInternalKey("350"), true);
  version_edit.DeleteFile(0, 1U);
  version_edit.DeleteFile(0, 88U);

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, 2);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 6, {{GetInternalKey("150"), GetInternalKey("179")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 7, {{GetInternalKey("180"), GetInternalKey("220")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 8, {{GetInternalKey("221"), GetInternalKey("300")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 666, {{GetInternalKey("301"), GetInternalKey("350")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 26, {{GetInternalKey("150"), GetInternalKey("170")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 27, {{GetInternalKey("171"), GetInternalKey("179")}}));

  ASSERT_EQ(0U, new_vstorage.NumLevelBytes(0));
  ASSERT_EQ(100U, new_vstorage.NumLevelBytes(3));
  ASSERT_EQ(300U, new_vstorage.NumLevelBytes(4));
  ASSERT_EQ(200U, new_vstorage.NumLevelBytes(5));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderPartitionTest, ApplyAndSaveToDynamic2) {
  ioptions_.level_compaction_dynamic_level_bytes = true;

  Add(0, 1U, "150", "200", 100U, 0, 200U, 200U, 0, 0, false, 200U, 200U);
  Add(0, 88U, "201", "300", 100U, 0, 100U, 100U, 0, 0, false, 100U, 100U);

  AddNew(4, 6U, "150", "179", "150", "179", 100U);
  AddOld(4, 7U, "180", "220", "180", "220", 100U);
  AddOld(4, 8U, "221", "300", "221", "300", 100U);

  AddFileSegmentMeta(6, "150", "179", true);
  AddFileSegmentMeta(7, "180", "220", true);
  AddFileSegmentMeta(8, "221", "300", true);

  AddNew(5, 26U, "150", "170", "150", "170", 100U);
  AddOld(5, 27U, "171", "179", "171", "179", 100U);

  AddFileSegmentMeta(26, "150", "170", true);
  AddFileSegmentMeta(27, "171", "179", true);

  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddPartitionFile(4, 0, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.AddPartitionMeta(666, GetInternalKey("301"), GetInternalKey("350"), true);
  version_edit.DeleteFile(0, 1U);
  version_edit.DeleteFile(0, 88U);
  version_edit.DeletePartitionFile(4, 0, 6U, GetInternalKey("150"), GetInternalKey("179"));
  version_edit.DeletePartitionFile(4, 0, 7U, GetInternalKey("180"), GetInternalKey("220"));
  version_edit.DeletePartitionFile(4, 0, 8U, GetInternalKey("221"), GetInternalKey("300"));
  version_edit.DeletePartitionMeta(6, GetInternalKey("150"), GetInternalKey("179"));
  version_edit.DeletePartitionMeta(7, GetInternalKey("180"), GetInternalKey("220"));
  version_edit.DeletePartitionMeta(8, GetInternalKey("221"), GetInternalKey("300"));

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, 2);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 666, {{GetInternalKey("301"), GetInternalKey("350")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 26, {{GetInternalKey("150"), GetInternalKey("170")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 27, {{GetInternalKey("171"), GetInternalKey("179")}}));

  ASSERT_EQ(0U, new_vstorage.NumLevelBytes(0));
  ASSERT_EQ(100U, new_vstorage.NumLevelBytes(4));
  ASSERT_EQ(200U, new_vstorage.NumLevelBytes(5));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderPartitionTest, ApplyMultipleAndSaveTo) {
  UpdateVersionStorageInfo();

  VersionEdit version_edit;
  version_edit.AddPartitionFile(2, 0, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 676, 0, 100U, GetInternalKey("401"),
                       GetInternalKey("450"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 636, 0, 100U, GetInternalKey("601"),
                       GetInternalKey("650"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 616, 0, 100U, GetInternalKey("501"),
                       GetInternalKey("550"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 606, 0, 100U, GetInternalKey("701"),
                       GetInternalKey("750"), 200, 200, false);
  version_edit.AddPartitionMeta(666, GetInternalKey("301"), GetInternalKey("350"), true);
  version_edit.AddPartitionMeta(676, GetInternalKey("401"), GetInternalKey("450"), true);
  version_edit.AddPartitionMeta(636, GetInternalKey("601"), GetInternalKey("650"), true);
  version_edit.AddPartitionMeta(616, GetInternalKey("501"), GetInternalKey("550"), true);
  version_edit.AddPartitionMeta(606, GetInternalKey("701"), GetInternalKey("750"), true);

  EnvOptions env_options;

  VersionBuilder version_builder(env_options, nullptr, &vstorage_);

  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, 2);
  version_builder.Apply(&version_edit);
  version_builder.SaveTo(&new_vstorage);

  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 666, {{GetInternalKey("301"), GetInternalKey("350")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 676, {{GetInternalKey("401"), GetInternalKey("450")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 636, {{GetInternalKey("601"), GetInternalKey("650")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 616, {{GetInternalKey("501"), GetInternalKey("550")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 606, {{GetInternalKey("701"), GetInternalKey("750")}}));

  ASSERT_EQ(500U, new_vstorage.NumLevelBytes(2));

  UnrefFilesInVersion(&new_vstorage);
}

TEST_F(VersionBuilderPartitionTest, ApplyDeleteAndSaveTo) {
  UpdateVersionStorageInfo();

  EnvOptions env_options;
  VersionBuilder version_builder(env_options, nullptr, &vstorage_);
  VersionStorageInfo new_vstorage(&icmp_, ucmp_, options_.num_levels,
                                  kCompactionStyleLevel, nullptr, false, 2);

  VersionEdit version_edit;
  version_edit.AddPartitionFile(2, 0, 666, 0, 100U, GetInternalKey("301"),
                       GetInternalKey("350"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 676, 0, 100U, GetInternalKey("401"),
                       GetInternalKey("450"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 636, 0, 100U, GetInternalKey("601"),
                       GetInternalKey("650"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 616, 0, 100U, GetInternalKey("501"),
                       GetInternalKey("550"), 200, 200, false);
  version_edit.AddPartitionFile(2, 0, 606, 0, 100U, GetInternalKey("701"),
                       GetInternalKey("750"), 200, 200, false);
  version_edit.AddPartitionMeta(666, GetInternalKey("301"), GetInternalKey("350"), true);
  version_edit.AddPartitionMeta(676, GetInternalKey("401"), GetInternalKey("450"), true);
  version_edit.AddPartitionMeta(636, GetInternalKey("601"), GetInternalKey("650"), true);
  version_edit.AddPartitionMeta(616, GetInternalKey("501"), GetInternalKey("550"), true);
  version_edit.AddPartitionMeta(606, GetInternalKey("701"), GetInternalKey("750"), true);
  version_builder.Apply(&version_edit);

  VersionEdit version_edit2;
  version_edit.AddPartitionFile(2, 0, 808, 0, 100U, GetInternalKey("901"),
                       GetInternalKey("950"), 200, 200, false);
  version_edit2.DeletePartitionFile(2, 0, 616, GetInternalKey("501"), GetInternalKey("550"));
  version_edit2.DeletePartitionFile(2, 0, 636, GetInternalKey("601"), GetInternalKey("650"));
  version_edit2.DeletePartitionMeta(616, GetInternalKey("501"), GetInternalKey("550"));
  version_edit2.DeletePartitionMeta(636, GetInternalKey("601"), GetInternalKey("650"));
  version_edit.AddPartitionFile(2, 0, 806, 0, 100U, GetInternalKey("801"),
                       GetInternalKey("850"), 200, 200, false);
  version_builder.Apply(&version_edit2);

  version_builder.SaveTo(&new_vstorage);

  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 666, {{GetInternalKey("301"), GetInternalKey("350")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 676, {{GetInternalKey("401"), GetInternalKey("450")}}));
  ASSERT_TRUE(ValidateSegmentMetas(&new_vstorage, 606, {{GetInternalKey("701"), GetInternalKey("750")}}));

  ASSERT_EQ(300U, new_vstorage.NumLevelBytes(2));

  UnrefFilesInVersion(&new_vstorage);
}

// TEST_F(VersionBuilderPartitionTest, EstimatedActiveKeys) {
//   const uint32_t kTotalSamples = 20;
//   const uint32_t kNumLevels = 5;
//   const uint32_t kFilesPerLevel = 8;
//   const uint32_t kNumFiles = kNumLevels * kFilesPerLevel;
//   const uint32_t kEntriesPerFile = 1000;
//   const uint32_t kDeletionsPerFile = 100;
//   for (uint32_t i = 0; i < kNumFiles; ++i) {
//     int level = static_cast<int>(i / kFilesPerLevel);
//     if (level < vstorage_.keep_levels())
//       Add(level, i + 1,
//           ToString((i + 100) * 1000).c_str(),
//           ToString((i + 100) * 1000 + 999).c_str(),
//           100U,  0, 100, 100,
//           kEntriesPerFile, kDeletionsPerFile,
//           (i < kTotalSamples));
//     else
//       AddNew(level, i + 1,
//           ToString((i + 100) * 1000).c_str(),
//           ToString((i + 100) * 1000 + 999).c_str(),
//           100U,  0, 100, 100,
//           kEntriesPerFile, kDeletionsPerFile,
//           (i < kTotalSamples));
//   }
//   // minus 2X for the number of deletion entries because:
//   // 1x for deletion entry does not count as a data entry.
//   // 1x for each deletion entry will actually remove one data entry.
//   ASSERT_EQ(vstorage_.GetEstimatedActiveKeys(),
//             (kEntriesPerFile - 2 * kDeletionsPerFile) * kNumFiles);
// }

}  // namespace rocksdb

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
