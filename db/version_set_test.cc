//  Copyright (c) 2011-present, Facebook, Inc.  All rights reserved.
//  This source code is licensed under both the GPLv2 (found in the
//  COPYING file in the root directory) and Apache 2.0 License
//  (found in the LICENSE.Apache file in the root directory).
//
// Copyright (c) 2011 The LevelDB Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

#include "db/version_set.h"
#include "db/db_impl/db_impl.h"
#include "db/log_writer.h"
#include "logging/logging.h"
#include "table/mock_table.h"
#include "test_util/testharness.h"
#include "test_util/testutil.h"
#include "util/string_util.h"

namespace rocksdb {

class GenerateLevelFilesBriefTest : public testing::Test {
 public:
  std::vector<FileMetaData*> files_;
  std::vector<Partition> partitions_;
  size_t counter;
  LevelFilesBrief file_level_;
  Arena arena_;

  GenerateLevelFilesBriefTest(): counter(0) { }

  ~GenerateLevelFilesBriefTest() override {
    for (size_t i = 0; i < files_.size(); i++) {
      delete files_[i];
    }
    for (size_t i = 0; i < partitions_.size(); i++) {
      for (size_t j = 0; j < partitions_[i].files_.size(); j++) {
        delete partitions_[i].files_[j].file_metadata;
      }
    }
  }

  void Add(const char* smallest, const char* largest,
           SequenceNumber smallest_seq = 100,
           SequenceNumber largest_seq = 100) {
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(files_.size() + 1, 0, 0);
    f->smallest = InternalKey(smallest, smallest_seq, kTypeValue);
    f->largest = InternalKey(largest, largest_seq, kTypeValue);
    files_.push_back(f);
  }

  void AddPartition(const char* smallest, const char* largest,
           SequenceNumber smallest_seq = 100,
           SequenceNumber largest_seq = 100) {
    Partition p;
    p.smallest = InternalKey(smallest, smallest_seq, kTypeValue);
    p.largest = InternalKey(largest, largest_seq, kTypeValue);
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(files_.size() + 1, 0, 0);
    f->smallest = InternalKey(smallest, smallest_seq, kTypeValue);
    f->largest = InternalKey(largest, largest_seq, kTypeValue);
    p.files_.emplace_back(f->smallest, f->largest, f);
    partitions_.push_back(p);
  }

  int Compare() {
    int diff = 0;
    for (size_t i = 0; i < files_.size(); i++) {
      if (file_level_.files[i].fd.GetNumber() != files_[i]->fd.GetNumber()) {
        diff++;
      }
    }
    return diff;
  }

  int ComparePartition() {
    int diff = 0;
    for (size_t i = 0; i < partitions_.size(); i++) {
      if (file_level_.partitions[i].file_metadata[0].fd.GetNumber() != partitions_[i].files_[0].file_metadata->fd.GetNumber())
        diff++;
    }
    return diff;
  }
};

TEST_F(GenerateLevelFilesBriefTest, Empty) {
  DoGenerateLevelFilesBrief(&file_level_, files_, &arena_);
  ASSERT_EQ(0u, file_level_.num_files);
  ASSERT_EQ(0, Compare());
}

TEST_F(GenerateLevelFilesBriefTest, Single) {
  Add("p", "q");
  DoGenerateLevelFilesBrief(&file_level_, files_, &arena_);
  ASSERT_EQ(1u, file_level_.num_files);
  ASSERT_EQ(0, Compare());
}

TEST_F(GenerateLevelFilesBriefTest, Multiple) {
  Add("150", "200");
  Add("200", "250");
  Add("300", "350");
  Add("400", "450");
  DoGenerateLevelFilesBrief(&file_level_, files_, &arena_);
  ASSERT_EQ(4u, file_level_.num_files);
  ASSERT_EQ(0, Compare());
}

TEST_F(GenerateLevelFilesBriefTest, EmptyPartition) {
  DoGenerateLevelFilesBrief(&file_level_, partitions_, &arena_);
  ASSERT_EQ(0u, file_level_.num_files);
  ASSERT_EQ(0, Compare());
}

TEST_F(GenerateLevelFilesBriefTest, SinglePartition) {
  AddPartition("p", "q");
  DoGenerateLevelFilesBrief(&file_level_, partitions_, &arena_);
  ASSERT_EQ(1u, file_level_.num_files);
  ASSERT_EQ(0, ComparePartition());
}

TEST_F(GenerateLevelFilesBriefTest, MultiplePartitions) {
  AddPartition("150", "200");
  AddPartition("200", "250");
  AddPartition("300", "350");
  AddPartition("400", "450");
  DoGenerateLevelFilesBrief(&file_level_, partitions_, &arena_);
  ASSERT_EQ(4u, file_level_.num_files);
  ASSERT_EQ(0, ComparePartition());
}

class CountingLogger : public Logger {
 public:
  CountingLogger() : log_count(0) {}
  using Logger::Logv;
  void Logv(const char* /*format*/, va_list /*ap*/) override { log_count++; }
  int log_count;
};

Options GetOptionsWithNumLevels(int num_levels,
                                std::shared_ptr<CountingLogger> logger) {
  Options opt;
  opt.num_levels = num_levels;
  opt.info_log = logger;
  return opt;
}

class VersionStorageInfoTest : public testing::Test {
 public:
  const Comparator* ucmp_;
  InternalKeyComparator icmp_;
  std::shared_ptr<CountingLogger> logger_;
  Options options_;
  ImmutableCFOptions ioptions_;
  MutableCFOptions mutable_cf_options_;
  VersionStorageInfo* vstorage_;

  InternalKey GetInternalKey(const char* ukey,
                             SequenceNumber smallest_seq = 100) {
    return InternalKey(ukey, smallest_seq, kTypeValue);
  }

  VersionStorageInfoTest()
      : ucmp_(BytewiseComparator()),
        icmp_(ucmp_),
        logger_(new CountingLogger()),
        options_(GetOptionsWithNumLevels(6, logger_)),
        ioptions_(options_),
        mutable_cf_options_(options_),
        vstorage_(nullptr) {}

  ~VersionStorageInfoTest() override {
    if (vstorage_ != nullptr) {
      for (int i = 0; i < vstorage_->keep_levels(); i++) {
        for (auto* f : vstorage_->LevelFiles(i)) {
          if (--f->refs == 0) {
            delete f;
          }
        }
      }
      for (int i = vstorage_->keep_levels(); i < vstorage_->num_levels(); i++) {
        for (auto& p : vstorage_->LevelPartitions(i)) {
          for (auto& f : p.files_) {
            if (--f.file_metadata->refs == 0) {
              delete f.file_metadata;
            }
          }
        }
      }
      delete vstorage_;
    }
  }

  void Init(int keep_levels) {
    vstorage_ = new VersionStorageInfo(&icmp_, ucmp_, 6, kCompactionStyleLevel, nullptr, false, keep_levels);
  }

  void Add(int level, uint32_t file_number, const char* smallest,
           const char* largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = GetInternalKey(smallest, 0);
    f->largest = GetInternalKey(largest, 0);
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFile(level, f);
  }

  void AddFileNewPartition(int level, uint32_t file_number, const char* smallest,
           const char* largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = GetInternalKey(smallest, 0);
    f->largest = GetInternalKey(largest, 0);
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileNewPartition(level, f, GetInternalKey(smallest, 0), GetInternalKey(largest, 0));
    vstorage_->AddFileSegmentMeta(file_number, GetInternalKey(smallest, 0), GetInternalKey(largest, 0), true);
  }

  void AddFileOldPartition(int level, uint32_t file_number, const char* smallest,
           const char* largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = GetInternalKey(smallest, 0);
    f->largest = GetInternalKey(largest, 0);
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileOldPartition(level, f, GetInternalKey(smallest, 0), GetInternalKey(largest, 0));
    vstorage_->AddFileSegmentMeta(file_number, GetInternalKey(smallest, 0), GetInternalKey(largest, 0), true);
  }

  void AddFileNewPartition(int level, uint32_t file_number,
                           const char* smallest, const char* largest,
                           const char* seg_smallest, const char* seg_largest,
                           uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = GetInternalKey(smallest, 0);
    f->largest = GetInternalKey(largest, 0);
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileNewPartition(level, f, GetInternalKey(seg_smallest, 0), GetInternalKey(seg_largest, 0));
    vstorage_->AddFileSegmentMeta(file_number, GetInternalKey(seg_smallest, 0), GetInternalKey(seg_largest, 0), true);
  }

  void AddFileOldPartition(int level, uint32_t file_number,
                           const char* smallest, const char* largest,
                           const char* seg_smallest, const char* seg_largest,
                           uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = GetInternalKey(smallest, 0);
    f->largest = GetInternalKey(largest, 0);
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileOldPartition(level, f, GetInternalKey(seg_smallest, 0), GetInternalKey(seg_largest, 0));
    vstorage_->AddFileSegmentMeta(file_number, GetInternalKey(seg_smallest, 0), GetInternalKey(seg_largest, 0), true);
  }

  void Add(int level, uint32_t file_number, const InternalKey& smallest,
           const InternalKey& largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = smallest;
    f->largest = largest;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFile(level, f);
  }

  void AddFileNewPartition(int level, uint32_t file_number, const InternalKey& smallest,
           const InternalKey& largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = smallest;
    f->largest = largest;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileNewPartition(level, f, smallest, largest);
    vstorage_->AddFileSegmentMeta(file_number, smallest, largest, true);
  }

  void AddFileOldPartition(int level, uint32_t file_number, const InternalKey& smallest,
           const InternalKey& largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = smallest;
    f->largest = largest;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileOldPartition(level, f, smallest, largest);
    vstorage_->AddFileSegmentMeta(file_number, smallest, largest, true);
  }

  void AddFileNewPartition(int level, uint32_t file_number, const InternalKey& smallest,
           const InternalKey& largest, const InternalKey& seg_smallest,
           const InternalKey& seg_largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = smallest;
    f->largest = largest;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileNewPartition(level, f, seg_smallest, seg_largest);
    vstorage_->AddFileSegmentMeta(file_number, seg_smallest, seg_largest, true);
  }

  void AddFileOldPartition(int level, uint32_t file_number, const InternalKey& smallest,
           const InternalKey& largest, const InternalKey& seg_smallest,
           const InternalKey& seg_largest, uint64_t file_size = 0) {
    assert(level < vstorage_->num_levels());
    FileMetaData* f = new FileMetaData;
    f->fd = FileDescriptor(file_number, 0, file_size);
    f->smallest = smallest;
    f->largest = largest;
    f->compensated_file_size = file_size;
    f->refs = 0;
    f->num_entries = 0;
    f->num_deletions = 0;
    vstorage_->AddFileOldPartition(level, f, seg_smallest, seg_largest);
    vstorage_->AddFileSegmentMeta(file_number, seg_smallest, seg_largest, true);
  }

  std::string GetOverlappingFiles(int level, const InternalKey& begin,
                                  const InternalKey& end) {
    std::vector<FileMetaData*> inputs;
    vstorage_->GetOverlappingInputs(level, &begin, &end, &inputs);

    std::string result;
    for (size_t i = 0; i < inputs.size(); ++i) {
      if (i > 0) {
        result += ",";
      }
      AppendNumberTo(&result, inputs[i]->fd.GetNumber());
    }
    return result;
  }
};

int FindFileInRangeTest(const InternalKeyComparator& icmp,
    const LevelFilesBrief& file_level,
    const Slice& key,
    uint32_t left,
    uint32_t right) {
  if (!file_level.is_partition_level) {
    auto cmp = [&](const FdWithKeyRange& f, const Slice& k) -> bool {
      return icmp.InternalKeyComparator::Compare(f.largest_key, k) < 0;
    };
    const auto &b = file_level.files;
    return static_cast<int>(std::lower_bound(b + left,
                                           b + right, key, cmp) - b);
  }
  else {
    auto cmp = [&](const PartitionWithKeyRange& f, const Slice& k) -> bool {
      return icmp.InternalKeyComparator::Compare(f.largest_key, k) < 0;
    };
    const auto &b = file_level.partitions;
    return static_cast<int>(std::lower_bound(b + left,
                                           b + right, key, cmp) - b);
  }
}

class FilePickerTest {
 public:
  FilePickerTest(const Slice& user_key,
             const Slice& ikey, autovector<LevelFilesBrief>* file_levels,
             unsigned int num_levels, FileIndexer* file_indexer,
             const Comparator* user_comparator,
             const InternalKeyComparator* internal_comparator)
      : num_levels_(num_levels),
        curr_level_(static_cast<unsigned int>(-1)),
        returned_file_level_(static_cast<unsigned int>(-1)),
        hit_file_level_(static_cast<unsigned int>(-1)),
        search_left_bound_(0),
        search_right_bound_(FileIndexer::kLevelMaxIndex),

        level_files_brief_(file_levels),
        is_hit_file_last_in_level_(false),
        curr_file_level_(nullptr),
        user_key_(user_key),
        ikey_(ikey),
        file_indexer_(file_indexer),
        user_comparator_(user_comparator),
        internal_comparator_(internal_comparator) {

    // Setup member variables to search first level.
    search_ended_ = !PrepareNextLevel();
    if (!search_ended_) {
      // Prefetch Level 0 table data to avoid cache miss if possible.
      for (unsigned int i = 0; i < (*level_files_brief_)[0].num_files; ++i) {
        auto* r = (*level_files_brief_)[0].files[i].fd.table_reader;
        if (r) {
          r->Prepare(ikey);
        }
      }
    }
  }

  int GetCurrentLevel() const { return curr_level_; }

  FdWithKeyRange* GetNextFile() {
    while (!search_ended_) {  // Loops over different levels.
      while (curr_index_in_curr_level_ < curr_file_level_->num_files) {
        if (!curr_file_level_->is_partition_level) {
          // Loops over all files in current level.
          FdWithKeyRange* f = &curr_file_level_->files[curr_index_in_curr_level_];
          hit_file_level_ = curr_level_;
          is_hit_file_last_in_level_ =
              curr_index_in_curr_level_ == curr_file_level_->num_files - 1;
          int cmp_largest = -1;

          // Do key range filtering of files or/and fractional cascading if:
          // (1) not all the files are in level 0, or
          // (2) there are more than 3 current level files
          // If there are only 3 or less current level files in the system, we skip
          // the key range filtering. In this case, more likely, the system is
          // highly tuned to minimize number of tables queried by each query,
          // so it is unlikely that key range filtering is more efficient than
          // querying the files.
          if (num_levels_ > 1 || curr_file_level_->num_files > 3) {
            // Check if key is within a file's range. If search left bound and
            // right bound point to the same find, we are sure key falls in
            // range.
            assert(curr_level_ == 0 ||
                   curr_index_in_curr_level_ == start_index_in_curr_level_ ||
                   user_comparator_->CompareWithoutTimestamp(
                       user_key_, ExtractUserKey(f->smallest_key)) <= 0);

            int cmp_smallest = user_comparator_->CompareWithoutTimestamp(
                user_key_, ExtractUserKey(f->smallest_key));
            if (cmp_smallest >= 0) {
              cmp_largest = user_comparator_->CompareWithoutTimestamp(
                  user_key_, ExtractUserKey(f->largest_key));
            }

            // Setup file search bound for the next level based on the
            // comparison results
            if (curr_level_ > 0) {
              file_indexer_->GetNextLevelIndex(curr_level_,
                                              curr_index_in_curr_level_,
                                              cmp_smallest, cmp_largest,
                                              &search_left_bound_,
                                              &search_right_bound_);
            }
            // Key falls out of current file's range
            if (cmp_smallest < 0 || cmp_largest > 0) {
              if (curr_level_ == 0) {
                ++curr_index_in_curr_level_;
                continue;
              } else {
                // Search next level.
                break;
              }
            }
          }

          returned_file_level_ = curr_level_;
          if (curr_level_ > 0 && cmp_largest < 0) {
            // No more files to search in this level.
            search_ended_ = !PrepareNextLevel();
          } else {
            ++curr_index_in_curr_level_;
          }
          return f;
        }
        else { // The partition level.
          PartitionWithKeyRange* p = &curr_file_level_->partitions[curr_index_in_curr_level_];
          FdWithKeyRange* f = &(p->file_metadata[curr_index_in_curr_partition_]);
          hit_file_level_ = curr_level_;
          is_hit_file_last_in_level_ =
              curr_index_in_curr_level_ == curr_file_level_->num_files - 1;
          int cmp_largest = -1;

          if (num_levels_ > 1 || curr_file_level_->num_files > 3) {
            assert(curr_level_ == 0 ||
                   curr_index_in_curr_level_ == start_index_in_curr_level_ ||
                   user_comparator_->CompareWithoutTimestamp(
                       user_key_, ExtractUserKey(p->file_metadata[curr_index_in_curr_partition_].smallest_key)) <= 0);

            int cmp_smallest = user_comparator_->CompareWithoutTimestamp(
                user_key_, ExtractUserKey(p->file_metadata[curr_index_in_curr_partition_].smallest_key));
            if (cmp_smallest >= 0) {
              cmp_largest = user_comparator_->CompareWithoutTimestamp(
                  user_key_, ExtractUserKey(p->file_metadata[curr_index_in_curr_partition_].largest_key));
            }

            int partition_cmp_largest = -1;
            int partition_cmp_smallest = user_comparator_->CompareWithoutTimestamp(
                user_key_, ExtractUserKey(p->smallest_key));
            if (partition_cmp_smallest >= 0) {
              partition_cmp_largest = user_comparator_->CompareWithoutTimestamp(
                  user_key_, ExtractUserKey(p->largest_key));
            }

            // Setup file search bound for the next level based on the
            // comparison results
            if (curr_level_ > 0) {
              file_indexer_->GetNextLevelIndex(curr_level_,
                                              curr_index_in_curr_level_,
                                              partition_cmp_smallest, partition_cmp_largest,
                                              &search_left_bound_,
                                              &search_right_bound_);
            }
            // Key falls out of current partition's range
            if (cmp_smallest < 0 || cmp_largest > 0) {
              if (curr_index_in_curr_partition_ > 0) {
                --curr_index_in_curr_partition_;
                continue;
              }
              else {
                break;
              }
            }
          }

          returned_file_level_ = curr_level_;
          if (curr_level_ > 0 && cmp_largest < 0) {
            if (curr_index_in_curr_partition_ == 0) {
              // No more files to search in this level.
              search_ended_ = !PrepareNextLevel();
            }
            else
              --curr_index_in_curr_partition_;
          } else {
            ++curr_index_in_curr_level_;
          }
          return f;
        }
      }
      // Start searching next level.
      search_ended_ = !PrepareNextLevel();
    }
    // Search ended.
    return nullptr;
  }

  // getter for current file level
  // for GET_HIT_L0, GET_HIT_L1 & GET_HIT_L2_AND_UP counts
  unsigned int GetHitFileLevel() { return hit_file_level_; }

  // Returns true if the most recent "hit file" (i.e., one returned by
  // GetNextFile()) is at the last index in its level.
  bool IsHitFileLastInLevel() { return is_hit_file_last_in_level_; }

 private:
  unsigned int num_levels_;
  unsigned int curr_level_;
  unsigned int returned_file_level_;
  unsigned int hit_file_level_;
  int32_t search_left_bound_;
  int32_t search_right_bound_;
  autovector<LevelFilesBrief>* level_files_brief_;
  bool search_ended_;
  bool is_hit_file_last_in_level_;
  LevelFilesBrief* curr_file_level_;
  unsigned int curr_index_in_curr_level_;
  unsigned int curr_index_in_curr_partition_;
  unsigned int start_index_in_curr_level_;
  Slice user_key_;
  Slice ikey_;
  FileIndexer* file_indexer_;
  const Comparator* user_comparator_;
  const InternalKeyComparator* internal_comparator_;


  // Setup local variables to search next level.
  // Returns false if there are no more levels to search.
  bool PrepareNextLevel() {
    curr_level_++;
    while (curr_level_ < num_levels_) {
      curr_file_level_ = &(*level_files_brief_)[curr_level_];
      if (curr_file_level_->num_files == 0) {
        // When current level is empty, the search bound generated from upper
        // level must be [0, -1] or [0, FileIndexer::kLevelMaxIndex] if it is
        // also empty.
        assert(search_left_bound_ == 0);
        assert(search_right_bound_ == -1 ||
               search_right_bound_ == FileIndexer::kLevelMaxIndex);
        // Since current level is empty, it will need to search all files in
        // the next level
        search_left_bound_ = 0;
        search_right_bound_ = FileIndexer::kLevelMaxIndex;
        curr_level_++;
        continue;
      }

      // Some files may overlap each other. We find
      // all files that overlap user_key and process them in order from
      // newest to oldest. In the context of merge-operator, this can occur at
      // any level. Otherwise, it only occurs at Level-0 (since Put/Deletes
      // are always compacted into a single entry).
      int32_t start_index;
      if (curr_level_ == 0) {
        // On Level-0, we read through all files to check for overlap.
        start_index = 0;
      } else {
        // On Level-n (n>=1), files are sorted. Binary search to find the
        // earliest file whose largest key >= ikey. Search left bound and
        // right bound are used to narrow the range.
        if (search_left_bound_ <= search_right_bound_) {
          if (search_right_bound_ == FileIndexer::kLevelMaxIndex) {
            search_right_bound_ =
                static_cast<int32_t>(curr_file_level_->num_files) - 1;
          }
          // `search_right_bound_` is an inclusive upper-bound, but since it was
          // determined based on user key, it is still possible the lookup key
          // falls to the right of `search_right_bound_`'s corresponding file.
          // So, pass a limit one higher, which allows us to detect this case.
          start_index =
              FindFileInRangeTest(*internal_comparator_, *curr_file_level_, ikey_,
                              static_cast<uint32_t>(search_left_bound_),
                              static_cast<uint32_t>(search_right_bound_) + 1);

          if (start_index == search_right_bound_ + 1) {
            // `ikey_` comes after `search_right_bound_`. The lookup key does
            // not exist on this level, so let's skip this level and do a full
            // binary search on the next level.
            search_left_bound_ = 0;
            search_right_bound_ = FileIndexer::kLevelMaxIndex;
            curr_level_++;
            continue;
          }
        } else {
          // search_left_bound > search_right_bound, key does not exist in
          // this level. Since no comparison is done in this level, it will
          // need to search all files in the next level.
          search_left_bound_ = 0;
          search_right_bound_ = FileIndexer::kLevelMaxIndex;
          curr_level_++;
          continue;
        }
      }
      start_index_in_curr_level_ = start_index;
      curr_index_in_curr_level_ = start_index;
      if (curr_file_level_->is_partition_level)
        curr_index_in_curr_partition_ = curr_file_level_->partitions[curr_index_in_curr_level_].num_files - 1;

      return true;
    }
    // curr_level_ = num_levels_. So, no more levels to search.
    return false;
  }
};

TEST_F(VersionStorageInfoTest, FilePicker1) {
  Init(-1);
  Add(1, 1U, "a", "z");
  Add(2, 2U, "a", "z");
  Add(3, 3U, "a", "z");
  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateFileIndexer();
  vstorage_->GenerateLevelFilesBrief();
  InternalKey ikey = GetInternalKey("k", 0);
  Slice user_key = ikey.user_key();
  FilePickerTest fp(
      user_key, ikey.Encode(), vstorage_->level_files_brief_test(),
      vstorage_->num_non_empty_levels_test(), vstorage_->file_indexer_test(),
      vstorage_->UserComparator(), vstorage_->InternalComparator());
  FdWithKeyRange* f = fp.GetNextFile();
  ASSERT_EQ(1lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(2lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(3lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_TRUE(f == nullptr);
}

TEST_F(VersionStorageInfoTest, PartitionFilePicker1) {
  Init(2);
  Add(1, 1U, "a", "z");
  AddFileNewPartition(2, 2U, "a", "z");
  AddFileNewPartition(3, 3U, "a", "z");
  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateFileIndexer();
  vstorage_->GenerateLevelFilesBrief();
  InternalKey ikey = GetInternalKey("k", 0);
  Slice user_key = ikey.user_key();
  FilePickerTest fp(
      user_key, ikey.Encode(), vstorage_->level_files_brief_test(),
      vstorage_->num_non_empty_levels_test(), vstorage_->file_indexer_test(),
      vstorage_->UserComparator(), vstorage_->InternalComparator());
  FdWithKeyRange* f = fp.GetNextFile();
  ASSERT_EQ(1lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(2lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(3lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_TRUE(f == nullptr);
}

TEST_F(VersionStorageInfoTest, PartitionFilePicker2) {
  Init(2);
  Add(1, 1U, "a", "z");
  AddFileNewPartition(2, 2U, "a", "l");
  AddFileOldPartition(2, 3U, "j", "z");
  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateFileIndexer();
  vstorage_->GenerateLevelFilesBrief();
  InternalKey ikey = GetInternalKey("k", 0);
  Slice user_key = ikey.user_key();
  FilePickerTest fp(
      user_key, ikey.Encode(), vstorage_->level_files_brief_test(),
      vstorage_->num_non_empty_levels_test(), vstorage_->file_indexer_test(),
      vstorage_->UserComparator(), vstorage_->InternalComparator());
  FdWithKeyRange* f = fp.GetNextFile();
  ASSERT_EQ(1lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(3lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(2lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_TRUE(f == nullptr);
}

TEST_F(VersionStorageInfoTest, PartitionFilePicker3) {
  Init(2);
  Add(1, 1U, "a", "z");
  AddFileNewPartition(2, 2U, "a", "i");
  AddFileNewPartition(2, 3U, "j", "z");
  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateFileIndexer();
  vstorage_->GenerateLevelFilesBrief();
  InternalKey ikey = GetInternalKey("k", 0);
  Slice user_key = ikey.user_key();
  FilePickerTest fp(
      user_key, ikey.Encode(), vstorage_->level_files_brief_test(),
      vstorage_->num_non_empty_levels_test(), vstorage_->file_indexer_test(),
      vstorage_->UserComparator(), vstorage_->InternalComparator());
  FdWithKeyRange* f = fp.GetNextFile();
  ASSERT_EQ(1lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(3lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_TRUE(f == nullptr);
}

TEST_F(VersionStorageInfoTest, PartitionFilePicker4) {
  Init(2);
  Add(1, 1U, "a", "z");
  AddFileNewPartition(2, 2U, "a", "i");
  AddFileOldPartition(2, 3U, "j", "z");
  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateFileIndexer();
  vstorage_->GenerateLevelFilesBrief();
  InternalKey ikey = GetInternalKey("k", 0);
  Slice user_key = ikey.user_key();
  FilePickerTest fp(
      user_key, ikey.Encode(), vstorage_->level_files_brief_test(),
      vstorage_->num_non_empty_levels_test(), vstorage_->file_indexer_test(),
      vstorage_->UserComparator(), vstorage_->InternalComparator());
  FdWithKeyRange* f = fp.GetNextFile();
  ASSERT_EQ(1lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(3lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_TRUE(f == nullptr);
}

TEST_F(VersionStorageInfoTest, PartitionFilePicker5) {
  Init(2);
  Add(1, 1U, "a", "j");
  AddFileNewPartition(2, 2U, "a", "l");
  AddFileOldPartition(2, 3U, "j", "z");
  AddFileNewPartition(3, 4U, "a", "c");
  AddFileNewPartition(3, 5U, "j", "m");
  AddFileNewPartition(4, 6U, "a", "l");
  AddFileOldPartition(4, 7U, "m", "z");
  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateFileIndexer();
  vstorage_->GenerateLevelFilesBrief();
  InternalKey ikey = GetInternalKey("k", 0);
  Slice user_key = ikey.user_key();
  FilePickerTest fp(
      user_key, ikey.Encode(), vstorage_->level_files_brief_test(),
      vstorage_->num_non_empty_levels_test(), vstorage_->file_indexer_test(),
      vstorage_->UserComparator(), vstorage_->InternalComparator());
  FdWithKeyRange* f = fp.GetNextFile();
  ASSERT_EQ(3lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(2lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(5lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_EQ(6lu, f->fd.GetNumber());
  f = fp.GetNextFile();
  ASSERT_TRUE(f == nullptr);
}

// class FilePickerMultiGetTest {
//  private:
//   struct FilePickerContext;

//  public:
//   FilePickerMultiGetTest(MultiGetRange* range,
//                      autovector<LevelFilesBrief>* file_levels,
//                      unsigned int num_levels, FileIndexer* file_indexer,
//                      const Comparator* user_comparator,
//                      const InternalKeyComparator* internal_comparator)
//       : num_levels_(num_levels),
//         curr_level_(static_cast<unsigned int>(-1)),
//         returned_file_level_(static_cast<unsigned int>(-1)),
//         hit_file_level_(static_cast<unsigned int>(-1)),
//         range_(range),
//         batch_iter_(range->begin()),
//         batch_iter_prev_(range->begin()),
//         maybe_repeat_key_(false),
//         current_level_range_(*range, range->begin(), range->end()),
//         current_file_range_(*range, range->begin(), range->end()),
//         level_files_brief_(file_levels),
//         is_hit_file_last_in_level_(false),
//         curr_file_level_(nullptr),
//         file_indexer_(file_indexer),
//         user_comparator_(user_comparator),
//         internal_comparator_(internal_comparator) {
//     for (auto iter = range_->begin(); iter != range_->end(); ++iter) {
//       fp_ctx_array_[iter.index()] =
//           FilePickerContext(0, FileIndexer::kLevelMaxIndex);
//     }

//     // Setup member variables to search first level.
//     search_ended_ = !PrepareNextLevel();
//     if (!search_ended_) {
//       // REVISIT
//       // Prefetch Level 0 table data to avoid cache miss if possible.
//       // As of now, only PlainTableReader and CuckooTableReader do any
//       // prefetching. This may not be necessary anymore once we implement
//       // batching in those table readers
//       for (unsigned int i = 0; i < (*level_files_brief_)[0].num_files; ++i) {
//         auto* r = (*level_files_brief_)[0].files[i].fd.table_reader;
//         if (r) {
//           for (auto iter = range_->begin(); iter != range_->end(); ++iter) {
//             r->Prepare(iter->ikey);
//           }
//         }
//       }
//     }
//   }

//   int GetCurrentLevel() const { return curr_level_; }

//   // Iterates through files in the current level until it finds a file that
//   // contains at least one key from the MultiGet batch
//   bool GetNextFileInLevelWithKeys(MultiGetRange* next_file_range,
//                                   size_t* file_index, FdWithKeyRange** fd,
//                                   bool* is_last_key_in_file) {
//     size_t curr_file_index = *file_index;
//     FdWithKeyRange* f = nullptr;
//     bool file_hit = false;
//     int cmp_largest = -1;
//     if (curr_file_index >= curr_file_level_->num_files) {
//       // In the unlikely case the next key is a duplicate of the current key,
//       // and the current key is the last in the level and the internal key
//       // was not found, we need to skip lookup for the remaining keys and
//       // reset the search bounds
//       if (batch_iter_ != current_level_range_.end()) {
//         ++batch_iter_;
//         for (; batch_iter_ != current_level_range_.end(); ++batch_iter_) {
//           struct FilePickerContext& fp_ctx = fp_ctx_array_[batch_iter_.index()];
//           fp_ctx.search_left_bound = 0;
//           fp_ctx.search_right_bound = FileIndexer::kLevelMaxIndex;
//         }
//       }
//       return false;
//     }
//     // Loops over keys in the MultiGet batch until it finds a file with
//     // atleast one of the keys. Then it keeps moving forward until the
//     // last key in the batch that falls in that file
//     if (!curr_file_level_->is_partition_level) {
//       while (batch_iter_ != current_level_range_.end() &&
//              (fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_level ==
//                   curr_file_index ||
//               !file_hit)) {
//         struct FilePickerContext& fp_ctx = fp_ctx_array_[batch_iter_.index()];
//         f = &curr_file_level_->files[fp_ctx.curr_index_in_curr_level];
//         Slice& user_key = batch_iter_->ukey;

//         // Do key range filtering of files or/and fractional cascading if:
//         // (1) not all the files are in level 0, or
//         // (2) there are more than 3 current level files
//         // If there are only 3 or less current level files in the system, we
//         // skip the key range filtering. In this case, more likely, the system
//         // is highly tuned to minimize number of tables queried by each query,
//         // so it is unlikely that key range filtering is more efficient than
//         // querying the files.
//         if (num_levels_ > 1 || curr_file_level_->num_files > 3) {
//           // Check if key is within a file's range. If search left bound and
//           // right bound point to the same find, we are sure key falls in
//           // range.
//           assert(curr_level_ == 0 ||
//                  fp_ctx.curr_index_in_curr_level ==
//                      fp_ctx.start_index_in_curr_level ||
//                  user_comparator_->Compare(user_key,
//                                            ExtractUserKey(f->smallest_key)) <= 0);

//           int cmp_smallest = user_comparator_->Compare(
//               user_key, ExtractUserKey(f->smallest_key));
//           if (cmp_smallest >= 0) {
//             cmp_largest = user_comparator_->Compare(
//                 user_key, ExtractUserKey(f->largest_key));
//           } else {
//             cmp_largest = -1;
//           }

//           // Setup file search bound for the next level based on the
//           // comparison results
//           if (curr_level_ > 0) {
//             file_indexer_->GetNextLevelIndex(
//                 curr_level_, fp_ctx.curr_index_in_curr_level, cmp_smallest,
//                 cmp_largest, &fp_ctx.search_left_bound,
//                 &fp_ctx.search_right_bound);
//           }
//           // Key falls out of current file's range
//           if (cmp_smallest < 0 || cmp_largest > 0) {
//             next_file_range->SkipKey(batch_iter_);
//           } else {
//             file_hit = true;
//           }
//         } else {
//           file_hit = true;
//         }
//         if (cmp_largest == 0) {
//           // cmp_largest is 0, which means the next key will not be in this
//           // file, so stop looking further. Also don't increment megt_iter_
//           // as we may have to look for this key in the next file if we don't
//           // find it in this one
//           break;
//         } else {
//           if (curr_level_ == 0) {
//             // We need to look through all files in level 0
//             ++fp_ctx.curr_index_in_curr_level;
//           }
//           ++batch_iter_;
//         }
//         if (!file_hit) {
//           curr_file_index =
//               (batch_iter_ != current_level_range_.end())
//                   ? fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_level
//                   : curr_file_level_->num_files;
//         }
//       }
//     }
//     else {
//       size_t curr_partition_index = fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_partition_;
//       while (batch_iter_ != current_level_range_.end() &&
//            ((fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_level == curr_file_index &&
//             curr_partition_index == fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_partition_) ||
//             !file_hit)) {
//         struct FilePickerContext& fp_ctx = fp_ctx_array_[batch_iter_.index()];
//         PartitionWithKeyRange* p = &curr_file_level_->partitions[fp_ctx.curr_index_in_curr_level];
//         f = &p->file_metadata[fp_ctx.curr_index_in_curr_partition_];
//         curr_partition_index = fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_partition_;
//         Slice& user_key = batch_iter_->ukey;

//         if (num_levels_ > 1 || curr_file_level_->num_files > 3) {
//           assert(curr_level_ == 0 ||
//                  fp_ctx.curr_index_in_curr_level ==
//                      fp_ctx.start_index_in_curr_level ||
//                  user_comparator_->Compare(user_key,
//                                            ExtractUserKey(p->file_metadata[fp_ctx.curr_index_in_curr_partition_].smallest_key)) <= 0);

//           int cmp_smallest = user_comparator_->Compare(
//               user_key, ExtractUserKey(p->file_metadata[fp_ctx.curr_index_in_curr_partition_].smallest_key));
//           if (cmp_smallest >= 0) {
//             cmp_largest = user_comparator_->Compare(
//                 user_key, ExtractUserKey(p->file_metadata[fp_ctx.curr_index_in_curr_partition_].largest_key));
//           } else {
//             cmp_largest = -1;
//           }

//           int partition_cmp_largest = -1;
//           int partition_cmp_smallest = user_comparator_->CompareWithoutTimestamp(
//               user_key, ExtractUserKey(p->smallest_key));
//           if (partition_cmp_smallest >= 0) {
//             partition_cmp_largest = user_comparator_->CompareWithoutTimestamp(
//                 user_key, ExtractUserKey(p->largest_key));
//           }

//           if (curr_level_ > 0) {
//             file_indexer_->GetNextLevelIndex(
//                 curr_level_, fp_ctx.curr_index_in_curr_level, partition_cmp_smallest,
//                 partition_cmp_largest, &fp_ctx.search_left_bound,
//                 &fp_ctx.search_right_bound);
//           }
//           // Key falls out of current file's range
//           if (cmp_smallest < 0 || cmp_largest > 0) {
//             next_file_range->SkipKey(batch_iter_);
//           } else {
//             file_hit = true;
//           }
//         } else {
//           file_hit = true;
//         }
//         if (cmp_largest == 0) {
//           break;
//         } else {
//           if (fp_ctx.curr_index_in_curr_partition_ > 0) {
//             --fp_ctx.curr_index_in_curr_partition_;
//           } else {
//             // fp_ctx.curr_index_in_curr_partition_ = 0;
//             ++batch_iter_;
//           }
//         }
//         if (!file_hit) {
//           curr_file_index =
//               (batch_iter_ != current_level_range_.end())
//                   ? fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_level
//                   : curr_file_level_->num_files;
//         }
//       }
//     }

//     *fd = f;
//     *file_index = curr_file_index;
//     *is_last_key_in_file = cmp_largest == 0;
//     return file_hit;
//   }

//   FdWithKeyRange* GetNextFile() {
//     while (!search_ended_) {
//       // Start searching next level.
//       if (batch_iter_ == current_level_range_.end()) {
//         search_ended_ = !PrepareNextLevel();
//         continue;
//       } else {
//         if (maybe_repeat_key_) {
//           maybe_repeat_key_ = false;
//           // Check if we found the final value for the last key in the
//           // previous lookup range. If we did, then there's no need to look
//           // any further for that key, so advance batch_iter_. Else, keep
//           // batch_iter_ positioned on that key so we look it up again in
//           // the next file
//           // For L0, always advance the key because we will look in the next
//           // file regardless for all keys not found yet
//           if (current_level_range_.CheckKeyDone(batch_iter_) ||
//               curr_level_ == 0) {
//             ++batch_iter_;
//           }
//         }
//         // batch_iter_prev_ will become the start key for the next file
//         // lookup
//         batch_iter_prev_ = batch_iter_;
//       }

//       MultiGetRange next_file_range(current_level_range_, batch_iter_prev_,
//                                     current_level_range_.end());
//       size_t curr_file_index =
//           (batch_iter_ != current_level_range_.end())
//               ? fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_level
//               : curr_file_level_->num_files;
//       FdWithKeyRange* f;
//       bool is_last_key_in_file;
//       if (!GetNextFileInLevelWithKeys(&next_file_range, &curr_file_index, &f,
//                                       &is_last_key_in_file)) {
//         search_ended_ = !PrepareNextLevel();
//       } else {
//         MultiGetRange::Iterator upper_key = batch_iter_;
//         if (is_last_key_in_file) {
//           // Since cmp_largest is 0, batch_iter_ still points to the last key
//           // that falls in this file, instead of the next one. Increment
//           // upper_key so we can set the range properly for SST MultiGet
//           ++upper_key;
//           ++(fp_ctx_array_[batch_iter_.index()].curr_index_in_curr_level);
//           maybe_repeat_key_ = true;
//         }
//         // Set the range for this file
//         current_file_range_ =
//             MultiGetRange(next_file_range, batch_iter_prev_, upper_key);
//         returned_file_level_ = curr_level_;
//         hit_file_level_ = curr_level_;
//         is_hit_file_last_in_level_ =
//             curr_file_index == curr_file_level_->num_files - 1;
//         return f;
//       }
//     }

//     // Search ended
//     return nullptr;
//   }

//   // getter for current file level
//   // for GET_HIT_L0, GET_HIT_L1 & GET_HIT_L2_AND_UP counts
//   unsigned int GetHitFileLevel() { return hit_file_level_; }

//   // Returns true if the most recent "hit file" (i.e., one returned by
//   // GetNextFile()) is at the last index in its level.
//   bool IsHitFileLastInLevel() { return is_hit_file_last_in_level_; }

//   const MultiGetRange& CurrentFileRange() { return current_file_range_; }

//  private:
//   unsigned int num_levels_;
//   unsigned int curr_level_;
//   unsigned int returned_file_level_;
//   unsigned int hit_file_level_;

//   struct FilePickerContext {
//     int32_t search_left_bound;
//     int32_t search_right_bound;
//     unsigned int curr_index_in_curr_partition_;
//     unsigned int curr_index_in_curr_level;
//     unsigned int start_index_in_curr_level;

//     FilePickerContext(int32_t left, int32_t right)
//         : search_left_bound(left), search_right_bound(right),
//           curr_index_in_curr_partition_(0),
//           curr_index_in_curr_level(0), start_index_in_curr_level(0) {}

//     FilePickerContext() = default;
//   };
//   std::array<FilePickerContext, MultiGetContext::MAX_BATCH_SIZE> fp_ctx_array_;
//   MultiGetRange* range_;
//   // Iterator to iterate through the keys in a MultiGet batch, that gets reset
//   // at the beginning of each level. Each call to GetNextFile() will position
//   // batch_iter_ at or right after the last key that was found in the returned
//   // SST file
//   MultiGetRange::Iterator batch_iter_;
//   // An iterator that records the previous position of batch_iter_, i.e last
//   // key found in the previous SST file, in order to serve as the start of
//   // the batch key range for the next SST file
//   MultiGetRange::Iterator batch_iter_prev_;
//   bool maybe_repeat_key_;
//   MultiGetRange current_level_range_;
//   MultiGetRange current_file_range_;
//   autovector<LevelFilesBrief>* level_files_brief_;
//   bool search_ended_;
//   bool is_hit_file_last_in_level_;
//   LevelFilesBrief* curr_file_level_;
//   FileIndexer* file_indexer_;
//   const Comparator* user_comparator_;
//   const InternalKeyComparator* internal_comparator_;

//   // Setup local variables to search next level.
//   // Returns false if there are no more levels to search.
//   bool PrepareNextLevel() {
//     if (curr_level_ == 0) {
//       MultiGetRange::Iterator mget_iter = current_level_range_.begin();
//       if (fp_ctx_array_[mget_iter.index()].curr_index_in_curr_level <
//           curr_file_level_->num_files) {
//         batch_iter_prev_ = current_level_range_.begin();
//         batch_iter_ = current_level_range_.begin();
//         return true;
//       }
//     }

//     curr_level_++;
//     // Reset key range to saved value
//     while (curr_level_ < num_levels_) {
//       bool level_contains_keys = false;
//       curr_file_level_ = &(*level_files_brief_)[curr_level_];
//       if (curr_file_level_->num_files == 0) {
//         // When current level is empty, the search bound generated from upper
//         // level must be [0, -1] or [0, FileIndexer::kLevelMaxIndex] if it is
//         // also empty.

//         for (auto mget_iter = current_level_range_.begin();
//              mget_iter != current_level_range_.end(); ++mget_iter) {
//           struct FilePickerContext& fp_ctx = fp_ctx_array_[mget_iter.index()];

//           assert(fp_ctx.search_left_bound == 0);
//           assert(fp_ctx.search_right_bound == -1 ||
//                  fp_ctx.search_right_bound == FileIndexer::kLevelMaxIndex);
//           // Since current level is empty, it will need to search all files in
//           // the next level
//           fp_ctx.search_left_bound = 0;
//           fp_ctx.search_right_bound = FileIndexer::kLevelMaxIndex;
//         }
//         // Skip all subsequent empty levels
//         do {
//           ++curr_level_;
//         } while ((curr_level_ < num_levels_) &&
//                  (*level_files_brief_)[curr_level_].num_files == 0);
//         continue;
//       }

//       // Some files may overlap each other. We find
//       // all files that overlap user_key and process them in order from
//       // newest to oldest. In the context of merge-operator, this can occur at
//       // any level. Otherwise, it only occurs at Level-0 (since Put/Deletes
//       // are always compacted into a single entry).
//       int32_t start_index = -1;
//       current_level_range_ =
//           MultiGetRange(*range_, range_->begin(), range_->end());
//       for (auto mget_iter = current_level_range_.begin();
//            mget_iter != current_level_range_.end(); ++mget_iter) {
//         struct FilePickerContext& fp_ctx = fp_ctx_array_[mget_iter.index()];
//         if (curr_level_ == 0) {
//           // On Level-0, we read through all files to check for overlap.
//           start_index = 0;
//           level_contains_keys = true;
//         } else {
//           // On Level-n (n>=1), files are sorted. Binary search to find the
//           // earliest file whose largest key >= ikey. Search left bound and
//           // right bound are used to narrow the range.
//           if (fp_ctx.search_left_bound <= fp_ctx.search_right_bound) {
//             if (fp_ctx.search_right_bound == FileIndexer::kLevelMaxIndex) {
//               fp_ctx.search_right_bound =
//                   static_cast<int32_t>(curr_file_level_->num_files) - 1;
//             }
//             // `search_right_bound_` is an inclusive upper-bound, but since it
//             // was determined based on user key, it is still possible the lookup
//             // key falls to the right of `search_right_bound_`'s corresponding
//             // file. So, pass a limit one higher, which allows us to detect this
//             // case.
//             Slice& ikey = mget_iter->ikey;
//             start_index = FindFileInRangeTest(
//                 *internal_comparator_, *curr_file_level_, ikey,
//                 static_cast<uint32_t>(fp_ctx.search_left_bound),
//                 static_cast<uint32_t>(fp_ctx.search_right_bound) + 1);
//             if (start_index == fp_ctx.search_right_bound + 1) {
//               // `ikey_` comes after `search_right_bound_`. The lookup key does
//               // not exist on this level, so let's skip this level and do a full
//               // binary search on the next level.
//               fp_ctx.search_left_bound = 0;
//               fp_ctx.search_right_bound = FileIndexer::kLevelMaxIndex;
//               current_level_range_.SkipKey(mget_iter);
//               continue;
//             } else {
//               level_contains_keys = true;
//             }
//           } else {
//             // search_left_bound > search_right_bound, key does not exist in
//             // this level. Since no comparison is done in this level, it will
//             // need to search all files in the next level.
//             fp_ctx.search_left_bound = 0;
//             fp_ctx.search_right_bound = FileIndexer::kLevelMaxIndex;
//             current_level_range_.SkipKey(mget_iter);
//             continue;
//           }
//         }
//         fp_ctx.start_index_in_curr_level = start_index;
//         fp_ctx.curr_index_in_curr_level = start_index;
//         if (curr_file_level_->is_partition_level)
//           fp_ctx.curr_index_in_curr_partition_ = curr_file_level_->partitions[fp_ctx.curr_index_in_curr_level].num_files - 1;
//       }
//       if (level_contains_keys) {
//         batch_iter_prev_ = current_level_range_.begin();
//         batch_iter_ = current_level_range_.begin();
//         return true;
//       }
//       curr_level_++;
//     }
//     // curr_level_ = num_levels_. So, no more levels to search.
//     return false;
//   }
// };

// TEST_F(VersionStorageInfoTest, FilePickerMultiGet1) {
//   Init(-1);
//   Add(1, 1U, "a", "z");
//   Add(2, 2U, "a", "z");
//   Add(3, 3U, "a", "z");
//   vstorage_->UpdateNumNonEmptyLevels();
//   vstorage_->GenerateFileIndexer();
//   vstorage_->GenerateLevelFilesBrief();

//   // Prepare range
//   InternalKey ikey = GetInternalKey("k", 0);
//   Slice key = ikey.Encode();
//   autovector<KeyContext, MultiGetContext::MAX_BATCH_SIZE> key_context;
//   key_context.emplace_back(key, nullptr, nullptr);
//   autovector<KeyContext*, MultiGetContext::MAX_BATCH_SIZE> sorted_keys;
//   sorted_keys.push_back(&key_context[0]);
//   MultiGetContext ctx(&sorted_keys[0], 1, 0);
//   MultiGetRange range = ctx.GetMultiGetRange();
//   FilePickerMultiGetTest fp(
//       &range,
//       vstorage_->level_files_brief_test(), vstorage_->num_non_empty_levels_test(),
//       vstorage_->file_indexer_test(), vstorage_->UserComparator(), vstorage_->InternalComparator());

//   FdWithKeyRange* f = fp.GetNextFile();
//   ASSERT_EQ(1lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f == nullptr);
// }

// TEST_F(VersionStorageInfoTest, FilePickerMultiGet2) {
//   Init(-1);
//   Add(1, 1U, "a", "h");
//   Add(2, 2U, "h", "z");
//   Add(3, 3U, "h", "z");
//   vstorage_->UpdateNumNonEmptyLevels();
//   vstorage_->GenerateFileIndexer();
//   vstorage_->GenerateLevelFilesBrief();

//   // Prepare range
//   InternalKey ikey1 = GetInternalKey("b", 0);
//   Slice key1 = ikey1.Encode();
//   InternalKey ikey2 = GetInternalKey("k", 0);
//   Slice key2 = ikey2.Encode();
//   autovector<KeyContext, MultiGetContext::MAX_BATCH_SIZE> key_context;
//   key_context.emplace_back(key1, nullptr, nullptr);
//   key_context.emplace_back(key2, nullptr, nullptr);
//   autovector<KeyContext*, MultiGetContext::MAX_BATCH_SIZE> sorted_keys;
//   sorted_keys.push_back(&key_context[0]);
//   sorted_keys.push_back(&key_context[1]);
//   MultiGetContext ctx(&sorted_keys[0], 2, 0);
//   MultiGetRange range = ctx.GetMultiGetRange();
//   FilePickerMultiGetTest fp(
//       &range,
//       vstorage_->level_files_brief_test(), vstorage_->num_non_empty_levels_test(),
//       vstorage_->file_indexer_test(), vstorage_->UserComparator(), vstorage_->InternalComparator());
  
//   FdWithKeyRange* f = fp.GetNextFile();
//   ASSERT_EQ(1lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f == nullptr);
// }

// TEST_F(VersionStorageInfoTest, PartitionFilePickerMultiGet1) {
//   Init(2);
//   Add(1, 1U, "a", "z");
//   AddFileNewPartition(2, 2U, "a", "z");
//   AddFileNewPartition(3, 3U, "a", "z");
//   vstorage_->UpdateNumNonEmptyLevels();
//   vstorage_->GenerateFileIndexer();
//   vstorage_->GenerateLevelFilesBrief();

//   // Prepare range
//   InternalKey ikey = GetInternalKey("k", 0);
//   Slice key = ikey.Encode();
//   autovector<KeyContext, MultiGetContext::MAX_BATCH_SIZE> key_context;
//   key_context.emplace_back(key, nullptr, nullptr);
//   autovector<KeyContext*, MultiGetContext::MAX_BATCH_SIZE> sorted_keys;
//   sorted_keys.push_back(&key_context[0]);
//   MultiGetContext ctx(&sorted_keys[0], 1, 0);
//   MultiGetRange range = ctx.GetMultiGetRange();
//   FilePickerMultiGetTest fp(
//       &range,
//       vstorage_->level_files_brief_test(), vstorage_->num_non_empty_levels_test(),
//       vstorage_->file_indexer_test(), vstorage_->UserComparator(), vstorage_->InternalComparator());

//   FdWithKeyRange* f = fp.GetNextFile();
//   ASSERT_EQ(1lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f == nullptr);
// }

// TEST_F(VersionStorageInfoTest, PartitionFilePickerMultiGet2) {
//   Init(2);
//   Add(1, 1U, "a", "z");
//   AddFileNewPartition(2, 2U, "a", "l");
//   AddFileOldPartition(2, 3U, "j", "z");
//   vstorage_->UpdateNumNonEmptyLevels();
//   vstorage_->GenerateFileIndexer();
//   vstorage_->GenerateLevelFilesBrief();

//   // Prepare range
//   InternalKey ikey = GetInternalKey("k", 0);
//   Slice key = ikey.Encode();
//   autovector<KeyContext, MultiGetContext::MAX_BATCH_SIZE> key_context;
//   key_context.emplace_back(key, nullptr, nullptr);
//   autovector<KeyContext*, MultiGetContext::MAX_BATCH_SIZE> sorted_keys;
//   sorted_keys.push_back(&key_context[0]);
//   MultiGetContext ctx(&sorted_keys[0], 1, 0);
//   MultiGetRange range = ctx.GetMultiGetRange();
//   FilePickerMultiGetTest fp(
//       &range,
//       vstorage_->level_files_brief_test(), vstorage_->num_non_empty_levels_test(),
//       vstorage_->file_indexer_test(), vstorage_->UserComparator(), vstorage_->InternalComparator());

//   FdWithKeyRange* f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(1lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f == nullptr);
// }

// TEST_F(VersionStorageInfoTest, PartitionFilePickerMultiGet3) {
//   Init(2);
//   Add(1, 1U, "a", "z");
//   AddFileNewPartition(2, 2U, "a", "l");
//   AddFileOldPartition(2, 3U, "j", "z");
//   vstorage_->UpdateNumNonEmptyLevels();
//   vstorage_->GenerateFileIndexer();
//   vstorage_->GenerateLevelFilesBrief();

//   // Prepare range
//   InternalKey ikey1 = GetInternalKey("b", 0);
//   Slice key1 = ikey1.Encode();
//   InternalKey ikey2 = GetInternalKey("k", 0);
//   Slice key2 = ikey2.Encode();
//   autovector<KeyContext, MultiGetContext::MAX_BATCH_SIZE> key_context;
//   key_context.emplace_back(key1, nullptr, nullptr);
//   key_context.emplace_back(key2, nullptr, nullptr);
//   autovector<KeyContext*, MultiGetContext::MAX_BATCH_SIZE> sorted_keys;
//   sorted_keys.push_back(&key_context[0]);
//   sorted_keys.push_back(&key_context[1]);
//   MultiGetContext ctx(&sorted_keys[0], 2, 0);
//   MultiGetRange range = ctx.GetMultiGetRange();
//   FilePickerMultiGetTest fp(
//       &range,
//       vstorage_->level_files_brief_test(), vstorage_->num_non_empty_levels_test(),
//       vstorage_->file_indexer_test(), vstorage_->UserComparator(), vstorage_->InternalComparator());
  
//   FdWithKeyRange* f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(1lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f == nullptr);
// }

// TEST_F(VersionStorageInfoTest, PartitionFilePickerMultiGet4) {
//   Init(2);
//   Add(1, 1U, "a", "z");
//   AddFileNewPartition(2, 2U, "a", "l");
//   AddFileNewPartition(3, 3U, "j", "z");
//   vstorage_->UpdateNumNonEmptyLevels();
//   vstorage_->GenerateFileIndexer();
//   vstorage_->GenerateLevelFilesBrief();

//   // Prepare range
//   InternalKey ikey1 = GetInternalKey("b", 0);
//   Slice key1 = ikey1.Encode();
//   InternalKey ikey2 = GetInternalKey("k", 0);
//   Slice key2 = ikey2.Encode();
//   autovector<KeyContext, MultiGetContext::MAX_BATCH_SIZE> key_context;
//   key_context.emplace_back(key1, nullptr, nullptr);
//   key_context.emplace_back(key2, nullptr, nullptr);
//   autovector<KeyContext*, MultiGetContext::MAX_BATCH_SIZE> sorted_keys;
//   sorted_keys.push_back(&key_context[0]);
//   sorted_keys.push_back(&key_context[1]);
//   MultiGetContext ctx(&sorted_keys[0], 2, 0);
//   MultiGetRange range = ctx.GetMultiGetRange();
//   FilePickerMultiGetTest fp(
//       &range,
//       vstorage_->level_files_brief_test(), vstorage_->num_non_empty_levels_test(),
//       vstorage_->file_indexer_test(), vstorage_->UserComparator(), vstorage_->InternalComparator());
  
//   FdWithKeyRange* f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(1lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f == nullptr);
// }

// TEST_F(VersionStorageInfoTest, PartitionFilePickerMultiGet5) {
//   Init(2);
//   Add(1, 1U, "a", "z");
//   AddFileNewPartition(2, 2U, "j", "z");
//   AddFileOldPartition(2, 3U, "a", "l");
//   vstorage_->UpdateNumNonEmptyLevels();
//   vstorage_->GenerateFileIndexer();
//   vstorage_->GenerateLevelFilesBrief();

//   // Prepare range
//   InternalKey ikey1 = GetInternalKey("b", 0);
//   Slice key1 = ikey1.Encode();
//   InternalKey ikey2 = GetInternalKey("k", 0);
//   Slice key2 = ikey2.Encode();
//   autovector<KeyContext, MultiGetContext::MAX_BATCH_SIZE> key_context;
//   key_context.emplace_back(key1, nullptr, nullptr);
//   key_context.emplace_back(key2, nullptr, nullptr);
//   autovector<KeyContext*, MultiGetContext::MAX_BATCH_SIZE> sorted_keys;
//   sorted_keys.push_back(&key_context[0]);
//   sorted_keys.push_back(&key_context[1]);
//   MultiGetContext ctx(&sorted_keys[0], 2, 0);
//   MultiGetRange range = ctx.GetMultiGetRange();
//   FilePickerMultiGetTest fp(
//       &range,
//       vstorage_->level_files_brief_test(), vstorage_->num_non_empty_levels_test(),
//       vstorage_->file_indexer_test(), vstorage_->UserComparator(), vstorage_->InternalComparator());
  
//   FdWithKeyRange* f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(1lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(3lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f != nullptr);
//   ASSERT_EQ(2lu, f->fd.GetNumber());
//   f = fp.GetNextFile();
//   ASSERT_TRUE(f == nullptr);
// }

TEST_F(VersionStorageInfoTest, MaxBytesForLevelStatic) {
  Init(-1);
  ioptions_.level_compaction_dynamic_level_bytes = false;
  mutable_cf_options_.max_bytes_for_level_base = 10;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  Add(4, 100U, "1", "2");
  Add(5, 101U, "1", "2");

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(1), 10U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 50U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 250U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1250U);

  ASSERT_EQ(0, logger_->log_count);
}

TEST_F(VersionStorageInfoTest, MaxBytesForLevelDynamic) {
  Init(-1);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 1000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  Add(5, 1U, "1", "2", 500U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(vstorage_->base_level(), 5);

  Add(5, 2U, "3", "4", 550U);
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 4);

  Add(4, 3U, "3", "4", 550U);
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 4);

  Add(3, 4U, "3", "4", 250U);
  Add(3, 5U, "5", "7", 300U);
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(1, logger_->log_count);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1005U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 3);

  Add(1, 6U, "3", "4", 5U);
  Add(1, 7U, "8", "9", 5U);
  logger_->log_count = 0;
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(1, logger_->log_count);
  ASSERT_GT(vstorage_->MaxBytesForLevel(4), 1005U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(3), 1005U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 1005U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(1), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 1);
}

TEST_F(VersionStorageInfoTest, MaxBytesForLevelDynamicLotsOfData) {
  Init(-1);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 100;
  mutable_cf_options_.max_bytes_for_level_multiplier = 2;
  Add(0, 1U, "1", "2", 50U);
  Add(1, 2U, "1", "2", 50U);
  Add(2, 3U, "1", "2", 500U);
  Add(3, 4U, "1", "2", 500U);
  Add(4, 5U, "1", "2", 1700U);
  Add(5, 6U, "1", "2", 500U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 800U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 400U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 200U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(1), 100U);
  ASSERT_EQ(vstorage_->base_level(), 1);
  ASSERT_EQ(0, logger_->log_count);
}

TEST_F(VersionStorageInfoTest, MaxBytesForLevelDynamicLargeLevel) {
  Init(-1);
  uint64_t kOneGB = 1000U * 1000U * 1000U;
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 10U * kOneGB;
  mutable_cf_options_.max_bytes_for_level_multiplier = 10;
  Add(0, 1U, "1", "2", 50U);
  Add(3, 4U, "1", "2", 32U * kOneGB);
  Add(4, 5U, "1", "2", 500U * kOneGB);
  Add(5, 6U, "1", "2", 3000U * kOneGB);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(5), 3000U * kOneGB);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 300U * kOneGB);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 30U * kOneGB);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 10U * kOneGB);
  ASSERT_EQ(vstorage_->base_level(), 2);
  ASSERT_EQ(0, logger_->log_count);
}

TEST_F(VersionStorageInfoTest, MaxBytesForLevelDynamicWithLargeL0_1) {
  Init(-1);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 40000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  mutable_cf_options_.level0_file_num_compaction_trigger = 2;

  Add(0, 1U, "1", "2", 10000U);
  Add(0, 2U, "1", "2", 10000U);
  Add(0, 3U, "1", "2", 10000U);

  Add(5, 4U, "1", "2", 1286250U);
  Add(4, 5U, "1", "2", 200000U);
  Add(3, 6U, "1", "2", 40000U);
  Add(2, 7U, "1", "2", 8000U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(2, vstorage_->base_level());
  // level multiplier should be 3.5
  ASSERT_EQ(vstorage_->level_multiplier(), 5.0);
  // Level size should be around 30,000, 105,000, 367,500
  ASSERT_EQ(40000U, vstorage_->MaxBytesForLevel(2));
  ASSERT_EQ(51450U, vstorage_->MaxBytesForLevel(3));
  ASSERT_EQ(257250U, vstorage_->MaxBytesForLevel(4));
}

TEST_F(VersionStorageInfoTest, MaxBytesForLevelDynamicWithLargeL0_2) {
  Init(-1);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 10000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  mutable_cf_options_.level0_file_num_compaction_trigger = 2;

  Add(0, 11U, "1", "2", 10000U);
  Add(0, 12U, "1", "2", 10000U);
  Add(0, 13U, "1", "2", 10000U);

  Add(5, 4U, "1", "2", 1286250U);
  Add(4, 5U, "1", "2", 200000U);
  Add(3, 6U, "1", "2", 40000U);
  Add(2, 7U, "1", "2", 8000U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(2, vstorage_->base_level());
  // level multiplier should be 3.5
  ASSERT_LT(vstorage_->level_multiplier(), 3.6);
  ASSERT_GT(vstorage_->level_multiplier(), 3.4);
  // Level size should be around 30,000, 105,000, 367,500
  ASSERT_EQ(30000U, vstorage_->MaxBytesForLevel(2));
  ASSERT_LT(vstorage_->MaxBytesForLevel(3), 110000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(3), 100000U);
  ASSERT_LT(vstorage_->MaxBytesForLevel(4), 370000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(4), 360000U);
}

TEST_F(VersionStorageInfoTest, MaxBytesForLevelDynamicWithLargeL0_3) {
  Init(-1);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 10000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  mutable_cf_options_.level0_file_num_compaction_trigger = 2;

  Add(0, 11U, "1", "2", 5000U);
  Add(0, 12U, "1", "2", 5000U);
  Add(0, 13U, "1", "2", 5000U);
  Add(0, 14U, "1", "2", 5000U);
  Add(0, 15U, "1", "2", 5000U);
  Add(0, 16U, "1", "2", 5000U);

  Add(5, 4U, "1", "2", 1286250U);
  Add(4, 5U, "1", "2", 200000U);
  Add(3, 6U, "1", "2", 40000U);
  Add(2, 7U, "1", "2", 8000U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(2, vstorage_->base_level());
  // level multiplier should be 3.5
  ASSERT_LT(vstorage_->level_multiplier(), 3.6);
  ASSERT_GT(vstorage_->level_multiplier(), 3.4);
  // Level size should be around 30,000, 105,000, 367,500
  ASSERT_EQ(30000U, vstorage_->MaxBytesForLevel(2));
  ASSERT_LT(vstorage_->MaxBytesForLevel(3), 110000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(3), 100000U);
  ASSERT_LT(vstorage_->MaxBytesForLevel(4), 370000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(4), 360000U);
}

TEST_F(VersionStorageInfoTest, EstimateLiveDataSize) {
  Init(-1);
  // Test whether the overlaps are detected as expected
  Add(1, 1U, "4", "7", 1U);  // Perfect overlap with last level
  Add(2, 2U, "3", "5", 1U);  // Partial overlap with last level
  Add(2, 3U, "6", "8", 1U);  // Partial overlap with last level
  Add(3, 4U, "1", "9", 1U);  // Contains range of last level
  Add(4, 5U, "4", "5", 1U);  // Inside range of last level
  Add(4, 5U, "6", "7", 1U);  // Inside range of last level
  Add(5, 6U, "4", "7", 10U);
  ASSERT_EQ(10U, vstorage_->EstimateLiveDataSize());
}

TEST_F(VersionStorageInfoTest, EstimateLiveDataSize2) {
  Init(-1);
  Add(0, 1U, "9", "9", 1U);  // Level 0 is not ordered
  Add(0, 1U, "5", "6", 1U);  // Ignored because of [5,6] in l1
  Add(1, 1U, "1", "2", 1U);  // Ignored because of [2,3] in l2
  Add(1, 2U, "3", "4", 1U);  // Ignored because of [2,3] in l2
  Add(1, 3U, "5", "6", 1U);
  Add(2, 4U, "2", "3", 1U);
  Add(3, 5U, "7", "8", 1U);
  ASSERT_EQ(4U, vstorage_->EstimateLiveDataSize());
}

TEST_F(VersionStorageInfoTest, GetOverlappingInputs) {
  Init(-1);
  // Two files that overlap at the range deletion tombstone sentinel.
  Add(1, 1U, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}, 1);
  Add(1, 2U, {"b", 0, kTypeValue}, {"c", 0, kTypeValue}, 1);
  // Two files that overlap at the same user key.
  Add(1, 3U, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeValue}, 1);
  Add(1, 4U, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}, 1);
  // Two files that do not overlap.
  Add(1, 5U, {"g", 0, kTypeValue}, {"h", 0, kTypeValue}, 1);
  Add(1, 6U, {"i", 0, kTypeValue}, {"j", 0, kTypeValue}, 1);
  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateLevelFilesBrief();

  ASSERT_EQ("1,2", GetOverlappingFiles(
      1, {"a", 0, kTypeValue}, {"b", 0, kTypeValue}));
  ASSERT_EQ("1", GetOverlappingFiles(
      1, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("2", GetOverlappingFiles(
      1, {"b", kMaxSequenceNumber, kTypeValue}, {"c", 0, kTypeValue}));
  ASSERT_EQ("3,4", GetOverlappingFiles(
      1, {"d", 0, kTypeValue}, {"e", 0, kTypeValue}));
  ASSERT_EQ("3", GetOverlappingFiles(
      1, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("3,4", GetOverlappingFiles(
      1, {"e", kMaxSequenceNumber, kTypeValue}, {"f", 0, kTypeValue}));
  ASSERT_EQ("3,4", GetOverlappingFiles(
      1, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}));
  ASSERT_EQ("5", GetOverlappingFiles(
      1, {"g", 0, kTypeValue}, {"h", 0, kTypeValue}));
  ASSERT_EQ("6", GetOverlappingFiles(
      1, {"i", 0, kTypeValue}, {"j", 0, kTypeValue}));
}

TEST_F(VersionStorageInfoTest, PartitionMaxBytesForLevelStatic) {
  Init(2);
  ioptions_.level_compaction_dynamic_level_bytes = false;
  mutable_cf_options_.max_bytes_for_level_base = 10;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  AddFileNewPartition(4, 100U, "1", "2");
  AddFileNewPartition(5, 101U, "1", "2");

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(1), 10U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 50U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 250U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1250U);

  ASSERT_EQ(0, logger_->log_count);
}

TEST_F(VersionStorageInfoTest, PartitionMaxBytesForLevelDynamic) {
  Init(2);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 1000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  AddFileNewPartition(5, 1U, "1", "2", 500U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(vstorage_->base_level(), 5);

  AddFileNewPartition(5, 2U, "3", "4", 550U);
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 4);

  AddFileNewPartition(4, 3U, "3", "4", 550U);
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 4);

  AddFileNewPartition(3, 4U, "3", "4", 250U);
  AddFileNewPartition(3, 5U, "5", "7", 300U);
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(1, logger_->log_count);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 1005U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 3);

  Add(1, 6U, "3", "4", 5U);
  Add(1, 7U, "8", "9", 5U);
  logger_->log_count = 0;
  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(1, logger_->log_count);
  ASSERT_GT(vstorage_->MaxBytesForLevel(4), 1005U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(3), 1005U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 1005U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(1), 1000U);
  ASSERT_EQ(vstorage_->base_level(), 1);
}

TEST_F(VersionStorageInfoTest, PartitionMaxBytesForLevelDynamicLotsOfData) {
  Init(2);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 100;
  mutable_cf_options_.max_bytes_for_level_multiplier = 2;
  Add(0, 1U, "1", "2", 50U);
  Add(1, 2U, "1", "2", 50U);
  AddFileNewPartition(2, 3U, "1", "2", 500U);
  AddFileNewPartition(3, 4U, "1", "2", 500U);
  AddFileNewPartition(4, 5U, "1", "2", 1700U);
  AddFileNewPartition(5, 6U, "1", "2", 500U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 800U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 400U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 200U);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(1), 100U);
  ASSERT_EQ(vstorage_->base_level(), 1);
  ASSERT_EQ(0, logger_->log_count);
}

TEST_F(VersionStorageInfoTest, PartitionMaxBytesForLevelDynamicLargeLevel) {
  Init(2);
  uint64_t kOneGB = 1000U * 1000U * 1000U;
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 10U * kOneGB;
  mutable_cf_options_.max_bytes_for_level_multiplier = 10;
  Add(0, 1U, "1", "2", 50U);
  AddFileNewPartition(3, 4U, "1", "2", 32U * kOneGB);
  AddFileNewPartition(4, 5U, "1", "2", 500U * kOneGB);
  AddFileNewPartition(5, 6U, "1", "2", 3000U * kOneGB);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(5), 3000U * kOneGB);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(4), 300U * kOneGB);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(3), 30U * kOneGB);
  ASSERT_EQ(vstorage_->MaxBytesForLevel(2), 10U * kOneGB);
  ASSERT_EQ(vstorage_->base_level(), 2);
  ASSERT_EQ(0, logger_->log_count);
}

TEST_F(VersionStorageInfoTest, PartitionMaxBytesForLevelDynamicWithLargeL0_1) {
  Init(2);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 40000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  mutable_cf_options_.level0_file_num_compaction_trigger = 2;

  Add(0, 1U, "1", "2", 10000U);
  Add(0, 2U, "1", "2", 10000U);
  Add(0, 3U, "1", "2", 10000U);

  AddFileNewPartition(5, 4U, "1", "2", 1286250U);
  AddFileNewPartition(4, 5U, "1", "2", 200000U);
  AddFileNewPartition(3, 6U, "1", "2", 40000U);
  AddFileNewPartition(2, 7U, "1", "2", 8000U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(2, vstorage_->base_level());
  // level multiplier should be 3.5
  ASSERT_EQ(vstorage_->level_multiplier(), 5.0);
  // Level size should be around 30,000, 105,000, 367,500
  ASSERT_EQ(40000U, vstorage_->MaxBytesForLevel(2));
  ASSERT_EQ(51450U, vstorage_->MaxBytesForLevel(3));
  ASSERT_EQ(257250U, vstorage_->MaxBytesForLevel(4));
}

TEST_F(VersionStorageInfoTest, PartitionMaxBytesForLevelDynamicWithLargeL0_2) {
  Init(2);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 10000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  mutable_cf_options_.level0_file_num_compaction_trigger = 2;

  Add(0, 11U, "1", "2", 10000U);
  Add(0, 12U, "1", "2", 10000U);
  Add(0, 13U, "1", "2", 10000U);

  AddFileNewPartition(5, 4U, "1", "2", 1286250U);
  AddFileNewPartition(4, 5U, "1", "2", 200000U);
  AddFileNewPartition(3, 6U, "1", "2", 40000U);
  AddFileNewPartition(2, 7U, "1", "2", 8000U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(2, vstorage_->base_level());
  // level multiplier should be 3.5
  ASSERT_LT(vstorage_->level_multiplier(), 3.6);
  ASSERT_GT(vstorage_->level_multiplier(), 3.4);
  // Level size should be around 30,000, 105,000, 367,500
  ASSERT_EQ(30000U, vstorage_->MaxBytesForLevel(2));
  ASSERT_LT(vstorage_->MaxBytesForLevel(3), 110000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(3), 100000U);
  ASSERT_LT(vstorage_->MaxBytesForLevel(4), 370000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(4), 360000U);
}

TEST_F(VersionStorageInfoTest, PartitionMaxBytesForLevelDynamicWithLargeL0_3) {
  Init(2);
  ioptions_.level_compaction_dynamic_level_bytes = true;
  mutable_cf_options_.max_bytes_for_level_base = 10000;
  mutable_cf_options_.max_bytes_for_level_multiplier = 5;
  mutable_cf_options_.level0_file_num_compaction_trigger = 2;

  Add(0, 11U, "1", "2", 5000U);
  Add(0, 12U, "1", "2", 5000U);
  Add(0, 13U, "1", "2", 5000U);
  Add(0, 14U, "1", "2", 5000U);
  Add(0, 15U, "1", "2", 5000U);
  Add(0, 16U, "1", "2", 5000U);

  AddFileNewPartition(5, 4U, "1", "2", 1286250U);
  AddFileNewPartition(4, 5U, "1", "2", 200000U);
  AddFileNewPartition(3, 6U, "1", "2", 40000U);
  AddFileNewPartition(2, 7U, "1", "2", 8000U);

  vstorage_->CalculateBaseBytes(ioptions_, mutable_cf_options_);
  ASSERT_EQ(0, logger_->log_count);
  ASSERT_EQ(2, vstorage_->base_level());
  // level multiplier should be 3.5
  ASSERT_LT(vstorage_->level_multiplier(), 3.6);
  ASSERT_GT(vstorage_->level_multiplier(), 3.4);
  // Level size should be around 30,000, 105,000, 367,500
  ASSERT_EQ(30000U, vstorage_->MaxBytesForLevel(2));
  ASSERT_LT(vstorage_->MaxBytesForLevel(3), 110000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(3), 100000U);
  ASSERT_LT(vstorage_->MaxBytesForLevel(4), 370000U);
  ASSERT_GT(vstorage_->MaxBytesForLevel(4), 360000U);
}

TEST_F(VersionStorageInfoTest, PartitionEstimateLiveDataSize) {
  Init(2);
  // Test whether the overlaps are detected as expected
  Add(1, 1U, "4", "7", 1U);  // Perfect overlap with last level
  AddFileNewPartition(2, 2U, "3", "5", 1U);  // Partial overlap with last level
  AddFileNewPartition(2, 3U, "6", "8", 1U);  // Partial overlap with last level
  AddFileNewPartition(3, 4U, "1", "9", 1U);  // Contains range of last level
  AddFileNewPartition(4, 5U, "4", "5", 1U);  // Inside range of last level
  AddFileNewPartition(4, 5U, "6", "7", 1U);  // Inside range of last level
  AddFileNewPartition(5, 6U, "4", "7", 10U);
  ASSERT_EQ(10U, vstorage_->EstimateLiveDataSize());
}

TEST_F(VersionStorageInfoTest, PartitionEstimateLiveDataSize2) {
  Init(2);
  Add(0, 1U, "9", "9", 1U);  // Level 0 is not ordered
  Add(0, 1U, "5", "6", 1U);  // Ignored because of [5,6] in l1
  Add(1, 1U, "1", "2", 1U);  // Ignored because of [2,3] in l2
  Add(1, 2U, "3", "4", 1U);  // Ignored because of [2,3] in l2
  Add(1, 3U, "5", "6", 1U);
  AddFileNewPartition(2, 4U, "2", "3", 1U);
  AddFileNewPartition(3, 5U, "7", "8", 1U);
  ASSERT_EQ(4U, vstorage_->EstimateLiveDataSize());
}

TEST_F(VersionStorageInfoTest, PartitionGetOverlappingInputs) {
  Init(2);
  // Two files that overlap at the range deletion tombstone sentinel.
  Add(1, 1U, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}, 1);
  Add(1, 2U, {"b", 0, kTypeValue}, {"c", 0, kTypeValue}, 1);
  // Two files that overlap at the same user key.
  Add(1, 3U, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeValue}, 1);
  Add(1, 4U, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}, 1);
  // Two files that do not overlap.
  Add(1, 5U, {"g", 0, kTypeValue}, {"h", 0, kTypeValue}, 1);
  Add(1, 6U, {"i", 0, kTypeValue}, {"j", 0, kTypeValue}, 1);

  AddFileNewPartition(2, 7U, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}, 1);
  AddFileOldPartition(2, 8U, {"b", 0, kTypeValue}, {"c", 0, kTypeValue}, 1);
  AddFileNewPartition(2, 9U, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeValue}, 1);
  AddFileOldPartition(2, 10U, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}, 1);
  AddFileNewPartition(2, 11U, {"g", 0, kTypeValue}, {"h", 0, kTypeValue}, 1);
  AddFileOldPartition(2, 12U, {"i", 0, kTypeValue}, {"j", 0, kTypeValue}, 1);

  AddFileNewPartition(3, 13U, {"b", 0, kTypeValue}, {"c", 0, kTypeValue}, 1);
  AddFileOldPartition(3, 14U, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}, 1);
  AddFileNewPartition(3, 15U, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}, 1);
  AddFileOldPartition(3, 16U, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeValue}, 1);

  vstorage_->UpdateNumNonEmptyLevels();
  vstorage_->GenerateLevelFilesBrief();

  ASSERT_EQ("1,2", GetOverlappingFiles(
      1, {"a", 0, kTypeValue}, {"b", 0, kTypeValue}));
  ASSERT_EQ("1", GetOverlappingFiles(
      1, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("2", GetOverlappingFiles(
      1, {"b", kMaxSequenceNumber, kTypeValue}, {"c", 0, kTypeValue}));
  ASSERT_EQ("3,4", GetOverlappingFiles(
      1, {"d", 0, kTypeValue}, {"e", 0, kTypeValue}));
  ASSERT_EQ("3", GetOverlappingFiles(
      1, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("3,4", GetOverlappingFiles(
      1, {"e", kMaxSequenceNumber, kTypeValue}, {"f", 0, kTypeValue}));
  ASSERT_EQ("3,4", GetOverlappingFiles(
      1, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}));
  ASSERT_EQ("5", GetOverlappingFiles(
      1, {"g", 0, kTypeValue}, {"h", 0, kTypeValue}));
  ASSERT_EQ("6", GetOverlappingFiles(
      1, {"i", 0, kTypeValue}, {"j", 0, kTypeValue}));

  ASSERT_EQ("7,8", GetOverlappingFiles(
      2, {"a", 0, kTypeValue}, {"b", 0, kTypeValue}));
  ASSERT_EQ("7", GetOverlappingFiles(
      2, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("8", GetOverlappingFiles(
      2, {"b", kMaxSequenceNumber, kTypeValue}, {"c", 0, kTypeValue}));
  ASSERT_EQ("9,10", GetOverlappingFiles(
      2, {"d", 0, kTypeValue}, {"e", 0, kTypeValue}));
  ASSERT_EQ("9", GetOverlappingFiles(
      2, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("9,10", GetOverlappingFiles(
      2, {"e", kMaxSequenceNumber, kTypeValue}, {"f", 0, kTypeValue}));
  ASSERT_EQ("9,10", GetOverlappingFiles(
      2, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}));
  ASSERT_EQ("11", GetOverlappingFiles(
      2, {"g", 0, kTypeValue}, {"h", 0, kTypeValue}));
  ASSERT_EQ("12", GetOverlappingFiles(
      2, {"i", 0, kTypeValue}, {"j", 0, kTypeValue}));

  ASSERT_EQ("13,14", GetOverlappingFiles(
      3, {"a", 0, kTypeValue}, {"b", 0, kTypeValue}));
  ASSERT_EQ("14", GetOverlappingFiles(
      3, {"a", 0, kTypeValue}, {"b", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("13", GetOverlappingFiles(
      3, {"b", kMaxSequenceNumber, kTypeValue}, {"c", 0, kTypeValue}));
  ASSERT_EQ("15,16", GetOverlappingFiles(
      3, {"d", 0, kTypeValue}, {"e", 0, kTypeValue}));
  ASSERT_EQ("16", GetOverlappingFiles(
      3, {"d", 0, kTypeValue}, {"e", kMaxSequenceNumber, kTypeRangeDeletion}));
  ASSERT_EQ("15,16", GetOverlappingFiles(
      3, {"e", kMaxSequenceNumber, kTypeValue}, {"f", 0, kTypeValue}));
  ASSERT_EQ("15,16", GetOverlappingFiles(
      3, {"e", 0, kTypeValue}, {"f", 0, kTypeValue}));
}

class FindLevelFileTest : public testing::Test {
 public:
  LevelFilesBrief file_level_;
  bool disjoint_sorted_files_;
  Arena arena_;

  FindLevelFileTest() : disjoint_sorted_files_(true) { }

  ~FindLevelFileTest() override {}

  void LevelFileInit(size_t num = 0) {
    char* mem = arena_.AllocateAligned(num * sizeof(FdWithKeyRange));
    file_level_.files = new (mem)FdWithKeyRange[num];
    file_level_.num_files = 0;
    file_level_.is_partition_level = false;
  }

  void LevelPartitionFileInit(size_t num = 0) {
    char* mem = arena_.AllocateAligned(num * sizeof(PartitionWithKeyRange));
    file_level_.partitions = new (mem)PartitionWithKeyRange[num];
    file_level_.num_files = 0;
    file_level_.is_partition_level = true;
  }

  void Add(const char* smallest, const char* largest,
           SequenceNumber smallest_seq = 100,
           SequenceNumber largest_seq = 100) {
    InternalKey smallest_key = InternalKey(smallest, smallest_seq, kTypeValue);
    InternalKey largest_key = InternalKey(largest, largest_seq, kTypeValue);

    Slice smallest_slice = smallest_key.Encode();
    Slice largest_slice = largest_key.Encode();

    char* mem = arena_.AllocateAligned(
        smallest_slice.size() + largest_slice.size());
    memcpy(mem, smallest_slice.data(), smallest_slice.size());
    memcpy(mem + smallest_slice.size(), largest_slice.data(),
        largest_slice.size());

    // add to file_level_
    size_t num = file_level_.num_files;
    auto& file = file_level_.files[num];
    file.fd = FileDescriptor(num + 1, 0, 0);
    file.smallest_key = Slice(mem, smallest_slice.size());
    file.largest_key = Slice(mem + smallest_slice.size(),
        largest_slice.size());
    file_level_.num_files++;
  }

  void AddPartition(const char* smallest, const char* largest,
           SequenceNumber smallest_seq = 100,
           SequenceNumber largest_seq = 100) {
    InternalKey smallest_key = InternalKey(smallest, smallest_seq, kTypeValue);
    InternalKey largest_key = InternalKey(largest, largest_seq, kTypeValue);

    Slice smallest_slice = smallest_key.Encode();
    Slice largest_slice = largest_key.Encode();

    char* mem = arena_.AllocateAligned(
        smallest_slice.size() + largest_slice.size());
    memcpy(mem, smallest_slice.data(), smallest_slice.size());
    memcpy(mem + smallest_slice.size(), largest_slice.data(),
        largest_slice.size());

    // add to file_level_
    size_t num = file_level_.num_files;
    auto& partition = file_level_.partitions[num];
    partition.num_files = 1;
    partition.smallest_key = Slice(mem, smallest_slice.size());
    partition.largest_key = Slice(mem + smallest_slice.size(),
        largest_slice.size());

    char* mem1 = arena_.AllocateAligned(sizeof(FdWithKeyRange));
    partition.file_metadata = new (mem1)FdWithKeyRange[1];
    partition.file_metadata[0].fd = FileDescriptor(num + 1, 0, 0);
    partition.file_metadata[0].smallest_key = Slice(mem, smallest_slice.size());
    partition.file_metadata[0].largest_key = Slice(mem + smallest_slice.size(),
        largest_slice.size());
    file_level_.num_files++;
  }

  int Find(const char* key) {
    InternalKey target(key, 100, kTypeValue);
    InternalKeyComparator cmp(BytewiseComparator());
    return FindFile(cmp, file_level_, target.Encode());
  }

  bool Overlaps(const char* smallest, const char* largest) {
    InternalKeyComparator cmp(BytewiseComparator());
    Slice s(smallest != nullptr ? smallest : "");
    Slice l(largest != nullptr ? largest : "");
    return SomeFileOverlapsRange(cmp, disjoint_sorted_files_, file_level_,
                                 (smallest != nullptr ? &s : nullptr),
                                 (largest != nullptr ? &l : nullptr));
  }
};

TEST_F(FindLevelFileTest, LevelEmpty) {
  LevelFileInit(0);

  ASSERT_EQ(0, Find("foo"));
  ASSERT_TRUE(! Overlaps("a", "z"));
  ASSERT_TRUE(! Overlaps(nullptr, "z"));
  ASSERT_TRUE(! Overlaps("a", nullptr));
  ASSERT_TRUE(! Overlaps(nullptr, nullptr));
}

TEST_F(FindLevelFileTest, LevelPartitionEmpty) {
  LevelPartitionFileInit(0);

  ASSERT_EQ(0, Find("foo"));
  ASSERT_TRUE(! Overlaps("a", "z"));
  ASSERT_TRUE(! Overlaps(nullptr, "z"));
  ASSERT_TRUE(! Overlaps("a", nullptr));
  ASSERT_TRUE(! Overlaps(nullptr, nullptr));
}

TEST_F(FindLevelFileTest, LevelSingle) {
  LevelFileInit(1);

  Add("p", "q");
  ASSERT_EQ(0, Find("a"));
  ASSERT_EQ(0, Find("p"));
  ASSERT_EQ(0, Find("p1"));
  ASSERT_EQ(0, Find("q"));
  ASSERT_EQ(1, Find("q1"));
  ASSERT_EQ(1, Find("z"));

  ASSERT_TRUE(! Overlaps("a", "b"));
  ASSERT_TRUE(! Overlaps("z1", "z2"));
  ASSERT_TRUE(Overlaps("a", "p"));
  ASSERT_TRUE(Overlaps("a", "q"));
  ASSERT_TRUE(Overlaps("a", "z"));
  ASSERT_TRUE(Overlaps("p", "p1"));
  ASSERT_TRUE(Overlaps("p", "q"));
  ASSERT_TRUE(Overlaps("p", "z"));
  ASSERT_TRUE(Overlaps("p1", "p2"));
  ASSERT_TRUE(Overlaps("p1", "z"));
  ASSERT_TRUE(Overlaps("q", "q"));
  ASSERT_TRUE(Overlaps("q", "q1"));

  ASSERT_TRUE(! Overlaps(nullptr, "j"));
  ASSERT_TRUE(! Overlaps("r", nullptr));
  ASSERT_TRUE(Overlaps(nullptr, "p"));
  ASSERT_TRUE(Overlaps(nullptr, "p1"));
  ASSERT_TRUE(Overlaps("q", nullptr));
  ASSERT_TRUE(Overlaps(nullptr, nullptr));
}

TEST_F(FindLevelFileTest, LevelPartitionSingle) {
  LevelPartitionFileInit(1);

  AddPartition("p", "q");
  ASSERT_EQ(0, Find("a"));
  ASSERT_EQ(0, Find("p"));
  ASSERT_EQ(0, Find("p1"));
  ASSERT_EQ(0, Find("q"));
  ASSERT_EQ(1, Find("q1"));
  ASSERT_EQ(1, Find("z"));

  ASSERT_TRUE(! Overlaps("a", "b"));
  ASSERT_TRUE(! Overlaps("z1", "z2"));
  ASSERT_TRUE(Overlaps("a", "p"));
  ASSERT_TRUE(Overlaps("a", "q"));
  ASSERT_TRUE(Overlaps("a", "z"));
  ASSERT_TRUE(Overlaps("p", "p1"));
  ASSERT_TRUE(Overlaps("p", "q"));
  ASSERT_TRUE(Overlaps("p", "z"));
  ASSERT_TRUE(Overlaps("p1", "p2"));
  ASSERT_TRUE(Overlaps("p1", "z"));
  ASSERT_TRUE(Overlaps("q", "q"));
  ASSERT_TRUE(Overlaps("q", "q1"));

  ASSERT_TRUE(! Overlaps(nullptr, "j"));
  ASSERT_TRUE(! Overlaps("r", nullptr));
  ASSERT_TRUE(Overlaps(nullptr, "p"));
  ASSERT_TRUE(Overlaps(nullptr, "p1"));
  ASSERT_TRUE(Overlaps("q", nullptr));
  ASSERT_TRUE(Overlaps(nullptr, nullptr));
}

TEST_F(FindLevelFileTest, LevelMultiple) {
  LevelFileInit(4);

  Add("150", "200");
  Add("200", "250");
  Add("300", "350");
  Add("400", "450");
  ASSERT_EQ(0, Find("100"));
  ASSERT_EQ(0, Find("150"));
  ASSERT_EQ(0, Find("151"));
  ASSERT_EQ(0, Find("199"));
  ASSERT_EQ(0, Find("200"));
  ASSERT_EQ(1, Find("201"));
  ASSERT_EQ(1, Find("249"));
  ASSERT_EQ(1, Find("250"));
  ASSERT_EQ(2, Find("251"));
  ASSERT_EQ(2, Find("299"));
  ASSERT_EQ(2, Find("300"));
  ASSERT_EQ(2, Find("349"));
  ASSERT_EQ(2, Find("350"));
  ASSERT_EQ(3, Find("351"));
  ASSERT_EQ(3, Find("400"));
  ASSERT_EQ(3, Find("450"));
  ASSERT_EQ(4, Find("451"));

  ASSERT_TRUE(! Overlaps("100", "149"));
  ASSERT_TRUE(! Overlaps("251", "299"));
  ASSERT_TRUE(! Overlaps("451", "500"));
  ASSERT_TRUE(! Overlaps("351", "399"));

  ASSERT_TRUE(Overlaps("100", "150"));
  ASSERT_TRUE(Overlaps("100", "200"));
  ASSERT_TRUE(Overlaps("100", "300"));
  ASSERT_TRUE(Overlaps("100", "400"));
  ASSERT_TRUE(Overlaps("100", "500"));
  ASSERT_TRUE(Overlaps("375", "400"));
  ASSERT_TRUE(Overlaps("450", "450"));
  ASSERT_TRUE(Overlaps("450", "500"));
}

TEST_F(FindLevelFileTest, LevelPartitionMultiple) {
  LevelPartitionFileInit(4);

  AddPartition("150", "200");
  AddPartition("200", "250");
  AddPartition("300", "350");
  AddPartition("400", "450");
  ASSERT_EQ(0, Find("100"));
  ASSERT_EQ(0, Find("150"));
  ASSERT_EQ(0, Find("151"));
  ASSERT_EQ(0, Find("199"));
  ASSERT_EQ(0, Find("200"));
  ASSERT_EQ(1, Find("201"));
  ASSERT_EQ(1, Find("249"));
  ASSERT_EQ(1, Find("250"));
  ASSERT_EQ(2, Find("251"));
  ASSERT_EQ(2, Find("299"));
  ASSERT_EQ(2, Find("300"));
  ASSERT_EQ(2, Find("349"));
  ASSERT_EQ(2, Find("350"));
  ASSERT_EQ(3, Find("351"));
  ASSERT_EQ(3, Find("400"));
  ASSERT_EQ(3, Find("450"));
  ASSERT_EQ(4, Find("451"));

  ASSERT_TRUE(! Overlaps("100", "149"));
  ASSERT_TRUE(! Overlaps("251", "299"));
  ASSERT_TRUE(! Overlaps("451", "500"));
  ASSERT_TRUE(! Overlaps("351", "399"));

  ASSERT_TRUE(Overlaps("100", "150"));
  ASSERT_TRUE(Overlaps("100", "200"));
  ASSERT_TRUE(Overlaps("100", "300"));
  ASSERT_TRUE(Overlaps("100", "400"));
  ASSERT_TRUE(Overlaps("100", "500"));
  ASSERT_TRUE(Overlaps("375", "400"));
  ASSERT_TRUE(Overlaps("450", "450"));
  ASSERT_TRUE(Overlaps("450", "500"));
}

TEST_F(FindLevelFileTest, LevelMultipleNullBoundaries) {
  LevelFileInit(4);

  Add("150", "200");
  Add("200", "250");
  Add("300", "350");
  Add("400", "450");
  ASSERT_TRUE(! Overlaps(nullptr, "149"));
  ASSERT_TRUE(! Overlaps("451", nullptr));
  ASSERT_TRUE(Overlaps(nullptr, nullptr));
  ASSERT_TRUE(Overlaps(nullptr, "150"));
  ASSERT_TRUE(Overlaps(nullptr, "199"));
  ASSERT_TRUE(Overlaps(nullptr, "200"));
  ASSERT_TRUE(Overlaps(nullptr, "201"));
  ASSERT_TRUE(Overlaps(nullptr, "400"));
  ASSERT_TRUE(Overlaps(nullptr, "800"));
  ASSERT_TRUE(Overlaps("100", nullptr));
  ASSERT_TRUE(Overlaps("200", nullptr));
  ASSERT_TRUE(Overlaps("449", nullptr));
  ASSERT_TRUE(Overlaps("450", nullptr));
}

TEST_F(FindLevelFileTest, LevelPartitionMultipleNullBoundaries) {
  LevelPartitionFileInit(4);

  AddPartition("150", "200");
  AddPartition("200", "250");
  AddPartition("300", "350");
  AddPartition("400", "450");
  ASSERT_TRUE(! Overlaps(nullptr, "149"));
  ASSERT_TRUE(! Overlaps("451", nullptr));
  ASSERT_TRUE(Overlaps(nullptr, nullptr));
  ASSERT_TRUE(Overlaps(nullptr, "150"));
  ASSERT_TRUE(Overlaps(nullptr, "199"));
  ASSERT_TRUE(Overlaps(nullptr, "200"));
  ASSERT_TRUE(Overlaps(nullptr, "201"));
  ASSERT_TRUE(Overlaps(nullptr, "400"));
  ASSERT_TRUE(Overlaps(nullptr, "800"));
  ASSERT_TRUE(Overlaps("100", nullptr));
  ASSERT_TRUE(Overlaps("200", nullptr));
  ASSERT_TRUE(Overlaps("449", nullptr));
  ASSERT_TRUE(Overlaps("450", nullptr));
}

TEST_F(FindLevelFileTest, LevelOverlapSequenceChecks) {
  LevelFileInit(1);

  Add("200", "200", 5000, 3000);
  ASSERT_TRUE(! Overlaps("199", "199"));
  ASSERT_TRUE(! Overlaps("201", "300"));
  ASSERT_TRUE(Overlaps("200", "200"));
  ASSERT_TRUE(Overlaps("190", "200"));
  ASSERT_TRUE(Overlaps("200", "210"));
}

TEST_F(FindLevelFileTest, LevelPartitionOverlapSequenceChecks) {
  LevelPartitionFileInit(1);

  AddPartition("200", "200", 5000, 3000);
  ASSERT_TRUE(! Overlaps("199", "199"));
  ASSERT_TRUE(! Overlaps("201", "300"));
  ASSERT_TRUE(Overlaps("200", "200"));
  ASSERT_TRUE(Overlaps("190", "200"));
  ASSERT_TRUE(Overlaps("200", "210"));
}

TEST_F(FindLevelFileTest, LevelOverlappingFiles) {
  LevelFileInit(2);

  Add("150", "600");
  Add("400", "500");
  disjoint_sorted_files_ = false;
  ASSERT_TRUE(! Overlaps("100", "149"));
  ASSERT_TRUE(! Overlaps("601", "700"));
  ASSERT_TRUE(Overlaps("100", "150"));
  ASSERT_TRUE(Overlaps("100", "200"));
  ASSERT_TRUE(Overlaps("100", "300"));
  ASSERT_TRUE(Overlaps("100", "400"));
  ASSERT_TRUE(Overlaps("100", "500"));
  ASSERT_TRUE(Overlaps("375", "400"));
  ASSERT_TRUE(Overlaps("450", "450"));
  ASSERT_TRUE(Overlaps("450", "500"));
  ASSERT_TRUE(Overlaps("450", "700"));
  ASSERT_TRUE(Overlaps("600", "700"));
}

class VersionSetTestBase {
 public:
  const static std::string kColumnFamilyName1;
  const static std::string kColumnFamilyName2;
  const static std::string kColumnFamilyName3;
  int num_initial_edits_;

  VersionSetTestBase()
      : env_(Env::Default()),
        dbname_(test::PerThreadDBPath("version_set_test")),
        db_options_(),
        mutable_cf_options_(cf_options_),
        table_cache_(NewLRUCache(50000, 16)),
        write_buffer_manager_(db_options_.db_write_buffer_size),
        versions_(new VersionSet(dbname_, &db_options_, env_options_,
                                 table_cache_.get(), &write_buffer_manager_,
                                 &write_controller_,
                                 /*block_cache_tracer=*/nullptr)),
        reactive_versions_(std::make_shared<ReactiveVersionSet>(
            dbname_, &db_options_, env_options_, table_cache_.get(),
            &write_buffer_manager_, &write_controller_)),
        shutting_down_(false),
        mock_table_factory_(std::make_shared<mock::MockTableFactory>()) {
    EXPECT_OK(env_->CreateDirIfMissing(dbname_));
    db_options_.db_paths.emplace_back(dbname_,
                                      std::numeric_limits<uint64_t>::max());
  }

  void PrepareManifest(std::vector<ColumnFamilyDescriptor>* column_families,
                       SequenceNumber* last_seqno,
                       std::unique_ptr<log::Writer>* log_writer) {
    assert(column_families != nullptr);
    assert(last_seqno != nullptr);
    assert(log_writer != nullptr);
    VersionEdit new_db;
    if (db_options_.write_dbid_to_manifest) {
      DBImpl* impl = new DBImpl(DBOptions(), dbname_);
      std::string db_id;
      impl->GetDbIdentityFromIdentityFile(&db_id);
      new_db.SetDBId(db_id);
    }
    new_db.SetLogNumber(0);
    new_db.SetNextFile(2);
    new_db.SetLastSequence(0);

    const std::vector<std::string> cf_names = {
        kDefaultColumnFamilyName, kColumnFamilyName1, kColumnFamilyName2,
        kColumnFamilyName3};
    const int kInitialNumOfCfs = static_cast<int>(cf_names.size());
    autovector<VersionEdit> new_cfs;
    uint64_t last_seq = 1;
    uint32_t cf_id = 1;
    for (int i = 1; i != kInitialNumOfCfs; ++i) {
      VersionEdit new_cf;
      new_cf.AddColumnFamily(cf_names[i]);
      new_cf.SetColumnFamily(cf_id++);
      new_cf.SetLogNumber(0);
      new_cf.SetNextFile(2);
      new_cf.SetLastSequence(last_seq++);
      new_cfs.emplace_back(new_cf);
    }
    *last_seqno = last_seq;
    num_initial_edits_ = static_cast<int>(new_cfs.size() + 1);
    const std::string manifest = DescriptorFileName(dbname_, 1);
    std::unique_ptr<WritableFile> file;
    Status s = env_->NewWritableFile(
        manifest, &file, env_->OptimizeForManifestWrite(env_options_));
    ASSERT_OK(s);
    std::unique_ptr<WritableFileWriter> file_writer(
        new WritableFileWriter(std::move(file), manifest, env_options_));
    {
      log_writer->reset(new log::Writer(std::move(file_writer), 0, false));
      std::string record;
      new_db.EncodeTo(&record);
      s = (*log_writer)->AddRecord(record);
      for (const auto& e : new_cfs) {
        record.clear();
        e.EncodeTo(&record);
        s = (*log_writer)->AddRecord(record);
        ASSERT_OK(s);
      }
    }
    ASSERT_OK(s);

    cf_options_.table_factory = mock_table_factory_;
    for (const auto& cf_name : cf_names) {
      column_families->emplace_back(cf_name, cf_options_);
    }
  }

  // Create DB with 3 column families.
  void NewDB() {
    std::vector<ColumnFamilyDescriptor> column_families;
    SequenceNumber last_seqno;
    std::unique_ptr<log::Writer> log_writer;
    SetIdentityFile(env_, dbname_);
    PrepareManifest(&column_families, &last_seqno, &log_writer);
    log_writer.reset();
    // Make "CURRENT" file point to the new manifest file.
    Status s = SetCurrentFile(env_, dbname_, 1, nullptr);
    ASSERT_OK(s);

    EXPECT_OK(versions_->Recover(column_families, false));
    EXPECT_EQ(column_families.size(),
              versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  }

  Env* env_;
  const std::string dbname_;
  EnvOptions env_options_;
  ImmutableDBOptions db_options_;
  ColumnFamilyOptions cf_options_;
  MutableCFOptions mutable_cf_options_;
  std::shared_ptr<Cache> table_cache_;
  WriteController write_controller_;
  WriteBufferManager write_buffer_manager_;
  std::shared_ptr<VersionSet> versions_;
  std::shared_ptr<ReactiveVersionSet> reactive_versions_;
  InstrumentedMutex mutex_;
  std::atomic<bool> shutting_down_;
  std::shared_ptr<mock::MockTableFactory> mock_table_factory_;
};

const std::string VersionSetTestBase::kColumnFamilyName1 = "alice";
const std::string VersionSetTestBase::kColumnFamilyName2 = "bob";
const std::string VersionSetTestBase::kColumnFamilyName3 = "charles";

class VersionSetTest : public VersionSetTestBase, public testing::Test {
 public:
  VersionSetTest() : VersionSetTestBase() {}
};

TEST_F(VersionSetTest, SameColumnFamilyGroupCommit) {
  NewDB();
  const int kGroupSize = 5;
  autovector<VersionEdit> edits;
  for (int i = 0; i != kGroupSize; ++i) {
    edits.emplace_back(VersionEdit());
  }
  autovector<ColumnFamilyData*> cfds;
  autovector<const MutableCFOptions*> all_mutable_cf_options;
  autovector<autovector<VersionEdit*>> edit_lists;
  for (int i = 0; i != kGroupSize; ++i) {
    cfds.emplace_back(versions_->GetColumnFamilySet()->GetDefault());
    all_mutable_cf_options.emplace_back(&mutable_cf_options_);
    autovector<VersionEdit*> edit_list;
    edit_list.emplace_back(&edits[i]);
    edit_lists.emplace_back(edit_list);
  }

  SyncPoint::GetInstance()->DisableProcessing();
  SyncPoint::GetInstance()->ClearAllCallBacks();
  int count = 0;
  SyncPoint::GetInstance()->SetCallBack(
      "VersionSet::ProcessManifestWrites:SameColumnFamily", [&](void* arg) {
        uint32_t* cf_id = reinterpret_cast<uint32_t*>(arg);
        EXPECT_EQ(0u, *cf_id);
        ++count;
      });
  SyncPoint::GetInstance()->EnableProcessing();
  mutex_.Lock();
  Status s =
      versions_->LogAndApply(cfds, all_mutable_cf_options, edit_lists, &mutex_);
  mutex_.Unlock();
  EXPECT_OK(s);
  EXPECT_EQ(kGroupSize - 1, count);
}

class VersionSetAtomicGroupTest : public VersionSetTestBase,
                                  public testing::Test {
 public:
  VersionSetAtomicGroupTest() : VersionSetTestBase() {}

  void SetUp() override {
    PrepareManifest(&column_families_, &last_seqno_, &log_writer_);
    SetupTestSyncPoints();
  }

  void SetupValidAtomicGroup(int atomic_group_size) {
    edits_.resize(atomic_group_size);
    int remaining = atomic_group_size;
    for (size_t i = 0; i != edits_.size(); ++i) {
      edits_[i].SetLogNumber(0);
      edits_[i].SetNextFile(2);
      edits_[i].MarkAtomicGroup(--remaining);
      edits_[i].SetLastSequence(last_seqno_++);
    }
    ASSERT_OK(SetCurrentFile(env_, dbname_, 1, nullptr));
  }

  void SetupIncompleteTrailingAtomicGroup(int atomic_group_size) {
    edits_.resize(atomic_group_size);
    int remaining = atomic_group_size;
    for (size_t i = 0; i != edits_.size(); ++i) {
      edits_[i].SetLogNumber(0);
      edits_[i].SetNextFile(2);
      edits_[i].MarkAtomicGroup(--remaining);
      edits_[i].SetLastSequence(last_seqno_++);
    }
    ASSERT_OK(SetCurrentFile(env_, dbname_, 1, nullptr));
  }

  void SetupCorruptedAtomicGroup(int atomic_group_size) {
    edits_.resize(atomic_group_size);
    int remaining = atomic_group_size;
    for (size_t i = 0; i != edits_.size(); ++i) {
      edits_[i].SetLogNumber(0);
      edits_[i].SetNextFile(2);
      if (i != ((size_t)atomic_group_size / 2)) {
        edits_[i].MarkAtomicGroup(--remaining);
      }
      edits_[i].SetLastSequence(last_seqno_++);
    }
    ASSERT_OK(SetCurrentFile(env_, dbname_, 1, nullptr));
  }

  void SetupIncorrectAtomicGroup(int atomic_group_size) {
    edits_.resize(atomic_group_size);
    int remaining = atomic_group_size;
    for (size_t i = 0; i != edits_.size(); ++i) {
      edits_[i].SetLogNumber(0);
      edits_[i].SetNextFile(2);
      if (i != 1) {
        edits_[i].MarkAtomicGroup(--remaining);
      } else {
        edits_[i].MarkAtomicGroup(remaining--);
      }
      edits_[i].SetLastSequence(last_seqno_++);
    }
    ASSERT_OK(SetCurrentFile(env_, dbname_, 1, nullptr));
  }

  void SetupTestSyncPoints() {
    SyncPoint::GetInstance()->DisableProcessing();
    SyncPoint::GetInstance()->ClearAllCallBacks();
    SyncPoint::GetInstance()->SetCallBack(
        "AtomicGroupReadBuffer::AddEdit:FirstInAtomicGroup", [&](void* arg) {
          VersionEdit* e = reinterpret_cast<VersionEdit*>(arg);
          EXPECT_EQ(edits_.front().DebugString(),
                    e->DebugString());  // compare based on value
          first_in_atomic_group_ = true;
        });
    SyncPoint::GetInstance()->SetCallBack(
        "AtomicGroupReadBuffer::AddEdit:LastInAtomicGroup", [&](void* arg) {
          VersionEdit* e = reinterpret_cast<VersionEdit*>(arg);
          EXPECT_EQ(edits_.back().DebugString(),
                    e->DebugString());  // compare based on value
          EXPECT_TRUE(first_in_atomic_group_);
          last_in_atomic_group_ = true;
        });
    SyncPoint::GetInstance()->SetCallBack(
        "VersionSet::ReadAndRecover:RecoveredEdits", [&](void* arg) {
          num_recovered_edits_ = *reinterpret_cast<int*>(arg);
        });
    SyncPoint::GetInstance()->SetCallBack(
        "ReactiveVersionSet::ReadAndApply:AppliedEdits",
        [&](void* arg) { num_applied_edits_ = *reinterpret_cast<int*>(arg); });
    SyncPoint::GetInstance()->SetCallBack(
        "AtomicGroupReadBuffer::AddEdit:AtomicGroup",
        [&](void* /* arg */) { ++num_edits_in_atomic_group_; });
    SyncPoint::GetInstance()->SetCallBack(
        "AtomicGroupReadBuffer::AddEdit:AtomicGroupMixedWithNormalEdits",
        [&](void* arg) {
          corrupted_edit_ = *reinterpret_cast<VersionEdit*>(arg);
        });
    SyncPoint::GetInstance()->SetCallBack(
        "AtomicGroupReadBuffer::AddEdit:IncorrectAtomicGroupSize",
        [&](void* arg) {
          edit_with_incorrect_group_size_ =
              *reinterpret_cast<VersionEdit*>(arg);
        });
    SyncPoint::GetInstance()->EnableProcessing();
  }

  void AddNewEditsToLog(int num_edits) {
    for (int i = 0; i < num_edits; i++) {
      std::string record;
      edits_[i].EncodeTo(&record);
      ASSERT_OK(log_writer_->AddRecord(record));
    }
  }

  void TearDown() override {
    SyncPoint::GetInstance()->DisableProcessing();
    SyncPoint::GetInstance()->ClearAllCallBacks();
    log_writer_.reset();
  }

 protected:
  std::vector<ColumnFamilyDescriptor> column_families_;
  SequenceNumber last_seqno_;
  std::vector<VersionEdit> edits_;
  bool first_in_atomic_group_ = false;
  bool last_in_atomic_group_ = false;
  int num_edits_in_atomic_group_ = 0;
  int num_recovered_edits_ = 0;
  int num_applied_edits_ = 0;
  VersionEdit corrupted_edit_;
  VersionEdit edit_with_incorrect_group_size_;
  std::unique_ptr<log::Writer> log_writer_;
};

TEST_F(VersionSetAtomicGroupTest, HandleValidAtomicGroupWithVersionSetRecover) {
  const int kAtomicGroupSize = 3;
  SetupValidAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kAtomicGroupSize);
  EXPECT_OK(versions_->Recover(column_families_, false));
  EXPECT_EQ(column_families_.size(),
            versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_TRUE(first_in_atomic_group_);
  EXPECT_TRUE(last_in_atomic_group_);
  EXPECT_EQ(num_initial_edits_ + kAtomicGroupSize, num_recovered_edits_);
  EXPECT_EQ(0, num_applied_edits_);
}

TEST_F(VersionSetAtomicGroupTest,
       HandleValidAtomicGroupWithReactiveVersionSetRecover) {
  const int kAtomicGroupSize = 3;
  SetupValidAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kAtomicGroupSize);
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  EXPECT_OK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                        &manifest_reporter,
                                        &manifest_reader_status));
  EXPECT_EQ(column_families_.size(),
            reactive_versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_TRUE(first_in_atomic_group_);
  EXPECT_TRUE(last_in_atomic_group_);
  // The recover should clean up the replay buffer.
  EXPECT_TRUE(reactive_versions_->TEST_read_edits_in_atomic_group() == 0);
  EXPECT_TRUE(reactive_versions_->replay_buffer().size() == 0);
  EXPECT_EQ(num_initial_edits_ + kAtomicGroupSize, num_recovered_edits_);
  EXPECT_EQ(0, num_applied_edits_);
}

TEST_F(VersionSetAtomicGroupTest,
       HandleValidAtomicGroupWithReactiveVersionSetReadAndApply) {
  const int kAtomicGroupSize = 3;
  SetupValidAtomicGroup(kAtomicGroupSize);
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  EXPECT_OK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                        &manifest_reporter,
                                        &manifest_reader_status));
  AddNewEditsToLog(kAtomicGroupSize);
  InstrumentedMutex mu;
  std::unordered_set<ColumnFamilyData*> cfds_changed;
  mu.Lock();
  EXPECT_OK(
      reactive_versions_->ReadAndApply(&mu, &manifest_reader, &cfds_changed));
  mu.Unlock();
  EXPECT_TRUE(first_in_atomic_group_);
  EXPECT_TRUE(last_in_atomic_group_);
  // The recover should clean up the replay buffer.
  EXPECT_TRUE(reactive_versions_->TEST_read_edits_in_atomic_group() == 0);
  EXPECT_TRUE(reactive_versions_->replay_buffer().size() == 0);
  EXPECT_EQ(num_initial_edits_, num_recovered_edits_);
  EXPECT_EQ(kAtomicGroupSize, num_applied_edits_);
}

TEST_F(VersionSetAtomicGroupTest,
       HandleIncompleteTrailingAtomicGroupWithVersionSetRecover) {
  const int kAtomicGroupSize = 4;
  const int kNumberOfPersistedVersionEdits = kAtomicGroupSize - 1;
  SetupIncompleteTrailingAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kNumberOfPersistedVersionEdits);
  EXPECT_OK(versions_->Recover(column_families_, false));
  EXPECT_EQ(column_families_.size(),
            versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_TRUE(first_in_atomic_group_);
  EXPECT_FALSE(last_in_atomic_group_);
  EXPECT_EQ(kNumberOfPersistedVersionEdits, num_edits_in_atomic_group_);
  EXPECT_EQ(num_initial_edits_, num_recovered_edits_);
  EXPECT_EQ(0, num_applied_edits_);
}

TEST_F(VersionSetAtomicGroupTest,
       HandleIncompleteTrailingAtomicGroupWithReactiveVersionSetRecover) {
  const int kAtomicGroupSize = 4;
  const int kNumberOfPersistedVersionEdits = kAtomicGroupSize - 1;
  SetupIncompleteTrailingAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kNumberOfPersistedVersionEdits);
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  EXPECT_OK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                        &manifest_reporter,
                                        &manifest_reader_status));
  EXPECT_EQ(column_families_.size(),
            reactive_versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_TRUE(first_in_atomic_group_);
  EXPECT_FALSE(last_in_atomic_group_);
  EXPECT_EQ(kNumberOfPersistedVersionEdits, num_edits_in_atomic_group_);
  // Reactive version set should store the edits in the replay buffer.
  EXPECT_TRUE(reactive_versions_->TEST_read_edits_in_atomic_group() ==
              kNumberOfPersistedVersionEdits);
  EXPECT_TRUE(reactive_versions_->replay_buffer().size() == kAtomicGroupSize);
  // Write the last record. The reactive version set should now apply all
  // edits.
  std::string last_record;
  edits_[kAtomicGroupSize - 1].EncodeTo(&last_record);
  EXPECT_OK(log_writer_->AddRecord(last_record));
  InstrumentedMutex mu;
  std::unordered_set<ColumnFamilyData*> cfds_changed;
  mu.Lock();
  EXPECT_OK(
      reactive_versions_->ReadAndApply(&mu, &manifest_reader, &cfds_changed));
  mu.Unlock();
  // Reactive version set should be empty now.
  EXPECT_TRUE(reactive_versions_->TEST_read_edits_in_atomic_group() == 0);
  EXPECT_TRUE(reactive_versions_->replay_buffer().size() == 0);
  EXPECT_EQ(num_initial_edits_, num_recovered_edits_);
  EXPECT_EQ(kAtomicGroupSize, num_applied_edits_);
}

TEST_F(VersionSetAtomicGroupTest,
       HandleIncompleteTrailingAtomicGroupWithReactiveVersionSetReadAndApply) {
  const int kAtomicGroupSize = 4;
  const int kNumberOfPersistedVersionEdits = kAtomicGroupSize - 1;
  SetupIncompleteTrailingAtomicGroup(kAtomicGroupSize);
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  // No edits in an atomic group.
  EXPECT_OK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                        &manifest_reporter,
                                        &manifest_reader_status));
  EXPECT_EQ(column_families_.size(),
            reactive_versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  // Write a few edits in an atomic group.
  AddNewEditsToLog(kNumberOfPersistedVersionEdits);
  InstrumentedMutex mu;
  std::unordered_set<ColumnFamilyData*> cfds_changed;
  mu.Lock();
  EXPECT_OK(
      reactive_versions_->ReadAndApply(&mu, &manifest_reader, &cfds_changed));
  mu.Unlock();
  EXPECT_TRUE(first_in_atomic_group_);
  EXPECT_FALSE(last_in_atomic_group_);
  EXPECT_EQ(kNumberOfPersistedVersionEdits, num_edits_in_atomic_group_);
  // Reactive version set should store the edits in the replay buffer.
  EXPECT_TRUE(reactive_versions_->TEST_read_edits_in_atomic_group() ==
              kNumberOfPersistedVersionEdits);
  EXPECT_TRUE(reactive_versions_->replay_buffer().size() == kAtomicGroupSize);
  EXPECT_EQ(num_initial_edits_, num_recovered_edits_);
  EXPECT_EQ(0, num_applied_edits_);
}

TEST_F(VersionSetAtomicGroupTest,
       HandleCorruptedAtomicGroupWithVersionSetRecover) {
  const int kAtomicGroupSize = 4;
  SetupCorruptedAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kAtomicGroupSize);
  EXPECT_NOK(versions_->Recover(column_families_, false));
  EXPECT_EQ(column_families_.size(),
            versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_EQ(edits_[kAtomicGroupSize / 2].DebugString(),
            corrupted_edit_.DebugString());
}

TEST_F(VersionSetAtomicGroupTest,
       HandleCorruptedAtomicGroupWithReactiveVersionSetRecover) {
  const int kAtomicGroupSize = 4;
  SetupCorruptedAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kAtomicGroupSize);
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  EXPECT_NOK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                         &manifest_reporter,
                                         &manifest_reader_status));
  EXPECT_EQ(column_families_.size(),
            reactive_versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_EQ(edits_[kAtomicGroupSize / 2].DebugString(),
            corrupted_edit_.DebugString());
}

TEST_F(VersionSetAtomicGroupTest,
       HandleCorruptedAtomicGroupWithReactiveVersionSetReadAndApply) {
  const int kAtomicGroupSize = 4;
  SetupCorruptedAtomicGroup(kAtomicGroupSize);
  InstrumentedMutex mu;
  std::unordered_set<ColumnFamilyData*> cfds_changed;
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  EXPECT_OK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                        &manifest_reporter,
                                        &manifest_reader_status));
  // Write the corrupted edits.
  AddNewEditsToLog(kAtomicGroupSize);
  mu.Lock();
  EXPECT_OK(
      reactive_versions_->ReadAndApply(&mu, &manifest_reader, &cfds_changed));
  mu.Unlock();
  EXPECT_EQ(edits_[kAtomicGroupSize / 2].DebugString(),
            corrupted_edit_.DebugString());
}

TEST_F(VersionSetAtomicGroupTest,
       HandleIncorrectAtomicGroupSizeWithVersionSetRecover) {
  const int kAtomicGroupSize = 4;
  SetupIncorrectAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kAtomicGroupSize);
  EXPECT_NOK(versions_->Recover(column_families_, false));
  EXPECT_EQ(column_families_.size(),
            versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_EQ(edits_[1].DebugString(),
            edit_with_incorrect_group_size_.DebugString());
}

TEST_F(VersionSetAtomicGroupTest,
       HandleIncorrectAtomicGroupSizeWithReactiveVersionSetRecover) {
  const int kAtomicGroupSize = 4;
  SetupIncorrectAtomicGroup(kAtomicGroupSize);
  AddNewEditsToLog(kAtomicGroupSize);
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  EXPECT_NOK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                         &manifest_reporter,
                                         &manifest_reader_status));
  EXPECT_EQ(column_families_.size(),
            reactive_versions_->GetColumnFamilySet()->NumberOfColumnFamilies());
  EXPECT_EQ(edits_[1].DebugString(),
            edit_with_incorrect_group_size_.DebugString());
}

TEST_F(VersionSetAtomicGroupTest,
       HandleIncorrectAtomicGroupSizeWithReactiveVersionSetReadAndApply) {
  const int kAtomicGroupSize = 4;
  SetupIncorrectAtomicGroup(kAtomicGroupSize);
  InstrumentedMutex mu;
  std::unordered_set<ColumnFamilyData*> cfds_changed;
  std::unique_ptr<log::FragmentBufferedReader> manifest_reader;
  std::unique_ptr<log::Reader::Reporter> manifest_reporter;
  std::unique_ptr<Status> manifest_reader_status;
  EXPECT_OK(reactive_versions_->Recover(column_families_, &manifest_reader,
                                        &manifest_reporter,
                                        &manifest_reader_status));
  AddNewEditsToLog(kAtomicGroupSize);
  mu.Lock();
  EXPECT_OK(
      reactive_versions_->ReadAndApply(&mu, &manifest_reader, &cfds_changed));
  mu.Unlock();
  EXPECT_EQ(edits_[1].DebugString(),
            edit_with_incorrect_group_size_.DebugString());
}

class VersionSetTestDropOneCF : public VersionSetTestBase,
                                public testing::TestWithParam<std::string> {
 public:
  VersionSetTestDropOneCF() : VersionSetTestBase() {}
};

// This test simulates the following execution sequence
// Time  thread1                  bg_flush_thr
//  |                             Prepare version edits (e1,e2,e3) for atomic
//  |                             flush cf1, cf2, cf3
//  |    Enqueue e to drop cfi
//  |    to manifest_writers_
//  |                             Enqueue (e1,e2,e3) to manifest_writers_
//  |
//  |    Apply e,
//  |    cfi.IsDropped() is true
//  |                             Apply (e1,e2,e3),
//  |                             since cfi.IsDropped() == true, we need to
//  |                             drop ei and write the rest to MANIFEST.
//  V
//
//  Repeat the test for i = 1, 2, 3 to simulate dropping the first, middle and
//  last column family in an atomic group.
TEST_P(VersionSetTestDropOneCF, HandleDroppedColumnFamilyInAtomicGroup) {
  std::vector<ColumnFamilyDescriptor> column_families;
  SequenceNumber last_seqno;
  std::unique_ptr<log::Writer> log_writer;
  PrepareManifest(&column_families, &last_seqno, &log_writer);
  Status s = SetCurrentFile(env_, dbname_, 1, nullptr);
  ASSERT_OK(s);

  EXPECT_OK(versions_->Recover(column_families, false /* read_only */));
  EXPECT_EQ(column_families.size(),
            versions_->GetColumnFamilySet()->NumberOfColumnFamilies());

  const int kAtomicGroupSize = 3;
  const std::vector<std::string> non_default_cf_names = {
      kColumnFamilyName1, kColumnFamilyName2, kColumnFamilyName3};

  // Drop one column family
  VersionEdit drop_cf_edit;
  drop_cf_edit.DropColumnFamily();
  const std::string cf_to_drop_name(GetParam());
  auto cfd_to_drop =
      versions_->GetColumnFamilySet()->GetColumnFamily(cf_to_drop_name);
  ASSERT_NE(nullptr, cfd_to_drop);
  // Increase its refcount because cfd_to_drop is used later, and we need to
  // prevent it from being deleted.
  cfd_to_drop->Ref();
  drop_cf_edit.SetColumnFamily(cfd_to_drop->GetID());
  mutex_.Lock();
  s = versions_->LogAndApply(cfd_to_drop,
                             *cfd_to_drop->GetLatestMutableCFOptions(),
                             &drop_cf_edit, &mutex_);
  mutex_.Unlock();
  ASSERT_OK(s);

  std::vector<VersionEdit> edits(kAtomicGroupSize);
  uint32_t remaining = kAtomicGroupSize;
  size_t i = 0;
  autovector<ColumnFamilyData*> cfds;
  autovector<const MutableCFOptions*> mutable_cf_options_list;
  autovector<autovector<VersionEdit*>> edit_lists;
  for (const auto& cf_name : non_default_cf_names) {
    auto cfd = (cf_name != cf_to_drop_name)
                   ? versions_->GetColumnFamilySet()->GetColumnFamily(cf_name)
                   : cfd_to_drop;
    ASSERT_NE(nullptr, cfd);
    cfds.push_back(cfd);
    mutable_cf_options_list.emplace_back(cfd->GetLatestMutableCFOptions());
    edits[i].SetColumnFamily(cfd->GetID());
    edits[i].SetLogNumber(0);
    edits[i].SetNextFile(2);
    edits[i].MarkAtomicGroup(--remaining);
    edits[i].SetLastSequence(last_seqno++);
    autovector<VersionEdit*> tmp_edits;
    tmp_edits.push_back(&edits[i]);
    edit_lists.emplace_back(tmp_edits);
    ++i;
  }
  int called = 0;
  SyncPoint::GetInstance()->DisableProcessing();
  SyncPoint::GetInstance()->ClearAllCallBacks();
  SyncPoint::GetInstance()->SetCallBack(
      "VersionSet::ProcessManifestWrites:CheckOneAtomicGroup", [&](void* arg) {
        std::vector<VersionEdit*>* tmp_edits =
            reinterpret_cast<std::vector<VersionEdit*>*>(arg);
        EXPECT_EQ(kAtomicGroupSize - 1, tmp_edits->size());
        for (const auto e : *tmp_edits) {
          bool found = false;
          for (const auto& e2 : edits) {
            if (&e2 == e) {
              found = true;
              break;
            }
          }
          ASSERT_TRUE(found);
        }
        ++called;
      });
  SyncPoint::GetInstance()->EnableProcessing();
  mutex_.Lock();
  s = versions_->LogAndApply(cfds, mutable_cf_options_list, edit_lists,
                             &mutex_);
  mutex_.Unlock();
  ASSERT_OK(s);
  ASSERT_EQ(1, called);
  if (cfd_to_drop->Unref()) {
    delete cfd_to_drop;
    cfd_to_drop = nullptr;
  }
}

INSTANTIATE_TEST_CASE_P(
    AtomicGroup, VersionSetTestDropOneCF,
    testing::Values(VersionSetTestBase::kColumnFamilyName1,
                    VersionSetTestBase::kColumnFamilyName2,
                    VersionSetTestBase::kColumnFamilyName3));
}  // namespace rocksdb

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
