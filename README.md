## MirrorKV

Original [RocksDB-cloud](https://github.com/rockset/rocksdb-cloud)

### Dependencies
We run the code in Ubuntu 20.04LTS. Please first install AWS CPP SDK in this [link](https://docs.aws.amazon.com/sdk-for-cpp/v1/developer-guide/setup-linux.html) (We use version 1.9.30).

After installation, please set the
following environment variables to run the cloud unit tests:

AWS_ACCESS_KEY_ID     : your aws access credentials

AWS_SECRET_ACCESS_KEY : your secret key

AWS_DEFAULT_REGION : the AWS region of your client (e.g. us-west-2)

Set the environment variable to enable AWS.

### Test Code

```
$ export USE_AWS=1
```
Compile the test program. Before running the test program, make sure your corresponding S3 bucket is created, or you can modify the path in db_test3.cc
```
$ make -j8 db_test3
```

### Examples
Cloud environment setup.
```cpp
Options options;
options.keep_levels = 2; // Keep the top two levels on EBS
std::string dbpath = "/tmp/PartitionTest5";
std::string region = "ap-northeast-1";
std::string bucket_prefix = "rockset.";
std::string bucket_suffix = "cloud-db-examples.alec";

CloudEnvOptions cloud_env_options;
std::unique_ptr<CloudEnv> cloud_env;
cloud_env_options.src_bucket.SetBucketName(bucket_suffix,bucket_prefix);
cloud_env_options.dest_bucket.SetBucketName(bucket_suffix,bucket_prefix);
cloud_env_options.keep_local_sst_files = true;
cloud_env_options.keep_sst_levels = 2; // Keep the top two levels on EBS
CloudEnv* cenv;
const std::string bucketName = bucket_suffix + bucket_prefix;
Status s = CloudEnv::NewAwsEnv(Env::Default(),
    bucket_suffix, dbpath, region,
    bucket_suffix, dbpath, region,
    cloud_env_options, nullptr, &cenv);
    // NewLRUCache(256*1024*1024));
ASSERT_OK(s);
cloud_env.reset(cenv);
```
Configure the database options and open the database.
```cpp
options.create_if_missing = true;
options.compaction_style = kCompactionStyleLevel;
options.write_buffer_size = 110 << 10;  // 110KB
options.arena_block_size = 4 << 10;
options.level0_file_num_compaction_trigger = 2;
options.max_bytes_for_level_base = 100 << 10; // 100KB
options.env = cloud_env.get();
options.kv_separation = __db_test3_kv_separation;
options.aws_use_cloud_cache = true;
// Cloud cache
options.ccache = std::shared_ptr<CloudCache>(new CloudCache(
  std::shared_ptr<Cache>(NewLRUCache(
    1 * 1024 * 1024,
    -1/*num_shard_bits*/,
    false/*strict_capacity_limit*/,
    0.3/*high_pri_pool_ratio*/
  )),
  1 * 1024 * 1024/*pcache size*/, 128 * 1024/*block size*/,
  cloud_env->GetBaseEnv()));
std::string persistent_cache = "";
DBCloud* db;
s = DBCloud::Open(options, dbpath, persistent_cache, 0, &db);
```
Insert 100 random keys.
```cpp
int num_keys = 100;
std::vector<std::string> keys;
std::vector<std::string> values;
std::set<std::string> existing_keys;
for (int i = 0; i < num_keys; i++) {
  std::string tmp_key;
  while (true) {
    tmp_key = RandomString(&rnd, 9);
    if (existing_keys.find(tmp_key) == existing_keys.end()) {
      existing_keys.insert(tmp_key);
      break;
    }
  }
  keys.push_back(tmp_key);
  values.push_back(RandomString(&rnd, (i == 99) ? 1 : 990));
  ASSERT_OK(db->Put(WriteOptions(), keys.back(), values.back()));
}
```
Query the inserted keys and verify.
```cpp
for (size_t i = 0; i < keys.size(); i++) {
  std::string val;
  ASSERT_OK(db->Get(ReadOptions(), keys[i], &val));
  ASSERT_EQ(values[i], val);
}
```

### Benchmark
We extend WiscKey and PebblesDB with cloud support and include them in our customized YCSB benchmark in C++.  
Please follow this [link](https://bitbucket.org/867245430/ycsb-c) to run the benchmarks.