#include "cache/lru_cache.h"
#include "cloud/cloud_cache.h"
#include "cloud/testutil.h"
#include "rocksdb/env.h"
#include "test_util/testharness.h"
#include "third-party/thread_pool.h"
#include "util/random.h"

#include <algorithm>
#include <random>

namespace rocksdb {

class PosixTest : public testing::Test {
public:
	PosixTest(): env_(Env::Default()), file_(test::PerThreadDBPath(env_, "posixtest")) {
		env_->DeleteFile(file_);
		FIRST_LEVEL_GRANULARITY = 2048 * 8;
		SECOND_LEVEL_GRANULARITY = 2048;
		THIRD_LEVEL_GRANULARITY = 256;
		MUTABLE_PERSISTENT_BLOCK_NUM = 4;
		PAGE_SIZE = 8;
		options_.posix_use_cloud_cache = true;
		options_.ccache =
				std::shared_ptr<CloudCache>(
					new CloudCache(
						std::shared_ptr<Cache>(NewLRUCache(512)),
						128/*pcache size*/, 64/*block size*/,
						env_));

		std::unique_ptr<WritableFile> f;
		Status s = env_->NewWritableFile(file_, &f, options_);
		if (!s.ok()) {
			printf("cannot open file to write\n");
			exit(-1);
		}
		char buf[9];
		for (int i = 0; i < 256; i++) {
			uint8_t tmp = i;
			sprintf(buf, "%c%c%c%c%c%c%c%c", tmp, tmp, tmp, tmp, tmp, tmp, tmp, tmp);
			s = f->Append(Slice(buf, 8));
			if (!s.ok()) {
				printf("append file error\n");
				exit(-1);
			}
		}
	}

	void get_result(uint64_t start_off, size_t n, char* scratch) {
		int idx = 0;
		for (uint64_t i = start_off; i < start_off + n; i++) {
			uint8_t c = i / 8;
			scratch[idx++] = c;
		}
	}

protected:
	Env* env_;
	std::string file_;
	EnvOptions options_;
};

// Sequentially read all pages.
TEST_F(PosixTest, Test1) {
	std::unique_ptr<RandomAccessFile> f;
	env_->NewRandomAccessFile(file_, &f, options_);
	Slice result;
	char scratch1[9];
	char scratch2[9];
	for (int i = 0; i < 256; i++) {
		ASSERT_TRUE((f->Read((uint64_t)(i) * PAGE_SIZE, PAGE_SIZE, &result, scratch1)).ok());
		uint8_t tmp = i;
		sprintf(scratch2, "%c%c%c%c%c%c%c%c", tmp, tmp, tmp, tmp, tmp, tmp, tmp, tmp);
		ASSERT_EQ(0, strncmp(scratch2, result.data(), PAGE_SIZE));
	}
	ASSERT_TRUE(options_.ccache->cached_size(file_) >= 640 && options_.ccache->cached_size(file_) <= 896);
	options_.ccache->print_summary(true);

	// Check cache.
	std::vector<uint8_t> ids;
	int start_page;
	options_.ccache->lookup(file_, 0, 2048, &start_page, &ids);
	ASSERT_EQ(0, start_page);
	ASSERT_EQ(256, (int)(ids.size()));

	// Sequential read.
	for (int i = 0; i < 256; i++) {
		ASSERT_TRUE((f->Read((uint64_t)(i) * PAGE_SIZE, PAGE_SIZE, &result, scratch1)).ok());
		uint8_t tmp = i;
		sprintf(scratch2, "%c%c%c%c%c%c%c%c", tmp, tmp, tmp, tmp, tmp, tmp, tmp, tmp);
		ASSERT_EQ(0, strncmp(scratch2, result.data(), PAGE_SIZE));
	}

	// Random read.
	Random rnd(101);
	for (int i = 0; i < 100; i++) {
		uint32_t page = rnd.Next() % 256;
		ASSERT_TRUE((f->Read((uint64_t)(page) * PAGE_SIZE, PAGE_SIZE, &result, scratch1)).ok());
		uint8_t tmp = page;
		sprintf(scratch2, "%c%c%c%c%c%c%c%c", tmp, tmp, tmp, tmp, tmp, tmp, tmp, tmp);
		ASSERT_EQ(0, strncmp(scratch2, result.data(), PAGE_SIZE));
	}
}

// Randomly read all pages.
TEST_F(PosixTest, Test2) {
	std::unique_ptr<RandomAccessFile> f;
	env_->NewRandomAccessFile(file_, &f, options_);
	Slice result;
	char scratch1[9];
	char scratch2[9];
	std::vector<int> read_pages(256);
	for (size_t i = 0; i < read_pages.size(); i++)
		read_pages[i] = (int)(i);
	std::random_shuffle(read_pages.begin(), read_pages.end());

	for (int i = 0; i < 256; i++) {
		ASSERT_TRUE((f->Read((uint64_t)(read_pages[i]) * PAGE_SIZE, PAGE_SIZE, &result, scratch1)).ok());
		uint8_t tmp = read_pages[i];
		sprintf(scratch2, "%c%c%c%c%c%c%c%c", tmp, tmp, tmp, tmp, tmp, tmp, tmp, tmp);
		ASSERT_EQ(0, strncmp(scratch2, result.data(), PAGE_SIZE));
	}
	ASSERT_TRUE(options_.ccache->cached_size(file_) >= 640 && options_.ccache->cached_size(file_) <= 896);

	// Check cache.
	std::vector<uint8_t> ids;
	int start_page;
	options_.ccache->lookup(file_, 0, 2048, &start_page, &ids);
	ASSERT_EQ(0, start_page);
	ASSERT_EQ(256, (int)(ids.size()));

	// Sequential read.
	for (int i = 0; i < 256; i++) {
		ASSERT_TRUE((f->Read((uint64_t)(i) * PAGE_SIZE, PAGE_SIZE, &result, scratch1)).ok());
		uint8_t tmp = i;
		sprintf(scratch2, "%c%c%c%c%c%c%c%c", tmp, tmp, tmp, tmp, tmp, tmp, tmp, tmp);
		ASSERT_EQ(0, strncmp(scratch2, result.data(), PAGE_SIZE));
	}

	// Random read.
	Random rnd(101);
	for (int i = 0; i < 100; i++) {
		uint32_t page = rnd.Next() % 256;
		ASSERT_TRUE((f->Read((uint64_t)(page) * PAGE_SIZE, PAGE_SIZE, &result, scratch1)).ok());
		uint8_t tmp = page;
		sprintf(scratch2, "%c%c%c%c%c%c%c%c", tmp, tmp, tmp, tmp, tmp, tmp, tmp, tmp);
		ASSERT_EQ(0, strncmp(scratch2, result.data(), PAGE_SIZE));
	}
}

// Randomly read ranges.
TEST_F(PosixTest, Test3) {
	std::unique_ptr<RandomAccessFile> f;
	env_->NewRandomAccessFile(file_, &f, options_);
	Random rnd(101);
	Slice result;
	char scratch1[2048];
	char scratch2[2048];
	for (int i = 0; i < 500; i++) {
		uint32_t start_off = rnd.Next() % 2048;
		uint32_t n = rnd.Next() % (2048 - start_off);
		while (n == 0)
			n = rnd.Next() % (2048 - start_off);
		ASSERT_TRUE((f->Read(start_off, n, &result, scratch1)).ok());
		get_result(start_off, n, scratch2);
		ASSERT_EQ(0, strncmp(scratch2, result.data(), n));
	}
}

} // namespace rocksdb

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}